"use strict";

var pms = require('../pmsNew');
var userauth = require('../userAuthentication');

module.exports = function (app, db){
    app.route("/api/sftp/pms").post(userauth.checkToken, pms.pmsSftpAPI);
}
