exports.kpiconstant = {
    channel: 'L&D',
    product: 'Illume'
}
exports.process = {
    openapi: 'OPENAPI',
    mylearning: 'My Learning',
    consumption: 'Content Consumption API',
    searchengine: 'Search Engine',
    login: 'Login',
    learningpreferenceform: 'Learning Preference Form',
    sftp: 'Day 1 user creation'
}
exports.ExceptionType = {
    200: {
        name: 'Success',
        value: 200
    },
    429: {
        name: 'Too Many Requests',
        value: 429
    },
    500: {
        name: 'Internal Server Error',
        value: 500
    },
    501: {
        name: 'Failed to search Error',
        value: 501
    },
    403: {
        name: 'Forbidden Error',
        value: 403
    },
    404: {
        name: 'Not found',
        value: 404
    },
    412:{
        name:'Access to the target resource has been denied',
        value: 412
    }
};