// require('core-js/modules/es.promise');
// require('core-js/modules/es.string.includes');
// require('core-js/modules/es.object.assign');
// require('core-js/modules/es.object.keys');
// require('core-js/modules/es.symbol');
// require('core-js/modules/es.symbol.async-iterator');
require('regenerator-runtime/runtime');
const path = require('path');
const ExcelJS = require('exceljs/dist/es5');
const fs = require('fs');
// var csv = require('csvtojson');
var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");
var moment = require('moment');
let number = 100;

var uniqueKey = 'Asset Code';

exports.jsonToExcel = (data) => {
    const dataPassed = data;
    // console.log('jsonToExcel', dataPassed);

    // return transformedData;

    // return convertDataIntoExcel(dataPassed);


    // const transformedData = transformData([data]);
    // return exportAsExcelFile(transformedData);

    return createExcel(data);
}


exports.excelToJson = (files, headerData, formatKey, cb) => {
    uniqueKey = formatKey;

    // if(files && Array.isArray(files)){
    //     files.forEach((file) =>{
    //         uploadfiles(file)
    //     })
    // }
    // let workbook = ExcelJS.read(files.file, { type: "binary" });
    // workbook.SheetNames.forEach(sheet => {
    //     let rowObject = ExcelJS.utils.sheet_to_row_object_array(workbook.Sheets[sheet]);
    //     console.log('rowObject', rowObject);
    //     // document.getElementById("jsondata").innerHTML = JSON.stringify(rowObject,undefined,4)
    // });
    const file = files.file;
    uploadfiles(file, (path, message) => {

        if (path) {
            if (file.name.split('.')[file.name.split('.').length - 1] === 'xlsx') {
                exceltojson = xlsxtojson;
            } else {
                exceltojson = xlstojson;
            }
            try {
                exceltojson({
                    input: path,
                    output: null, //since we don't need output.json
                    lowerCaseHeaders: false
                }, function (err, result) {
                    if (err) {
                        // console.log('exceltojson', err)
                        cb(false, 'Unable to read excel');

                    } else {
                        // console.log('exceltojson', result);
                        const validateHeaders = validateHeadersOfInput(headerData, result);
                        if (!validateHeaders.error) {
                            const modifiedResult = mergeObjectBasedOnKey(headerData, result);
                            cb(true, modifiedResult)
                        } else {
                            cb(false, validateHeaders.message)
                        }

                    }
                    deleteTempFile(path);
                });
            } catch (e) {
                deleteTempFile(path);
                cb(false, 'Unable to read excel');

            }
        } else {
            deleteTempFile(path);
            cb(false, message);
        }
        // console.log('path', path);

        // var workbook = new ExcelJS.Workbook();
        // workbook.xlsx.readFile(path)
        //     .then(function() {
        //         let worksheet = workbook.getWorksheet("data1");
        //         worksheet.eachRow({ includeEmpty: false }, function(row, rowNumber) {
        //             console.log("Row " + rowNumber + " = " + JSON.stringify(row.values));
        //         });
        //     });
    });
}

// function transformData(data) {
//     const noOfRowaToGenerate = 10;
//     return data.map(({ name, values }) => {
//         const headers = values.reduce(
//             (prev, next) => ({
//                 ...prev,
//                 [next.header]: Array.isArray(next.value) ?
//                     next.value.map(({ name }) => name) : next.value,
//             }), {}
//         );
//         console.log('headers', headers);
//         console.log('array', Array(noOfRowaToGenerate).fill(headers));
//         return {
//             workSheet: name,
//             rows: Array(noOfRowaToGenerate).fill(headers),
//             headers: data
//         };
//     });
// }

// function exportAsExcelFile(workbookData) {
//     const workbook = new ExcelJS.Workbook();
//     console.log('workbookData', workbookData)
//     workbookData.forEach(({ workSheet, rows }) => {
//         const worksheet = workbook.addWorksheet(workSheet);

//         const uniqueHeaders = [
//             ...new Set(
//                 rows.reduce((prev, next) => [...prev, ...Object.keys(next)], [])
//             ),
//         ];
//         console.log('uniqueHeaders', uniqueHeaders);
//         worksheet.columns = uniqueHeaders.map((x) => ({ header: x, key: x }));
//         console.log('uniqueHeaders.map((x) => ({ header: x, key: x }))', uniqueHeaders.map((x) => ({ header: x, key: x })));
//         console.log('sheet.columns', worksheet.columns);
//         rows.forEach((jsonRow, i) => {
//             let cellValues = {...jsonRow };

//             uniqueHeaders.forEach((header, j) => {
//                 if (Array.isArray(jsonRow[header])) {
//                     cellValues[header] = '';
//                 }
//             });
//             worksheet.addRow(cellValues);
//             uniqueHeaders.forEach((header, j) => {
//                 if (Array.isArray(jsonRow[header])) {
//                     const jsonDropdown = jsonRow[header];
//                     worksheet.getCell(
//                         getSpreadSheetCellNumber(i + 1, j)
//                     ).dataValidation = {
//                         type: 'list',
//                         formulae: [`"${jsonDropdown.join(',')}"`],
//                         errorStyle: 'error',
//                         errorTitle: 'Invalid value',
//                         error: 'The value must be C&W, Pro Unlimited, Concur, AWS, Azure, Legal, Other'
//                     };
//                 }
//             });
//         });
//         // worksheet.commit();
//     });

//     // for (let index = 0; index < workbookData.length; index++) {
//     //     const workSheet = workbookData[index]['workSheet'];
//     //     const rows = workbookData[index]['rows'];
//     //     const sheet = workbook.addWorksheet(workSheet);

//     //     const uniqueHeaders = [
//     //         ...new Set(
//     //             rows.reduce((prev, next) => [...prev, ...Object.keys(next)], [])
//     //         ),
//     //     ];
//     //     console.log('uniqueHeaders', uniqueHeaders);
//     //     sheet.columns = uniqueHeaders.map((x) => ({ header: x, key: x }));
//     //     console.log('uniqueHeaders.map((x) => ({ header: x, key: x }))', uniqueHeaders.map((x) => ({ header: x, key: x })));
//     //     console.log('sheet.columns', sheet.columns);
//     //     rows.forEach((jsonRow, i) => {
//     //         let cellValues = {...jsonRow };

//     //         uniqueHeaders.forEach((header, j) => {
//     //             if (Array.isArray(jsonRow[header])) {
//     //                 cellValues[header] = '';
//     //             }
//     //         });
//     //         sheet.addRow(cellValues);
//     //         uniqueHeaders.forEach((header, j) => {
//     //             if (Array.isArray(jsonRow[header])) {
//     //                 const jsonDropdown = jsonRow[header];
//     //                 sheet.getCell(
//     //                     getSpreadSheetCellNumber(i + 1, j)
//     //                 ).dataValidation = {
//     //                     type: 'list',
//     //                     formulae: [`"${jsonDropdown.join(',')}"`],
//     //                     errorStyle: 'error',
//     //                     errorTitle: 'Invalid value',
//     //                     error: 'The value must be C&W, Pro Unlimited, Concur, AWS, Azure, Legal, Other'
//     //                 };
//     //             }
//     //         });
//     //     });
//     // }
//     return workbook;
//     // this.saveAsExcelFile(buffer, excelFileName);
// }

// function convertDataIntoExcel(data) {
//     var workbook = new ExcelJS.Workbook();
//     var worksheet = workbook.addWorksheet(data.name);
//     const headerArray = [
//         { header: "Id", key: "id", width: 5 },
//         { header: "Title", key: "title", width: 25 },
//         { header: "Description", key: "description", width: 25 },
//         { header: "Published", key: "published", width: 10 },
//     ];
//     // const headerArray = data.headerArray;
//     worksheet.columns = [...headerArray];
//     // headerArray.forEach((item, index) => {
//     //         if (item.value) {
//     //             // console.log('cellname', getCellByName(worksheet, item.value))
//     //             // worksheet.getColumn('A1').dataValidation = {
//     //             //     type: 'list',
//     //             //     allowBlank: true,
//     //             //     formulae: ['"male, female, other"'],
//     //             //     errorStyle: 'error',
//     //             //     errorTitle: 'choose a gender ',
//     //             //     error: 'must be male, female or other'
//     //             // };

//     //             worksheet.getColumn('id').eachCell({ includeEmpty: true }, function(cell, rowNumber) {
//     //                 console.log('rowNumber', rowNumber)
//     //                 cell.dataValidation = {
//     //                     type: 'list',
//     //                     allowBlank: true,
//     //                     formulae: ['"male,female,other"']
//     //                 };
//     //             });;
//     //         }

//     //     })
//     // Add Array Rows
//     return workbook;

// }

function getSpreadSheetCellNumber(row, column, addDoller = false) {
    let result = '';

    // Get spreadsheet column letter
    let n = column;
    while (n >= 0) {
        result = String.fromCharCode((n % 26) + 65) + result;
        n = Math.floor(n / 26) - 1;
    }

    // Get spreadsheet row number
    let char = addDoller ? '$' : '';
    result = result + char + `${row + 1}`;

    return result;
}

function createExcel(data) {
    let workbook = new ExcelJS.Workbook();
    let worksheet = workbook.addWorksheet(String(data.name.slice(0, 20)));
    let worksheetMasterData = workbook.addWorksheet('MasterData');

    worksheet.columns = [...data.headerArray];
    let rowsArrays = [];
    let feelBlankDataFlag = true;
    // let validationRangeArray = {};
    const rangeObj = createValidationSheet(data.headerArray, worksheetMasterData);
    if (Array.isArray(data.rows) && data.rows.length != 0) {
        // rowsArrays = data.rows;
        rowsArrays = divideAssetsIntoArray(data.rows, data.passedUniqueKey);

        // console.log('divideAssetsIntoArray', rowsArrays);
        feelBlankDataFlag = false;
    } else {
        rowsArrays = Array(number).fill('');
        feelBlankDataFlag = true;
    }

    for (let index = 0; index < rowsArrays.length; index++) {
        // const element = array[index];
        const object = {};
        data.headerArray.forEach(element => {
            if (feelBlankDataFlag) {
                object[element.key] = '';
            } else {
                if (element["isDate"] == 1) {
                    // object[element.key] = new Date(rowsArrays[index][element.header]);
                    object[element.key] = rowsArrays[index][element.header] ? new Date(moment(rowsArrays[index][element.header]).utcOffset(0, true)) : '';
                    
                } else {
                    object[element.key] = rowsArrays[index][element.header];
                }
            }

        });
        worksheet.addRow(object);

        data.headerArray.forEach((element, j) => {
            if (Array.isArray(element.value)) {
                const jsonDropdown = element.value;
                worksheet.getCell(
                    getSpreadSheetCellNumber(index + 1, j)
                ).dataValidation = {
                    type: 'list',
                    allowBlank: true,
                    showErrorMessage: true,
                    // formulae: [`"${jsonDropdown.join(',')}"`],
                    formulae: [`${rangeObj[element.key]}`],

                    errorStyle: 'error',
                    errorTitle: 'Invalid value',
                    // error: 'The value must be in ' + jsonDropdown.join(',')
                    error: 'Please enter a valid value',

                };

            }
            if (element["convertText"] == 1) {
                const cell = worksheet.getColumn(j);
                cell.eachCell((c) => { c.numFmt = '@'; });
            }
            if (element["isDate"] == 1) {
                const cell = worksheet.getColumn(j);
                cell.eachCell((c) => { c.numFmt = "dd/mm/yyyy\\ h:mm:ss\\ AM/PM"; });
            }

        });



    }
    data.headerArray.forEach((element, j) => {
        if (element["convertText"] == 1) {
            const cell = worksheet.getColumn(j + 1);
            cell.eachCell((c) => { c.numFmt = '@'; });
        }
        if (element["isDate"] == 1) {
            const cell = worksheet.getColumn(j + 1);
            cell.eachCell((c) => { c.numFmt = "dd/mm/yyyy\\ hh:mm:ss"; });
        }

    });
    return workbook;
}


function uploadfiles(file, cb) {
    // if (!fs.existsSync(excelPath)) {
    //     fs.mkdirSync(excelPath, { recursive: true });
    // }
    let baseDir = path.join(__dirname);
    const newpath = baseDir + '/' + file.name;
    file.mv(newpath)
        .then(() => {
            //console.log(path);

            cb(newpath, true);
        })
        .catch(err => {
            // console.log('errpath', err);
            cb(null, 'Unable to save file');
        });
}

// exports.readDataFromCSV = function readDataFromCSV(path, cb) {
//     csv()
//         .fromFile(path).then((result) => {
//             console.log('result', result);
//             cb(true, result);
//             deleteTempFile(path);
//         }).catch((e) => {
//             console.log('e', e);
//             deleteTempFile(path);
//             cb(false, 'unable to read file');

//         });
// }

// exports.readDataFromExcel = function readDataFromCSV(path, cb) {

// }

function deleteTempFile(path) {
    if (fs.existsSync(path)) {
        fs.unlinkSync(path);
    }
}

function divideAssetsIntoArray(data, passedUniqueKey) {
    const finalArray = [];
    data.forEach((element, index) => {
        const array = Object.keys(element);
        maxLength = 1;
        multiValueKey = {};
        firstFlag = true;
        array.forEach((key, indexj) => {
            const stringArray = String(element[key]).split('%comma%');
            if (stringArray && stringArray.length > 1) {
                if (maxLength < stringArray.length) {
                    maxLength = stringArray.length;
                }
                element[key] = stringArray;
                multiValueKey[key] = stringArray.length;
            }
        });
        while (maxLength != 0) {
            const uniqueKeyFinal = passedUniqueKey ? passedUniqueKey : uniqueKey;
            const object = getObjectFromData(element, multiValueKey, firstFlag, uniqueKeyFinal);
            element = object.element;
            multiValueKey = object.multiValueKey;
            finalArray.push(object.newObject);
            firstFlag = false;
            maxLength--;
        }
    });
    return finalArray;
}

function getObjectFromData(element, multiValueKey, firstFlag, uniqueKeyFinal) {
    const keyArray = Object.keys(element);
    const newObject = {};
    keyArray.forEach((key, index) => {
        if (multiValueKey[key] && multiValueKey[key].length != 0) {
            newObject[key] = String(element[key].splice(0, 1)[0]).trim();
            multiValueKey[key]--;
        } else {
            newObject[key] = firstFlag || key == uniqueKeyFinal ? element[key] : '';
        }
    });
    return { element: element, multiValueKey: multiValueKey, newObject: newObject }
}

function mergeObjectBasedOnKey(headerData, data) {
    const finalArray = [];
    const object = {};
    const headerDataSingleSelect = {};
    headerData.forEach((element) => {
        headerDataSingleSelect[element.header] = element.isSingleSel;
    })
    // console.log('headerDataSingleSelect', headerDataSingleSelect);
    data.forEach((element, index) => {
        if (element[uniqueKey] in object) {
            //    const internalObject =  object[uniqueKey];
            const keyArray = Object.keys(element);
            keyArray.forEach((key, indexj) => {
                if (element[key] && key != uniqueKey) {
                    if (headerDataSingleSelect[key] != 1) {
                        const array = object[element[uniqueKey]][key].split('%comma%');
                        if (array.indexOf(String(element[key]).trim()) == -1) {
                            object[element[uniqueKey]][key] = object[element[uniqueKey]][key] == null || object[element[uniqueKey]][key] == '' ? element[key] : object[element[uniqueKey]][key] + '%comma%' + element[key];
                        }
                    } else {
                        object[element[uniqueKey]][key] = element[key];
                    }
                }
            })
        } else {
            object[element[uniqueKey]] = element;
        }
    });

    // console.log('object', object);

    for (key in object) {
        finalArray.push(object[key]);
    }

    return finalArray;
}

function createValidationSheet(headerArray, worksheetMasterData) {
    if (!headerArray || !Array.isArray(headerArray)) {
        return {};
    }
    if (!worksheetMasterData) {
        return {};
    }
    const valueMapping = {};
    const rangeArray = {};
    let maxValue = 0;
    for (let index = 0; index < headerArray.length; index++) {
        if (headerArray[index]["value"]) {
            const element = headerArray[index];
            const valueArray = element["value"];
            valueMapping[element["key"]] = {
                value: valueArray,
                length: valueArray.length,
            }

            if (maxValue < valueArray.length) {
                maxValue = valueArray.length;
            }
        }
    }

    const masterDatakeys = Object.keys(valueMapping);
    const keyArray = [];
    // const worksheetMasterDataList = [];
    masterDatakeys.forEach((element, index) => {
        keyArray.push({ "id": element });
        let colStart = getSpreadSheetCellNumber(0, index, true);
        let colEnd = getSpreadSheetCellNumber(valueMapping[element]['value'].length - 1, index, true);
        // console.log('colStart', colStart);
        // console.log('colEnd', colEnd)
        rangeArray[element] = 'MasterData!' + '$' + colStart + ':' + '$' + colEnd;
    });
    //  console.log('rangeArray', rangeArray);
    worksheetMasterData.columns = keyArray;
    for (let index = 0; index < maxValue; index++) {
        // const element = array[index];
        const array = [];
        masterDatakeys.forEach((element) => {
            // keyArray.push({"id": element})
            if (index < valueMapping[element]['value'].length) {
                // const element = ;
                array.push(replceCharecters(String(valueMapping[element]['value'][index])));
            } else {
                array.push('');
            }
        });

        // worksheetMasterDataList.push(object);
        worksheetMasterData.addRow(array);
    }
    // console.log('worksheetMasterDataList',worksheetMasterDataList);
    return rangeArray;
}

function validateHeadersOfInput(headerArray, excelData) {
    if (excelData && Array.isArray(excelData) && excelData.length != 0) {
        const excelDataHeaders = Object.keys(excelData[0]);
        let invalid = false;
        if (headerArray.length == excelDataHeaders.length) {
            let message = '';
            for (let index = 0; index < headerArray.length; index++) {
                const element = headerArray[index].header;
                // if(String(element).toLowerCase().trim() != String(excelDataHeaders[index]).toLowerCase().trim()){
                //     invalid = true;
                //     // break;
                //   if(message !=''){
                //     message = message + ',';
                //   }
                //   message = message + element;
                // }
                if (excelDataHeaders.indexOf(element) == -1) {
                    invalid = true;
                    // break;
                    if (message != '') {
                        message = message + ',';
                    }
                    message = message + element;
                }
            }
            if (invalid) {
                message = message + ' are invalid';
                return { error: true, message: message, data: [] }
            } else {
                return { error: false, message: '', data: [] }
            }
        } else {
            return { error: true, message: 'Invalid Headers', data: [] }
        }
    } else {
        return { error: true, message: 'data is empty', data: [] }
    }
}

function replceCharecters(element) {
    element = element.replaceAll("&quot", '"');
    element = element.replaceAll("&slash", "\\");
    return element;
}