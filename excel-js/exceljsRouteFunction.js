const exceljsHelper = require('./exceljsHelper')

exports.jsonToExcel = (req, res) => {
    const object = req.body;
    const workbook = exceljsHelper.jsonToExcel(object);
    // res.status(200).json({ type: true, body: req.body });

    // res is a Stream object
    res.setHeader(
        "Content-Type",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    );
    res.setHeader(
        "Content-Disposition",
        "attachment; filename=" + object["name"] + ".xlsx"
    );
    return workbook.xlsx.write(res).then(function() {
        res.status(200).end();
    });
}

exports.excelToJson = (req, res, next) => {

    var body = req.body;
    // console.log(req.files);
    if (!req.files) {
        res.status(500).json({ type: false, data: 'Please select a file' });
        return;
    }
    exceljsHelper.excelToJson(req.files, (status, result) => {
        if (status) {
            res.status(200).json({ type: status, data: result })
        } else {
            res.status(500).json({ type: status, data: result })
        }
    });


}

exports.populateDate = (req, res, next) => {

    var { data } = req.body;
}