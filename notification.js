const pushnotification = require('./pushnotification');
const mail = require('./mail');
const sms = require('./sms');
const logmanage = require('./manage');
var db = null;
var schedule = require('node-schedule'); // this is for schedular
var requestApi = require('request');
var config = require('./config');
var RFInUse = false;
exports.userPushNotification = (request, response) => {
    const fromHr = request.body.fromHr;
    const toHr = request.body.toHr;
    db = request.db;
    var defaultFromHr = -1;
    var defaultToHr = -1;
    if (String(fromHr)) {
        defaultFromHr = fromHr;
    }
    if (String(toHr)) {
        defaultToHr = toHr;
    }
    var fromDate = new Date().addHours(defaultFromHr);
    var toDate = new Date().addHours(defaultToHr);
    var modeId = 3;
    var sql = "call SP_GET_Notification_Data(?,?,?)";
    db.query(sql, [fromDate, toDate, modeId], function(err, success) {

        if (err) {
            response.json({
                type: false,
                data: "Error Occurred: " + err
            });
        } else {
            if (success[0].length > 0) {
                var list = success[0];
                OneToOneNotification(list, 0, (result) => {
                    response.json({
                        type: true,
                        data: result
                    });
                })
            } else {
                response.json({
                    type: true,
                    data: []
                });
            }
        }
    })

};
Date.prototype.addHours = function(h) {
        this.setHours(this.getHours() + h);
        return this;
    }
    /**
     * Save User Devices Info
     * 
     */

     exports.registeruserinfo = (request, response) => {
        var params = request.body;
        var db = request.db;
        var platFormId = params.platformId ? params.platformId : '';
        var sql = "call SP_SAVE_User_Devices(?,?,?,?,?,?,?,?,?,?,?)";
        db.query(sql, [
            params.userid,
            params.appid,
            params.model,
            params.platform,
            params.version,
            params.pushid,
            params.pushplayerid,
            params.uuid,
            params.appVersionNumber,
            params.appVersionCode,
            platFormId
        ], function(err, success) {

            if (err) {
                response.json({
                    type: false,
                    data: "Error Occurred: " + err
                });
            } else {
                response.json({
                    type: true,
                    data: success[0]
                });
            }
        })
    }
    /**
     * Many User and Single Message (notification)
     */
    OneToOneNotification = (list, i, cb, response) => {
        try {
            if(!response){
                response = {
                    accepted:  [],
                    rejected: []
                };
            }
        
            if (i == list.length || list.length == 0) {
                cb(response);
            } else {
                var data = list[i];
                if (data.notModeId == 1) { // For SMS
                    if (data.phone && data.message) {
                        var message = data.message;
                        sms.sendSMS(data.phone, message, (result) => {
                            updateSentStatus(data.id);
                            OneToOneNotification(list, i + 1, cb, response);
                        });
                    } else {
                        updateSentStatus(data.id);
                        OneToOneNotification(list, i + 1, cb, response);
                    }
                } else if (data.notModeId == 2) { //For Email
                    if (data.title && data.email && data.message) {
                        var to = [];
                        var subject = data.title;
                        var messages = data.message;
                        var username = data.username;
                        to.push(data.email);
                        mail.sendMail(to, subject, messages, username, (result) => {
                            if(result && Array.isArray(result.accepted) && result.accepted.length > 0){
                                if(response){
                                    response.accepted.push(result.messageId)
                                }
                            } else if(result && Array.isArray(result.rejected) && result.rejected.length > 0){
                                if(response){
                                    response.rejected.push(result.messageId || 'email-error-'+index);
                                }
                            }
                            /**
                             * Commented By Bhavesh Tandel 24th Nov 2021
                             */
                            // updateSentStatus(data.id);
                            // OneToOneNotification(list, i + 1, cb, response);
                        })
                        /**
                         * Added by Bhavesh Tandel on 24th Nov 2021 and added settimeout function
                         */
                        setTimeout(() =>{
                            updateSentStatus(data.id);
                            OneToOneNotification(list, i + 1, cb, response);
                        }, 300);
                    } else {
                        updateSentStatus(data.id);
                        OneToOneNotification(list, i + 1, cb, response);
                    }
                } else if (data.notModeId == 3) { //For Mobile Push Notification
                    data.playersIds = [];
                    data.playersIds.push(list[i].pushplayerid);
                    pushnotification.ActiveUserNotification(data, (err, res1) => {
                        // console.log(response);
                        // cb(response);
                        updateSentStatus(data.id);
                        OneToOneNotification(list, i + 1, cb, response);
                    });
                } else {
                    updateSentStatus(data.id);
                    OneToOneNotification(list, i + 1, cb, response);
                }
            }
        } catch (error) {
            logmanage.inserterrorlog('OneToOneNotification', error.message, 'CRON', null);
            OneToOneNotification(list, i + 1, cb, response);
        }
    }
    
    updateSentStatus = (id) => {
            var sql = "call SP_UPDATE_SENT_NOTIFICATION(?)";
            db.query(sql, [id], function(err, success) {
    
                if (err) {
                    // logmanage.savedErrorLog("Update Sent Flag ", "\n" + JSON.stringify(err));
                    logmanage.inserterrorlog('UpdateSentStatus', err.message, 'CRON', id);
                }
            })
        }
    /**
     * Single User and Many Message (notification)
     */
exports.ManyToOneNotification = (data, cb) => {

    }
    /* SCHEDULAR FOR SEND NOTIFICATION*/
exports.callshedular = function(db) {

    if (config.overallcroneflag) {
        var rule1 = new schedule.RecurrenceRule();
        //rule1.second = 01;
        rule1.minute = 01;
        //rule1.hour = 0;

        var j = schedule.scheduleJob(rule1, function() {
            console.log('cron excute', rule1.minute); //,':',rule1.second);
            processlog(db, "Notification Sent 15min cron", "Cron execute for " + rule1.minute, "successfully", "AUTO", 0);
            let tdata = {
                'fromHr': -2,
                'toHr': 1,
                'db': db,
                'crontime': 1
            }
            if (!RFInUse) {
                send_userNotification(tdata);
            }
        });

        var rule2 = new schedule.RecurrenceRule();
        //rule2.second = 02;
        rule2.minute = 16;
        //rule2.hour = 0;

        var k = schedule.scheduleJob(rule2, function() {
            console.log('cron excute', rule2.minute) //,':',rule2.second);
            processlog(db, "Notification Sent 15min cron", "Cron execute for " + rule2.minute, "successfully", "AUTO", 0);
            let tdata = {
                'fromHr': -2,
                'toHr': 1,
                'db': db,
                'crontime': 16
            }
            if (!RFInUse) {
                send_userNotification(tdata);
            }
        });

        var rule3 = new schedule.RecurrenceRule();
        // rule3.second = 0;
        rule3.minute = 31;
        // rule3.hour = 0;

        var l = schedule.scheduleJob(rule3, function() {
            console.log('cron excute', rule3.minute);
            processlog(db, "Notification Sent 15min cron", "Cron execute for " + rule3.minute, "successfully", "AUTO", 0);
            let tdata = {
                'fromHr': -2,
                'toHr': 1,
                'db': db,
                'crontime': 31
            }
            if (!RFInUse) {
                send_userNotification(tdata);
            }
        });

        var rule4 = new schedule.RecurrenceRule();
        // rule4.second = 0;
        rule4.minute = 46;
        // rule4.hour = 0;

        var m = schedule.scheduleJob(rule4, function() {
            console.log('cron excute', rule4.minute);
            processlog(db, "Notification Sent 15min cron", "Cron execute for " + rule4.minute, "successfully", "AUTO", 0);
            let tdata = {
                'fromHr': -2,
                'toHr': 1,
                'db': db,
                'crontime': 46
            }
            if (!RFInUse) {
                send_userNotification(tdata);
            }
        });
    }
}

send_userNotification = function(request) {
    db = request.db;
    processlog(db, "Notification Sent 15min cron", "send userNotification process start for " + request.crontime, "successfully", "AUTO", 0);
    RFInUse = true;
    const fromHr = request.fromHr;
    const toHr = request.toHr;
    var modeId = 3; // 3 for push notification 2 for mail and 1 for sms
    var defaultFromHr = -1;
    var defaultToHr = -1;
    if (String(fromHr)) {
        defaultFromHr = fromHr;
    }
    if (String(toHr)) {
        defaultToHr = toHr;
    }
    var dt = new Date();
    var fDate = new Date().setHours(dt.getHours() + (defaultFromHr));
    var tDate = new Date().setHours(dt.getHours() + (defaultToHr));
    var fromDate = GetFormattedDate(fDate);
    var toDate = GetFormattedDate(tDate);
    var sql = "call SP_GET_Notification_Data(?,?,?)";
    db.query(sql, [fromDate, toDate, modeId], function(err, success) {

        if (err) {
            // response.json({
            //     type: false,
            //     data: "Error Occurred: " + err
            // });
            processlog(db, "Notification Sent 15min cron", "get notification records for " + request.crontime, "err" + err.message, "AUTO", 0);
            RFInUse = false;
        } else {
            if (success[0].length > 0) {
                processlog(db, "Notification Sent 15min cron", "get notification records for " + request.crontime, "sucefull with " + success[0].length + " no of record found", "AUTO", 0);
                var list = success[0];
                try {
                    OneToOneNotification(list, 0, (result) => {
                        // response.json({
                        //     type: true,
                        //     data: result
                        // });
                        processlog(db, "One To One Notification", "Total number of accepted result " + result.accepted.length, "successfully", "AUTO", 0);
                        processlog(db, "One To One Notification", "Total number of rejected result " + result.rejected.length, "successfully", "AUTO", 0);
                        console.log('done all call ...!!!');
                        processlog(db, "Notification Sent 15min cron", "sent notification Done for " + request.crontime, "successfully ...!!! ", "AUTO", 0);
                        RFInUse = false;
                    })
                } catch (error) {
                    processlog(db, "Notification Sent 15min cron", "get notification records for " + request.crontime, "err" + error.message, "AUTO", 0);
                    RFInUse = false;
                }
            } else {
                // response.json({
                //     type: true,
                //     data: []
                // });
                processlog(db, "Notification Sent 15min cron", "sent notification Done for " + request.crontime, "nodata found ...!!! ", "AUTO", 0);
                console.log('nodata found ...!!!');
                RFInUse = false;
            }
        }
    });

};


function GetFormattedDate(date) {
    var HH = '00';
    var MM = '00';
    var SS = '00';
    if (!date) {
        var todayTime = new Date();
        var month = todayTime.getMonth() + 1;
        var day = todayTime.getDate();
        var year = todayTime.getFullYear();
    } else {
        var todayTime = new Date(date);
        var month = todayTime.getMonth() + 1;
        var day = todayTime.getDate();
        var year = todayTime.getFullYear();
        HH = todayTime.getHours();
        MM = todayTime.getMinutes();
        SS = todayTime.getSeconds();
    }


    if (day < 10) {
        day = "0" + day
    }
    if (month < 10) {
        month = "0" + month
    }
    return year + "-" + month + "-" + day + " " + HH + ":" + MM + ":" + SS;
}

exports.generateDynamicLink = (request, response) => {

    const reqParams = request.body;
    console.log('reqParams : ', reqParams);
    reqParams['platform'] = 3;
    exports.generateFirebaseDynamicLink(reqParams, function(err, resp, body) {
        if (err) {
            response.json({
                type: false,
                data: "Error Occurred: " + err
            });
        } else {
            // response.json({
            //     type: true,
            //     // data: resp,
            //     // resBody: body,
            //     statusCode: resp.statusCode,
            //     data: body,
            //     // data: body.shortLink
            // });
            var params = {
                areaId: reqParams.areaId,
                instanceId: reqParams.instanceId,
                redirectUrl: body.shortLink,
            }
            exports.updateFirebaseDynamicLink(params, request.db, function(result) {
                if (!result) {
                    response.json({
                        type: false,
                        data: "Error Occurred: " + err
                    });
                } else {
                    response.json({
                        type: true,
                        // data: resp,
                        // resBody: body,
                        statusCode: resp.statusCode,
                        data: body,
                        // data: body.shortLink
                    });
                }
            });
        }
    });
}

exports.generateSignupDynamicLink = (request, response, next) => {

    const reqParams = request.body;
    console.log('reqParams : ', reqParams);

    exports.generateFirebaseDynamicLink(reqParams, function(err, resp, body) {
        if (err) {
            response.json({
                type: false,
                data: "Error Occurred: " + err
            });
        } else {
            // response.json({
            //     type: true,
            //     // data: resp,
            //     // resBody: body,
            //     statusCode: resp.statusCode,
            //     data: body,
            //     // data: body.shortLink
            // });
            // var params = {
            //     redirectUrl: body.shortLink,
            // }
            // response.json({
            //     type: true,
            //     data: body.shortLink,
            //     statusCode: resp.statusCode,
            //   });

            request.body['shortLink'] = body.shortLink;
            next();
        }
    });
}

exports.generateFirebaseDynamicLink = (data, cb) => {
    const reqParams = data;
    console.log('reqParams : ', reqParams);
    let tempRedirectUrl;
    if (reqParams['platform'] == 3) {
        if (config.serverType === 'saas' && reqParams['portalUrl'] && reqParams['portalUrl'] != '') {
            tempRedirectUrl = 'https://' + reqParams.portalUrl + '/#/' + reqParams.link;
        } else {
            tempRedirectUrl = config.learnerPortalDomain.domain + reqParams.link;
        }
    } else {
        tempRedirectUrl = config.adminPortalDomain.domain + reqParams.link;
    }

    console.log('final redirection url : ', tempRedirectUrl);
    const tempUrl = 'https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=' + config.firebase.apikey;

    // const tempBody = {
    //     "dynamicLinkInfo": {
    //         "domainUriPrefix": config.firebase.dynamicLinkDomain,
    //         "link": request.link,
    //         "androidInfo": {
    //             "androidPackageName": config.firebase.bundleId
    //         },
    //         "iosInfo": {
    //             "iosBundleId": config.firebase.bundleId
    //         }
    //     },
    //     "suffix": {
    //         "option": "SHORT"
    //     }
    // };

    var tempBody = config.firebase.dynamicLinkBodyParams;
    // tempBody.dynamicLinkInfo.link = reqParams.link;
    tempBody.dynamicLinkInfo.link = tempRedirectUrl;

    const options = {
        uri: tempUrl,
        method: 'POST',
        json: tempBody
    };

    requestApi.post(options, function(err, resp, body) {
        cb(err, resp, body);
    });
}

exports.updateFirebaseDynamicLink = (data, db, cb) => {
    console.log('Update params :', data);
    var tempTableName = '';
    var tempColumnName = '';
    if (data.areaId === 3) {
        tempTableName = 'edge_workflows';
        tempColumnName = 'workflowid';
    } else if (data.areaId === 2) {
        tempTableName = 'edge_course';
        tempColumnName = 'courseId';
    } else if (data.areaId === 5) {
        tempTableName = 'edge_course_activities';
        tempColumnName = 'activityId';
    } else if (data.areaId === 21) {
        tempTableName = 'edge_cc_calls';
        tempColumnName = 'id';
    } else if (data.areaId === 29) {
        tempTableName = 'edge_workflow_instance_steps';
        tempColumnName = 'id';
    }
    // var sql = "call SP_change_col_value('edge_course','courseId',?,'redirectionLink',?,1);";
    var sql = "call SP_change_col_value(" + "'" + tempTableName + "'" + "," + "'" + tempColumnName + "'" + ",?,'redirectionLink',?,1);";
    console.log('final sp : ', sql);
    db.query(sql, [data.instanceId, data.redirectUrl], function(err, success) {
        if (err) {
            cb(null);
        } else {
            cb(success);
        }
    });
}

function processlog(db, type, activity, desc, trig, tId) {
    var resourceSQL = "call SP_Automated_Process_log(?,?,?,?,?)";
    db.query(resourceSQL, [type, activity, desc, trig, tId], function(err, res) {
        if (err) {
            // console.log('err ', err);
        } else {
            //  console.log('res ', res);

        }
    });
}

exports.callcochingdynamiccrongenration = async(db, inst) => {
    var type = 1;
    if (inst > 0) {
        type = 3
    }
    var resourceSQL = "call SP_GET_Notification_cc_cron_data(?,?,?)";
    var fromDate = GetFormattedDate(new Date().addHours(0));
    // fromDate = new Date();
    console.log('fromDate', fromDate);
    db.query(resourceSQL, [fromDate, type, inst], function(err, res) {
        if (err) {
            // console.log('err ', err);
        } else {
            //  console.log('res ', res);
            if (res[0].length > 0) {
                var list = res[0];
                list.forEach(function(item) {
                    if (item.senddate) {
                        console.log('item ', item);
                        var date = new Date(item.senddate)
                        var x = new multishedular(db, date);
                    }
                });
            }
        }
    });
}
class multishedular {
    constructor(db, dttime) {
        var k = schedule.scheduleJob(dttime, function() {
            let tdata = {
                'fromHr': 0,
                'toHr': 0,
                'db': db,
                'crontime': 'cc'
            }
            send_userNotification_cc(tdata);
        });
    }
};

send_userNotification_cc = function(request) {

    db = request.db;
    processlog(db, "Notification Sent 15min cron", "send userNotification process start for " + request.crontime, "successfully", "AUTO", 0);
    //RFInUse = true;
    var fromDate = GetFormattedDate(new Date());
    var sql = "call SP_GET_Notification_cc_cron_data(?,?,?);";
    db.query(sql, [fromDate, 2, 0], function(err, success) {

        if (err) {
            processlog(db, "Notification Sent 15min cron", "get notification records for " + request.crontime, "err" + err.message, "AUTO", 0);
        } else {
            if (success[0].length > 0) {
                processlog(db, "Notification Sent 15min cron", "get notification records for " + request.crontime, "sucefull with " + success[0].length + " no of record found", "AUTO", 0);
                var list = success[0];
                try {
                    console.log('OneToOneNotification ', list.length);
                    OneToOneNotification(list, 0, (result) => {
                        console.log('OneToOneNotification', 'done all call ...!!!');
                        processlog(db, "Notification Sent 15min cron", "sent notification Done for " + request.crontime, "successfully ...!!! ", "AUTO", 0);
                    })
                } catch (error) {
                    processlog(db, "Notification Sent 15min cron", "get notification records for " + request.crontime, "err" + error.message, "AUTO", 0);
                }
            } else {
                processlog(db, "Notification Sent 15min cron", "sent notification Done for " + request.crontime, "nodata found ...!!! ", "AUTO", 0);
                console.log('nodata found ...!!!');
            }
        }
    });

};