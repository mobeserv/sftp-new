'use strict';

// var schedule = require('node-schedule');
// var Client = require('ssh2-sftp-client');
// var config = require('../../../config');
// var globalpath = __dirname + '/sftpfiles/';
// var csv = require('csvtojson');
// var TwinBcrypt = require('twin-bcrypt');
// var salt = TwinBcrypt.genSalt(10);
var cc_notification = require('./notification');

exports.notificationcron = function(server, db) {
  
    try {
        // var db = req.db;
       
        var sql = 'call SP_GET_Tenant_List()';
        db.query(sql, [], function(err, list) {
            if (err) {
                processlog(db, 'Error in Get tenant list ', "Get tenant list", 'Error'+ err.message, 'AUTO',0);
            } else {
                processlog(db, 'Employee Enrol Notification', "Get tenant list", 'sucessfull', 'AUTO',0);
                if (list[0].length > 0) {
                    Notification_fro_single_tenant(db, list[0], 0,[],[],[],'AUTO', function(sucessfull, unsuccessfull, resions) {
                        //console.log("Done all calls!",userList.length);
                        //console.log("Done all calls!",unsuccessfuluser);
                        //console.log("Done all calls!",resions);
                        var donedata = {
                            Total_tenant_found: list[0].length,
                            sucessfull_enrolmentfor_tenant: sucessfull.length,
                            unsucessfull_enrolmentfor_tenant: unsuccessfull.length,
                            resions_list: resions
                        }
                        processlog(db, 'Employee Enrol Notification', "done all calls ", JSON.stringify(donedata), 'AUTO',0);
                        // send_response(db, res, undefined, donedata);
                        cc_notification.callcochingdynamiccrongenration(db,0);
                    });
                }
                else
                {
                    processlog(db, 'Tenant list result', "NO data Found", 'sucessfull', 'AUTO',0);
                }
            }
        })
    } catch (e) {
        processlog(db, 'Error in Get tenant list in try block', "Get tenant list", 'Error'+ e.message, 'AUTO',0);
     
    }

}


exports.notification_manual = function(req, res) {

    
    try {
        var db = req.db;
        var sql = 'call SP_GET_Tenant_List()';
        db.query(sql, [], function(err, list) {
            if (err) {
                processlog(db, 'Error in Get tenant list ', "Get tenant list", 'Error'+ err.message, 'manual',0);
            } else {

                if (list[0].length > 0) {
                    Notification_fro_single_tenant(db, list[0], 0,[],[],[],'Manual', function(sucessfull, unsuccessfull, resions) {
                        //console.log("Done all calls!",userList.length);
                        //console.log("Done all calls!",unsuccessfuluser);
                        //console.log("Done all calls!",resions);
                        var donedata = {
                            Total_tenant_found: list[0].length,
                            sucessfull_enrolmentfor_tenant: sucessfull.length,
                            unsucessfull_enrolmentfor_tenant: unsuccessfull.length,
                            resions_list: resions
                        }
                        processlog(db, 'Employee Enrol Notification', "done all calls ", JSON.stringify(donedata), 'Manual',0);
                        send_response(req, res, undefined, donedata);
                    });
                }
                else
                {
                    processlog(db, 'Tenant list result', "NO data Found", 'sucessfull', 'manual',0);
                }
            }
        })
    } catch (e) {
        processlog(db, 'Error in Get tenant list in try block', "Get tenant list", 'Error'+ e.message, 'manual',0);
     
    }


}

function Notification_fro_single_tenant(newdb, tenatlist, i ,sucessfull, unsuccessfull, resions, type, callback) {
    processlog(newdb, 'Entered Notification_fro_single_tenant', "Entered", i+'Entered Successfully'+tenatlist.length, 'auto',0);
    if (i == tenatlist.length || tenatlist.length === 0)
        return callback(sucessfull, unsuccessfull, resions);
        var db = newdb;
    processlog(db, 'Notification employee for tenant'+tenatlist[i].tenantName , "function call", 'sucessfull', type,tenatlist[i].tenantId);
    try {
        //console.log(tenatlist[i].tenantId ,'--',tenatlist[i].tenantName )
        var db = newdb;
        var sql = 'call SP_TRG_COURSE_Notification_by_tenant(?)';
        db.query(sql, [tenatlist[i].tenantId], function(err, list) {
            if (err) {
                unsuccessfull.push(tenatlist[i]);
                resions.push({ tenantId: tenatlist[i].tenantId, err: err });
            } else {
                sucessfull.push(tenatlist[i]);
                resions.push({ tenantId: tenatlist[i].tenantId, err: err });
            }
            Notification_fro_single_tenant(newdb, tenatlist, i + 1,sucessfull, unsuccessfull, resions, type, callback);
         })
    } catch (e) {
        processlog(db, 'Error at Notification employee start for tenant'+tenatlist[i].tenantName+' in try block', "Notification employee", 'Error'+ e.message, type,tenatlist[i].tenantId);
     
    }
}
//console.log(GetFormattedDate());

function GetFormattedDate(date) {
    if (date) {
        var todayTime = new Date(date);
        var month = todayTime.getMonth() + 1;
        var day = todayTime.getDate();
        var year = todayTime.getFullYear();
    } else {
        var todayTime = new Date();
        var month = todayTime.getMonth() + 1;
        var day = todayTime.getDate() - 1;
        var year = todayTime.getFullYear();
    }


    if (day < 10) {
        day = "0" + day
    }
    if (month < 10) {
        month = "0" + month
    }
    return year + "" + month + "" + day;
}

function processlog(db, type, activity, desc, trig,tId) {
    var resourceSQL = "call SP_Automated_Process_log(?,?,?,?,?)";
    db.query(resourceSQL, [type, activity, desc, trig,tId], function(err, res) {
        if (err) {
            // console.log('err ', err);
        } else {
            //  console.log('res ', res);

        }
    });
}

function send_response(req, res, err, data) {
    if (err) {
        res.json({
            type: false,
            data: "Error occured: " + err
        });
    } else {
        res.json({
            type: true,
            Data: data
        });
    }
}