var port = 9090;
const config = require('./config');
var express = require('express'),
    cors = require('cors'),
    server = express(),
    mysql = require('mysql'),
    bodyParser = require("body-parser"),
    app = require('http').Server(server),
    SFTP = require('./SFTPemployee');
    SFTProle = require('./SFTPemprolechange');
    var pl = require('./processLog')
    var fileupload = require('express-fileupload');

server.set('port', process.env.PORT || port);

var db = mysql.createConnection(config.db);


db.connect(function(error) {
    if (error) {
        pl.processlog(db, "addusercron db connection", "DB Not Connected", error.message, 'AUTO', 0);
        console.log(error);
    } else {
        console.log("Db Connected.");
        pl.processlog(db, "addusercron db connection", "DB Connected", "successfully", 'AUTO', 0);

        SFTP.addusercron(app, db);
    }
});


server.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    req.db = db;
    next();
    
});
//server.use(fileUpload());
server.use(bodyParser.urlencoded({ extended: false, limit: '500mb' }));
server.use(bodyParser.json({ limit: '500mb' }));
server.use(cors());
server.use(fileupload());


app.listen(port, function() {
        console.log('Example app listening on port ', port, '!')
    })
    var router = express.Router();
    // server.get('/api/admin/sftp_maual',SFTP.sftpUserAdd_manual);

    server.get('/', function(req, res, next) {
        //res.render('index', { title: 'Edge app api' });
        res.json({
                          type: true
                      });
      });
      function changeDate(value) {
        if (value == null || value == '') {
            return value
        } else {
            const splitarray = value.includes('/') ? value.split('/') : value.split('-');
            const date = splitarray.reverse().join('-');
            return new Date(date);
        }
    };
    console.log(changeDate('10-04-2015'));

    var routes = require('./routes/routes'); //importing route

    routes(server, db);