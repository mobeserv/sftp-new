'use strict';
const ErrorLog = require("./manage");
const exceljs = require('./excel-js/exceljsHelper');
const dirPath = require('path').resolve('./');
var config = require("./config");
const AWS = require("aws-sdk"); // from AWS SDK
const FILE_PERMISSION = config.s3.acl.public
const BUCKET = config.s3.bucket;
const KEY = config.s3.accesskey;
const SECRET = config.s3.secretkey;
const s3 = new AWS.S3({ signatureVersion: 'v4', accessKeyId: KEY, secretAccessKey: SECRET });
const fs = require('fs');
const QP = require('./QP');
const mail = require('./mail');
const Mailgen = require('mailgen');
const nodemailer = require('nodemailer');
const options = config.smtp;
let transport = nodemailer.createTransport(options)
var pl = require('./processLog');
const { resolve } = require("path");

exports.insertCronandSFTPLinks = async function (db, flag) {
    var userId = null;
    var sendflag = 0;
    try {
        console.log('insertCronandSFTPLinks starts')
        const sql = "call SP_GET_Tenant_List()";
        db.query(sql, [], async function (err, result) {

            if (err) {
                pl.processlog(db, 'SP_GET_Tenant_List  ', "function call ", 'Error' + err.message, 'AUTO', 0);
            }
            else {
                console.log('insertCronandSFTPLinks starts res',flag, result)
                var b = await exports.createlinkreports(db, result, flag);

            }
        })

    } catch (e) {
        console.log(e);
        pl.processlog(db, 'insertCronandSFTPLinks Started ', "function call ", 'Error' + e.message, 'AUTO', 0);

    }
}


exports.createlinkreports = async (db, result, flag) => {
    console.log('createlinkreports',flag, result)
    return new Promise(async (resolve, reject) => {

        var tenantArray = [];
        if (result && result[0] && result[0].length > 0) {
            tenantArray = result[0];
            await asyncForEach(tenantArray, async (item) =>
            //for(let j = 0 ; j< tenantArray.length ;  j++)
            {
                let tId = item.tenantId;//tenantArray[j];
                if (flag == 2) {
                    try {
                        pl.processlog(db, 'get_detail_enrollment_report  ', "function call ", 'start sucessfull', 'AUTO', tId);
                        var result_enroll = await exports.get_detail_enrollment_report(db, tId);
                    }
                    catch (e) {
                        console.log(e);
                        pl.processlog(db, 'get_detail_enrollment_report Started ', "function call ", 'Error' + e.message, 'AUTO', tId);
                    }

                    try {
                        pl.processlog(db, 'get_overall_enrollment_report  ', "function call ", 'start sucessfull', 'AUTO', tId);
                        var result_overall_enroll = await exports.get_overall_enrollment_report(db, tId);
                    }
                    catch (e) {
                        console.log(e);
                        pl.processlog(db, 'get_overall_enrollment_report Started ', "function call ", 'Error' + e.message, 'AUTO', tId);
                    }
                }
                else if (flag == 1) {
                    try {
                        pl.processlog(db, 'get_sftp_status_report  ', "function call ", 'start sucessfull', 'AUTO', tId);
                        var result_sftp_enroll = await exports.get_sftp_status_report(db, tId);
                    }
                    catch (e) {
                        console.log(e);
                        pl.processlog(db, 'get_sftp_status_report Started ', "function call ", 'Error' + e.message, 'AUTO', tId);
                    }
                }
            })
            console.log('sendmailfunction before',flag)
            exports.sendmailfunction(db, flag);
        }


    })
}

exports.sendmailfunction = async (db, flag) => {
    console.log('sendmailfunction', flag)
    if (flag == 1) {
        try {;
            console.log('sendnotificationforsftpstatusreport')
            await exports.sendnotificationforsftpstatusreport(db);
        }
        catch (e) {
            pl.processlog(db, 'sendnotificationforsftpstatusreport Started ', "function call ", 'Error' + e.message, 'AUTO', tId);
        }
    }
    else if (flag == 2) {
        try {
            await exports.sendnotificationforcronreport(db);
        }
        catch (e) {
            pl.processlog(db, 'sendnotificationforsftpstatusreport Started ', "function call ", 'Error' + e.message, 'AUTO', tId);
        }
    }
}

exports.get_overall_enrollment_report = async (db, tenantId) => {
    return new Promise((resolve, reject) => {
        const newdb = db;
        const SQL = 'call sp_get_overall_enrollment_report(?);';
        try {
            newdb.query(SQL, [tenantId], async function (err, result) {
                if (result && result.length > 0) {

                    const timestamp = Date.now();
                    const dateObject = new Date(timestamp);
                    const date = dateObject.getDate();
                    const month = dateObject.getMonth() + 1;
                    const year = dateObject.getFullYear();
                    var datetime = `${year}-${month}-${date}`;
                    var fileName = 'overall_' + datetime + '_' + tenantId + 'abc'
                    const data = {
                        "name": fileName,
                        "headerArray": parseJsonFun(result[0], 1),
                        "rows": result[1]
                    };

                    const object = data;
                    const workbook = exceljs.jsonToExcel(object);
                    workbook.removeWorksheet('MasterData');
                    //let filepath = dirPath + "/outputs/" + fileName + ".xlsx";
                    let filepath = config.mntdir + "/cronreports/" + fileName +".xlsx";
                    var filefors3 = {
                        path: filepath,
                        name: fileName + ".xlsx"
                    }

                    await workbook.xlsx
                        .writeFile(filepath)
                        .then(async () => {
                            await uploadtos3(filefors3, db, tenantId, 1);
                            resolve(1);
                        }
                        );

                } else {
                    errorlog.inserterrorlog('get_overall_enrollment_report', 'No data available', 'API', null);
                    resolve({err: true, data: 'No data available'})
                }
            })
        }
        catch (err) {
            console.log(err.message);
            errorlog.inserterrorlog('get_overall_enrollment_report', err.message, 'API', null);
            resolve({err: true, data: err.message})
        }
    })
}

exports.get_detail_enrollment_report = async (db, tenantId) => {
    return new Promise((resolve, reject) => {
        const newdb = db;
        const SQL = 'call sp_get_detail_enrollment_report(?);';
        try {
            // await qP.queryPromise(SQL, [sectionId, tenantId]).then((result) => {
            newdb.query(SQL, [tenantId], async function (err, result) {
                if (result && result.length > 0) {

                    const timestamp = Date.now();
                    const dateObject = new Date(timestamp);
                    const date = dateObject.getDate();
                    const month = dateObject.getMonth() + 1;
                    const year = dateObject.getFullYear();
                    var datetime = `${year}-${month}-${date}`;
                    var fileName = 'detail_' + datetime + '_' + tenantId
                    const data = {
                        "name": fileName,
                        "headerArray": parseJsonFun(result[0], 1),
                        "rows": result[1]
                    };

                    const object = data;
                    const workbook = exceljs.jsonToExcel(object);
                    workbook.removeWorksheet('MasterData');
                    //let filepath = dirPath + "/outputs/" + fileName + ".xlsx";
                    let filepath = config.mntdir + "/cronreports/" + fileName +".xlsx";

                    var filefors3 = {
                        path: filepath,
                        name: fileName + ".xlsx"
                    }

                    await workbook.xlsx
                        .writeFile(filepath)
                        .then(async () => {
                            await uploadtos3(filefors3, db, tenantId, 2);
                            resolve(1);
                        }
                        );

                } else {
                    errorlog.inserterrorlog('get_detail_enrollment_report', 'No data available', 'API', null);
                    resolve({err: true, data: 'No data available'})
                }
            })
        }
        catch (err) {
            console.log(err.message);
            errorlog.inserterrorlog('get_detail_enrollment_report', err.message, 'API', null);
            resolve({err: true, data: err.message})
        }
    })
}

exports.get_sftp_status_report = async (db, tenantId) => {

    return new Promise((resolve, reject) => {
        const newdb = db;
        const SQL = 'call sp_get_sftp_status_report(?);';
        try {
            // await qP.queryPromise(SQL, [sectionId, tenantId]).then((result) => {
            newdb.query(SQL, [tenantId], async function (err, result) {
                if (result && result.length > 0) {
                    const timestamp = Date.now();
                    const dateObject = new Date(timestamp);
                    const date = dateObject.getDate();
                    const month = dateObject.getMonth() + 1;
                    const year = dateObject.getFullYear();
                    var datetime = `${year}-${month}-${date}`;
                    var fileName = 'sftpstatus' + datetime + '_' + tenantId
                    const data = {
                        "name": fileName,
                        "headerArray": parseJsonFun(result[0], 1),
                        "rows": result[1]
                    };

                    const object = data;
                    const workbook = exceljs.jsonToExcel(object);
                    workbook.removeWorksheet('MasterData');
                    //let filepath = dirPath + "/outputs/" + fileName + ".xlsx";
                    let filepath = config.mntdir + "/cronreports/" + fileName +".xlsx";
                    var filefors3 = {
                        path: filepath,
                        name: fileName + ".xlsx"
                    }

                    await workbook.xlsx
                        .writeFile(filepath)
                        .then(async () => {
                            await uploadtos3(filefors3, db, tenantId, 3);
                            resolve(1);
                        }
                        );

                } else {
                    errorlog.inserterrorlog('get_sftp_status_report', 'No data available', 'API', null);
                    resolve({err: true, data: 'No data available'})
                }
            })
        }
        catch (err) {
            console.log(err.message);
            resolve({err: true, data: err.message})
        }
    })
}

const uploadtos3 = async (file, db, tenantId, typeId) => {
    return new Promise((resolve, reject)=>{
        const Key = file.name;
        s3.upload({
            Key,
            Bucket: BUCKET,
            Body: fs.readFileSync(file.path),
            ACL: FILE_PERMISSION
        },
            async (err, data) => {
                if (err) {
                    ErrorLog.inserterrorlog('Error in Upload to s3', err.message, 'uploadtos3', null);
                    resolve({err: true, data: err.message});
                }
                else {
                    let link = data.Location;
                    if (link != null && link != '')
                        var {err, data} = await saveLink(db, tenantId, link, file.path, file.name, typeId);

                        console.log(err);
                        console.log(data);


                    resolve({err: false, data: data});
                }
            });
    })
}

async function saveLink(db, tenantId, link, deletepath, fileName, typeId) {
    return new Promise((resolve, reject)=>{
        const SQL = 'call SP_sftp_link_save(?,?,?,?);';
        var params = [fileName, link, tenantId, typeId];
        db.query(SQL, params, async function (err, result) {
            if (err) {
                ErrorLog.inserterrorlog('Error in File Deletion in  SFTP  Report Links', err.message, 'saveLink', null);
                resolve({err: true, data: err.message})
            }
            console.log(result ,'', tenantId );
            if (result && Array.isArray(result) && result[0].length != 0 && result[0][0]) {
               
                await fs.unlink(deletepath, (err) => {
                    if (err) {
                        ErrorLog.inserterrorlog('Error in File Deletion in  SFTP  Report Links', err.message, 'saveLink', null);
                    }
                    else {
                        console.log("File Deleted successfully....");
                    }
                    resolve({err: false, data: 'File Deleted successfully....'})
                });
            }else{
                resolve({err: true, data: 'Not Found'})
            }
        })
    })
}

function parseJsonFun(list, type) {

    if (type == 1) {
        list.forEach(element => {
            if (element["value"] != "" && typeof element["value"] == 'string') {
                element["value"] = JSON.parse(element["value"]);
            }
        });
    } else if (type == 2) {
        list.forEach(element => {
            if (element["list"] != "" && typeof element["list"] == 'string') {
                element["list"] = JSON.parse(element["list"]);
            }
            if (element["localFilterParams"] != "" && typeof element["localFilterParams"] == 'string') {
                element["localFilterParams"] = JSON.parse(element["localFilterParams"]);
            }
            if (element["filterMetadata"] != "" && typeof element["filterMetadata"] == 'string') {
                element["filterMetadata"] = JSON.parse(element["filterMetadata"]);
            }
        });
    }

    return list;
}


exports.sendnotificationforcronreport = async (db) => {
    try {
        const qP = new QP(db);
        const SQL = 'call SP_cron_report_mail();';
        var params = [];
        await qP.queryPromise(SQL, params).then(result => {

            if (result[0] && result[0].length > 0) {
                var item = result[0][0];
                item.reconmail = item.reconmail;
                item.username = 'Admin';
            pl.processlog(db, 'sendnotificationforcronreport', "function call ", JSON.stringify(item), 'AUTO', 0);
                mail.sendmailcc(item.email, item.title, item.message, item.username, item.emailcc);
            pl.processlog(db, 'sendnotificationforcronreport', "function call ended", JSON.stringify(item), 'AUTO', 0);
            }

        }).catch((error) => {
            ErrorLog.inserterrorlog("sendnotificationforcronreport", error.message, "Email Notification", null);
        });
    }
    catch (e) {
        console.log(e);
        ErrorLog.inserterrorlog("sendnotificationforcronreport", e.message, "Email Notification", null);
    }
};

exports.sendnotificationforsftpstatusreport = async (db) => {
    try {
        const qP = new QP(db);
        const SQL = 'call SP_sftp_status_report_mail();';
        var params = [];
        await qP.queryPromise(SQL, params).then(result => {
             console.log('sendnotificationforsftpstatusreport',result)
            if (result[0] && result[0].length > 0) {
                var item = result[0][0];
                item.reconmail = item.reconmail;
                item.username = 'Admin';
                pl.processlog(db, 'sendnotificationforsftpstatusreport', "function call started ", JSON.stringify(item), 'AUTO', 0);
                mail.sendmailcc(item.email, item.title, item.message, item.username, item.emailcc);
                pl.processlog(db, 'sendnotificationforsftpstatusreport', "function call ended ", JSON.stringify(item), 'AUTO', 0);
            }

        }).catch((error) => {

            ErrorLog.inserterrorlog("sendnotificationforsftpstatusreport", error.message, "Email Notification", null);
        });
    }
    catch (e) {
        console.log(e);
        ErrorLog.inserterrorlog("sendnotificationforsftpstatusreport", e.message, "Email Notification", null);
    }
};


async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}