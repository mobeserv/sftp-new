var ErrorLog = require('./manage');

exports.processlog_asynchronous = (db, type, activity, desc, trig,tId) => {

    var resourceSQL = "call SP_Automated_Process_log_asynchronous(?,?,?,?,?)";
    db.query(resourceSQL, [type, activity, desc, trig,tId], function(err, res) {
        if (err) {
            // console.log('err ', err);
        } else {
            //  console.log('res ', res);

        }
    });
    
}
exports.processlog = async (db, type, activity, desc, trig,tId) => {
    // var resourceSQL = "call SP_Automated_Process_log(?,?,?,?,?)";
    // return db.query(resourceSQL, [type, activity, desc, trig,tId], function(err, res) {
    //     if (err) {
    //        return err; // console.log('err ', err);
    //     } else {
    //         //  console.log('res ', res);
    //         return res;
    //     }
    // });
    try {
        var sql = "call SP_Automated_Process_log(?,?,?,?,?)";
        return await db.query(sql, [type, activity, desc, trig,tId]);
    } catch (error) {
        console.warn(error);
        ErrorLog.inserterrorlog('processlog', error.message, null, null);
        return null;
    }
}

exports.process_log_insertion = async (req, res) => {
    try {
        const list = req.body;
        const db = req.db;
        await list.forEach( (value, key) => {
           const data = exports.processlog(db,
                value.process_type,
                value.process_activity,
                value.description,
                value.trigger_type,
                value.tenantId);
        })
        res.json({
            data: 'done',
            type: true
        });
    } catch (e) {
        ErrorLog.inserterrorlog('process_log_insertion', e.message, null, null);
        res.json({
            data: e.message,
            type: false
        });
    }
}