const config = require('./config');
var mailgun = require('mailgun-js')({apiKey: config.mailGun.API_KEY, domain: config.mailGun.Domain});
/**
 * SMTP
 */
const nodemailer = require('nodemailer');
const Mailgen = require('mailgen');
const options = config.smtp;
let transport = nodemailer.createTransport(options);
const ErrorLog = require("./manage");
//const logmanage = require('../../../logs/manage');

/**
 * SMTP
 *///
var registeremail   = config.ses.registeremail || "bhavesh@mobeserv.com";
var AWS = require('aws-sdk');
// Instantiate SES.
// var ses = new aws.SES();
AWS.config.update({region: 'ap-south-1'});
const ses = new AWS.SES({
    accessKeyId: config.s3.accesskey,//process.env.AWS_ACCESS_KEY,
    secretAccessKey: config.s3.secretkey//process.env.AWS_SECRET_ACCESS_KEY
});
var pl = require('./processLog');

//Day 1 Changes START
exports.sendmailcc = async (to, subject, messages, username, cc) => {
    return new Promise(function (resolve, reject) {
        if (config.serverType === 'illume') {
            var mailGenerator = new Mailgen({
                theme: 'default',
                product: {
                    name: '<span style="font-family:Arial;font-size:14.5px;">Team L&D.</span>',
                    link: 'https://learn.bajajfinserv.in',
                    logo: config.mailGun.asset.image,
                    copyright: "© 2020 BFL Illume. All rights reserved."
                }
            })
            var email = {
                body: {
                    name: username ? '<span style="font-family:Arial;font-size:14.5px;">' + username + '</span>' : null,
                    intro: messages,
                    signature: '<span style="font-family:Arial;font-size:14.5px;">Cheers</span>',
                    greeting: '<span style="font-family:Arial;font-size:14.5px;">Hi</span>'
                }
            };
            const message = {
                from: 'ILLUME<no-reply@bajajfinserv.in>', // Sender address
                to: to,         // List of recipients
                subject: subject, // Subject line
                html: mailGenerator.generate(email), // Plain text body
                cc: cc
            };
            transport.sendMail(message, function (err, info) {
                if (err) {
                    ErrorLog.inserterrorlog('sendmailcc', err.message, null, 0);
                    console.log(err);
                    resolve({err: true, data: err.message});
                } else {
                    console.log('Mail sent successfully');
                    console.log(info);
                    resolve({err: false, data: info});
                }
            });
        }
    })
}
//Day 1 Changes END

exports.sendMail = async (to, subject, messages, username,  cb) => {
    if (config.serverType === 'saas') {
        var mailGenerator = new Mailgen({
            theme: 'default',
            product: {
                name: 'The EDGE Team',    
                link: 'https://edgelearning.co.in',
                logo: config.mailGun.asset.image,
                copyright: "© 2020 EDGE. All rights reserved."
                }
            })
            var email = {
                body: {
                    name: username ,
                    intro: messages,
                    signature: 'Cheers'
                    // greeting: '<span style="font-family:Arial;font-size:14.5px;">Hi</span>'
                }
            };
        
            if(typeof messages === 'object'){
                email.body['action'] = messages.action;
                email.body['outro'] = messages.outro;
                email.body.intro = messages.msg;
            }
        var mailsenddata = {
            from:"Edge Learning<"+registeremail+">",
            to: to, //Should be array example : ['sandeep.daga@regsus.com','support@metalsurvey.com'],
            subject: subject, // "Registrations status-("+formatDate(new Date())+")",
            html: mailGenerator.generate(email),
            //attachment: attch,    
        };
        var { err, result} = await exports.sendMailAws(mailsenddata.from, mailsenddata.to, mailsenddata.subject, mailsenddata.html);
        if (err) {
            const data = {
                message:messages,
                senderId:mailsenddata.to,
                sending_status:'error',
                sending_description:JSON.stringify(result, null, '  '),
            };
            //logmanage.insertnotifylog(data);
            cb({type:false,error:result});
        } else {
            const data={
                message:messages,
                senderId:mailsenddata.to,
                sending_status:'successful',
                sending_description:JSON.stringify(result, null, '  '),
            };
            //logmanage.insertnotifylog(data);
            cb({type:true,body:result});
        }
        
        /*mailgun.messages().send(mailsenddata, function(error, body) {
            if (error) {
                cb({type:false,error:error});
                const data={
                    message:messages,
                    senderId:mailsenddata.to,
                    sending_status:'error',
					sending_description:JSON.stringify(error, null, '  '),
                };
                //logmanage.insertnotifylog(data);
               
            } else {
                cb({type:true,body:body});
                const data={
                    message:messages,
                    senderId:mailsenddata.to,
                    sending_status:'successful',
					sending_description:JSON.stringify(body, null, '  '),
                };
                //logmanage.insertnotifylog(data);
                
            }
        });*/        
    }
    if (config.serverType === 'illume'){
        var mailGenerator = new Mailgen({
            theme: 'default',
            product: {
                name: '<span style="font-family:Arial;font-size:14.5px;">Team L&D.</span>',    
                link: 'https://learn.bajajfinserv.in',
                logo: config.mailGun.asset.image,
                copyright: "© 2020 BFL Illume. All rights reserved."
                }
            })
        var email = {
            body: {
                name: '<span style="font-family:Arial;font-size:14.5px;">'+ username +'</span>',
                intro: messages,
                signature: '<span style="font-family:Arial;font-size:14.5px;">Cheers</span>',
                greeting: '<span style="font-family:Arial;font-size:14.5px;">Hi</span>'
            }
        };
    
        if(typeof messages === 'object'){
            email.body['action'] = messages.action;
            email.body['outro'] = messages.outro;
            email.body.intro = messages.msg;
        }
    
        const message = {
            from: 'ILLUME<no-reply@bajajfinserv.in>', // Sender address
            to: to,         // List of recipients
            subject: subject, // Subject line
            html: mailGenerator.generate(email) // Plain text body
        };
        transport.sendMail(message, function(err, info) {
            if (err) {
                cb(err);
                console.log(err);
              const data={
                message:messages,
                senderId:message.to,
                sending_status:'error',
                sending_description:JSON.stringify(err, null, '  '),
            };
            //logmanage.insertnotifylog(data);
              
            } else {
                cb(info);
              console.log(info);
              const data={
                message:messages,
                senderId:message.to,
                sending_status:'successful',
                sending_description:JSON.stringify(info, null, '  '),
            };
            //logmanage.insertnotifylog(data);
              
            }
        });
    }    
}

exports.commonmail = (req, res) => {
    var to = req.body.to, subject = req.body.subject, messages = req.body.messages, username = req.body.username;
    
    // var { err, result} = await exports.sendMailAws(to, subject, messages, username);
    // if (Array.isArray(req.body)) {
    //     list = req.body;
    //     var smsarray = [];
    //     console.log('start time', new Date());
    //     exports.getItem(list, 0, [], (result)=>{
    //         console.log('end time', new Date());
    //         res.json({
    //             type: err,
    //             data: result
    //         });
    //     });
    // } else {
    //     var { err, result} = await exports.sendMailAws(to, subject, messages, username);
    //     res.json({
    //         type: err,
    //         data: result
    //     });
    // }
    exports.sendMail(to, subject, messages, username, (response) => {
        res.json({
            data: response
        });
    });
}

/*PReon Email Function  */
exports.sendpreonMail = async (to, subject, messages, username,  cb) => {
    if (config.serverType === 'saas'){
        var mailGenerator = new Mailgen({
            theme: 'default',
            product: {
                name: 'The EDGE Team',    
                link: 'https://edgelearning.co.in',
                logo: config.mailGun.asset.image,
                copyright: "© 2020 EDGE. All rights reserved."
                }
            })
            var email = {
                body: {
                    name: username ,
                    intro: messages,
                    signature: 'Cheers'
                    // greeting: '<span style="font-family:Arial;font-size:14.5px;">Hi</span>'
                }
            };
        
            if(typeof messages === 'object'){
                email.body['action'] = messages.action;
                email.body['outro'] = messages.outro;
                email.body.intro = messages.msg;
            }
        var mailsenddata = {
            from:'Edge Learning<'+registeremail+'>',
            to: to ,//|| ['team@mobeserv.com'], //Should be array example : ['sandeep.daga@regsus.com','support@metalsurvey.com'],
            subject: subject, // "Registrations status-("+formatDate(new Date())+")",
            html: mailGenerator.generate(email),
            //attachment: attch,    
        };
        var { err, result} = await exports.sendMailAws(mailsenddata.from, mailsenddata.to, mailsenddata.subject, mailsenddata.html);
        if (err) {
            const data={
                message:messages,
                senderId:mailsenddata.to,
                sending_status:'error',
                sending_description:JSON.stringify(result, null, '  '),
            };
            //logmanage.insertnotifylog(data);
            cb(result);
        } else {
            const data={
                message:messages,
                senderId:mailsenddata.to,
                sending_status:'successful',
                sending_description:JSON.stringify(result, null, '  '),
            };
            //logmanage.insertnotifylog(data);
            cb(result);
        }
        /*mailgun.messages().send(mailsenddata, function(error, body) {
            if (error) {
                
                cb(error);
                const data={
                    message:messages,
                    senderId:mailsenddata.to,
                    sending_status:'error',
                    sending_description:JSON.stringify(error, null, '  '),
                };
                //logmanage.insertnotifylog(data);
                
            } else {
                cb(body);
                const data={
                    message:messages,
                    senderId:mailsenddata.to,
                    sending_status:'successful',
                    sending_description:JSON.stringify(body, null, '  '),
                };
                //logmanage.insertnotifylog(data);
            }
        });*/
    }
    if (config.serverType === 'illume'){
        var mailGenerator = new Mailgen({
            theme: 'default',
            product: {
                name: ' ',    
                link: 'https://learn.bajajfinserv.in',
                logo: config.mailGun.asset.image,
                copyright: "© 2019 BFL Illume. All rights reserved."
                }
            })
        var email = {
            body: {
                // name: username,
                // intro: messages,
                //signature: 'Thank you'
                name: '<span style="font-family:Arial;font-size:14.5px;">'+ username +'</span>',
                intro: messages,
                signature: '<span style="font-family:Arial;font-size:14.5px;">Thank you</span>',
                greeting: '<span style="font-family:Arial;font-size:14.5px;">Hi</span>'
            }
        };
        const message = {
            from: 'ILLUME<no-reply@bajajfinserv.in>', // Sender address
            to: to,         // List of recipients
            subject: subject, // Subject line
            html: mailGenerator.generate(email) // Plain text body
        };
        transport.sendMail(message, function(err, info) {
            if (err) {
                cb(err);
            const data={
                message:messages,
                senderId:message.to,
                sending_status:'error',
                sending_description:JSON.stringify(err, null, '  '),
            };
            //logmanage.insertnotifylog(data);
            
            } else {
                cb(info);
            console.log(info);
            const data={
                message:messages,
                senderId:message.to,
                sending_status:'successful',
                sending_description:JSON.stringify(info, null, '  '),
            };
            //logmanage.insertnotifylog(data);
            
            }
        });
    }
   
}

exports.sendmailgunemail = (req, res) => {
    try {
        const to = req.body.to, from = req.body.from, 
        subject = req.body.subject, body = req.body.body;
        exports.sendMailMailgun(to, subject, body, (result) => {
            res.json({type: true});
        })
    } catch (error) {
        res.json(
            {
                type: false,
                data: error.message
            }
        );
    }
}
exports.sendMailMailgun = async (to, subject, body, cb) => {
    var mailGenerator = new Mailgen({
        theme: 'default',
        product: {
            name: 'Edge',    
            link: 'https://edgelearning.co.in',
            logo: config.mailGun.asset.image,
            copyright: "© 2019 EDGE. All rights reserved."
            }
        })
        var email = {
            body: {
                name: "Unmesh",
                intro: body,
                signature: 'Thank you'
            }
        };
    var mailsenddata = {
        from:'Edge Learning<'+registeremail+'>',
        to: to, //|| ['team@mobeserv.com', 'unmesh@edgelearning.co.in'], //Should be array example : ['sandeep.daga@regsus.com','support@metalsurvey.com'],
        subject: subject, // "Registrations status-("+formatDate(new Date())+")",
        html: mailGenerator.generate(email),
        //attachment: attch,    
    };
    var { err, result} = await exports.sendMailAws(mailsenddata.from, mailsenddata.to, mailsenddata.subject, mailsenddata.html);
    if (err) {
        const data={
            message:body,
            senderId:mailsenddata.to,
            sending_status:'error',
            sending_description:JSON.stringify(result, null, '  '),
        };
        //logmanage.insertnotifylog(data);
        cb(result);
    } else {
        const data={
            message:body,
            senderId:mailsenddata.to,
            sending_status:'successful',
            sending_description:JSON.stringify(result, null, '  '),
        };
        //logmanage.insertnotifylog(data);
        cb(result);
    }
    /*mailgun.messages().send(mailsenddata, function(error, body) {
        if (error) {
            
            cb(error);
            const data={
                message:body,
                senderId:mailsenddata.to,
                sending_status:'error',
                sending_description:JSON.stringify(error, null, '  '),
            };
            //logmanage.insertnotifylog(data);
            
        } else {
            cb(body);
            const data={
                message:body,
                senderId:mailsenddata.to,
                sending_status:'successful',
                sending_description:JSON.stringify(body, null, '  '),
            };
            //logmanage.insertnotifylog(data);
            
        }
    });*/
}

exports.sendDomainCreationMail = (req, res) => {
    try {
        const to = req.body.userDetails.emailId;
        body = 'Username: ' + req.body.userDetails.emailId + '<br/> Phone: ' + req.body.userDetails.phone + 
        '<br/> firstName: ' + req.body.userDetails.firstName +  '<br/> lastName: ' + req.body.userDetails.lastName +
         '<br/> subdomain: ' + 'https://' + req.body.subDomainName + '.edgelearning.co.in' ;
        exports.sendDomainMailgunCreationMail(to, body,req.body, (result) => {
            res.json(
                {
                    type: true,
                    message: 'Mail sent successfully',
                    alreadyExist: false,
                 }
            );
        })
    } catch (error) {
        res.json(
            {
                type: false,
                data: error.message
            }
        );
    }
}

exports.sendDomainMailgunCreationMail = async (to, body, params,cb) => {
    var mailGenerator = new Mailgen({
        theme: 'default',
        product: {
            name: 'The EDGE Team',    
            link: 'https://edgelearning.co.in',
            logo: config.mailGun.asset.image,
            copyright: "© 2020 EDGE. All rights reserved."
            }
        })
        const emailData = `Domain Created Successfully. <br>` + body
        var email = {
           
            body: {
                name: params.userDetails.firstName,
                intro: emailData,
                signature: 'Cheers'
            }
        };
    var mailsenddata = {
        from:'Edge Learning<'+registeremail+'>',
        to: to,// || ['team@mobeserv.com'], //Should be array example : ['sandeep.daga@regsus.com','support@metalsurvey.com'],
        subject: 'Domain Added', // "Registrations status-("+formatDate(new Date())+")",
        html: mailGenerator.generate(email),
        //attachment: attch,    
    };
    var { err, result} = await exports.sendMailAws(mailsenddata.from, mailsenddata.to, mailsenddata.subject, mailsenddata.html);
    if (err) {
        const data={
            message:body,
            senderId:mailsenddata.to,
            sending_status:'error',
            sending_description:JSON.stringify(result, null, '  '),
        };
        //logmanage.insertnotifylog(data);
        cb(result);
    } else {
        const data={
            message:body,
            senderId:mailsenddata.to,
            sending_status:'successful',
            sending_description:JSON.stringify(result, null, '  '),
        };
        //logmanage.insertnotifylog(data);
        cb(result);
    }
    /*mailgun.messages().send(mailsenddata, function(error, body) {
        if (error) {
            cb(error);
            const data={
                message:body,
                senderId:mailsenddata.to,
                sending_status:'error',
                sending_description:JSON.stringify(error, null, '  '),
            };
            //logmanage.insertnotifylog(data);
            
        } else {
            cb(body);
            const data={
                message:body,
                senderId:mailsenddata.to,
                sending_status:'successful',
                sending_description:JSON.stringify(body, null, '  '),
            };
            //logmanage.insertnotifylog(data);
            
        }
    });*/
}


exports.sendSignupMail = (req, res) => {
    try {
        const to = req.body.emailId;
        body = req.body.shortLink;
        req.body['db']=req.db;
        exports.sendMailgenSignupMail(to, body,req.body, (result) => {
            res.json(
                {
                    type: true,
                    message: 'Mail sent successfully',
                    link: req.body.shortLink,
                 }
            );
        })
    } catch (error) {
        res.json(
            {
                type: false,
                data: error.message
            }
        );
    }
}

exports.sendMailgenSignupMail = async (to, body, params,cb) => {
    var mailGenerator = new Mailgen({
        theme: 'default',
        product: {
            name: 'The EDGE Team',    
            link: 'https://edgelearning.co.in',
            logo: config.mailGun.asset.image,
            copyright: "© 2020 EDGE. All rights reserved."
            }
        })
        const emailData = `
        <div>
        <p> Please click the button below to confirm your email address:</p>   
        </div>
        <div class="button">
            <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="`+ body +`"style="height:35px;v-text-anchor:middle;" arcsize="8%" strokecolor="#1282ce" fillcolor="#1282ce">
              <w:anchorlock/>
              <center style="color:#ffffff;font-family:sans-serif;font-size:16px;font-weight:bold;">Click here to verify</center>
            </v:roundrect>
        <![endif]-->
        <a href=`+ body +` style="background-color:#1282ce;border:0px solid #1282ce;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;font-weight:bold;line-height:35px;text-align:center;text-decoration:none;width:150px;-webkit-text-size-adjust:none;mso-hide:all;">Click here to verify</a>
    </div>
    <div>
            <p>Once confirmed, this email will be uniquely associated with your EDGE Learning account.</p>
    </div>`
        var email = {
           
            body: {
                name: params['firstName'],
                intro: emailData,
                signature: 'Cheers'
            }
        };
    var mailsenddata = {
        from:'Edge Learning<'+registeremail+'>',
        to: to,// || ['team@mobeserv.com'], //Should be array example : ['sandeep.daga@regsus.com','support@metalsurvey.com'],
        subject: 'Confirm your account', // "Registrations status-("+formatDate(new Date())+")",
        html: mailGenerator.generate(email),
        //attachment: attch,    
    };
    var Notificatiopmailsenddata = {     
        title:'Admin User Signup Mail',
        subtitle:'Admin User Signup Mail',
        message:emailData,
        additionalData:'',
        userId:null,
        areaId:35,
        notModeId:2,
        instanceId:null,
        event:'Admin User Signup Mail',
        isRead:0,
        isSent:0,
        senddate:
        new Date().toISOString().slice(0, 10) +
        ' ' +
        new Date().toISOString().slice(11, 19),
        tenantId:null,
        eventId:0,
        uniqueId:params.uniqueId,
    }

    var { err, result} = await exports.sendMailAws(mailsenddata.from, mailsenddata.to, mailsenddata.subject, mailsenddata.html);
    if (err) {
        new sendMailsaveemailadmin_Signup(params.db,Notificatiopmailsenddata);
        const data={
            message:emailData,
            senderId:mailsenddata.to,
            sending_status:'error',
            sending_description:JSON.stringify(result, null, '  '),
        };
        //logmanage.insertnotifylog(data);
        cb(result);
    } else {
        Notificatiopmailsenddata.isSent=1;
        new sendMailsaveemailadmin_Signup(params.db,Notificatiopmailsenddata);
        const data={
            message:emailData,
            senderId:mailsenddata.to,
            sending_status:'successful',
            sending_description:JSON.stringify(result, null, '  '),
        };
        //logmanage.insertnotifylog(data);
        cb(result);
    }
    /*mailgun.messages().send(mailsenddata, function(error, body) {
        if (error) {
            
            cb(error);
            new sendMailsaveemailadmin_Signup(params.db,Notificatiopmailsenddata);
            const data={
                message:emailData,
                senderId:mailsenddata.to,
                sending_status:'error',
                sending_description:JSON.stringify(error, null, '  '),
            };
            //logmanage.insertnotifylog(data);
            
        } else {

            cb(body);

            Notificatiopmailsenddata.isSent=1;
            new sendMailsaveemailadmin_Signup(params.db,Notificatiopmailsenddata);
            const data={
                message:emailData,
                senderId:mailsenddata.to,
                sending_status:'successful',
                sending_description:JSON.stringify(body, null, '  '),
            };
            //logmanage.insertnotifylog(data);
        }
    });*/
}
// exports.sendMailgenSignupMail = (to, body, params,cb) => {
//     console.log('Theme =>',  path.resolve('./api/controllers/common/theme.html'));
//     var mailGenerator = new Mailgen({
//         theme: {
//             // Build an absolute path to the theme file within your project
//             path: path.resolve('./api/controllers/common/theme.html'),
//             // Also (optionally) provide the path to a plaintext version of the theme (if you wish to use `generatePlaintext()`)
//             plaintextPath: path.resolve('./api/controllers/common/theme.txt')
//         },
//         product: {
//             name: 'The EDGE Team',    
//             link: 'https://edgelearning.co.in',
//             logo: config.mailGun.asset.image,
//             copyright: "© 2020 EDGE. All rights reserved."
//             }
//         })
      
//         var email = {
           
//             body: {
//                 name: params['firstName'],
//                 intro: '',
//                 signature: 'Cheers'
//             }
//         };
//     var mailsenddata = {
//         from:'Edge Learning<admin@edgelearning.co.in>',
//         to: to || ['team@mobeserv.com'], //Should be array example : ['sandeep.daga@regsus.com','support@metalsurvey.com'],
//         subject: 'Verify Email to proceed', // "Registrations status-("+formatDate(new Date())+")",
//         html: mailGenerator.generate(email),
//         //attachment: attch,    
//     };
//     mailgun.messages().send(mailsenddata, function(error, body) {
//         if (error) {
//             // console.log('mailnot send ',error);
//             cb(error);
//         } else {
//             // console.log('mailsend',body)
//             cb(body);
//         }
//     });
// }
exports.sendUserSignupMail = (req, res) => {
    try {
        const to = req.body.emailId;
        body = req.body.loginURL;
        req.body['db']=req.db;
        exports.sendMailgenUserSignupMail(to, body,req.body, (result) => {
            res.json(
                {
                    type: true,
                    message: req.body.message,
                    alreadyExist: false,
                    link: req.body.loginURL,
                 }
            );
        })
    } catch (error) {
        res.json(
            {
                type: false,
                data: error.message
            }
        );
    }
}

exports.sendMailgenUserSignupMail = async (to, body, params, cb) => {
    var mailGenerator = new Mailgen({
        theme: 'default',
        product: {
            name: 'The EDGE Team',    
            link: 'https://edgelearning.co.in',
            logo: config.mailGun.asset.image,
            copyright: "© 2020 EDGE. All rights reserved."
            }
        })
        const emailData = `
        <div>
        <p> Account Created Successfully:</p>   
        </div>
        <div class="button">
            <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="`+ body +`"style="height:35px;v-text-anchor:middle;" arcsize="8%" strokecolor="#1282ce" fillcolor="#1282ce">
              <w:anchorlock/>
              <center style="color:#ffffff;font-family:sans-serif;font-size:16px;font-weight:bold;">Click here to verify</center>
            </v:roundrect>
        <![endif]-->
        <a href=`+ body +` style="background-color:#1282ce;border:0px solid #1282ce;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;font-weight:bold;line-height:35px;text-align:center;text-decoration:none;width:150px;-webkit-text-size-adjust:none;mso-hide:all;">Click here to login</a>
    </div>
    <div>
            <p>Once confirmed, this email will be uniquely associated with your EDGE Learning account.</p>
    </div>`
        var email = {
           
            body: {
                name: params['firstName'],
                intro: emailData,
                signature: 'Cheers'
            }
        };
    var mailsenddata = {
        from:'Edge Learning<'+registeremail+'>',
        to: to,// || ['team@mobeserv.com'], //Should be array example : ['sandeep.daga@regsus.com','support@metalsurvey.com'],
        subject: 'New user created', // "Registrations status-("+formatDate(new Date())+")",
        html: mailGenerator.generate(email),
        //attachment: attch,    
    };
    var Notificatiopmailsenddata = {     
        title:'User Signup Mail',
        subtitle:'User Signup Mail',
        message:emailData,
        additionalData:'',
        userId:params.userId,
        areaId:35,
        notModeId:2,
        instanceId:null,
        event:'User Signup Mail',
        isRead:0,
        isSent:0,
        senddate:
        new Date().toISOString().slice(0, 10) +
        ' ' +
        new Date().toISOString().slice(11, 19),
        tenantId:params.tenantId,
        eventId:0
    }
    var { err, result} = await exports.sendMailAws(mailsenddata.from, mailsenddata.to, mailsenddata.subject, mailsenddata.html);
    if (err) {
        const data={
            message:emailData,
            senderId:mailsenddata.to,
            sending_status:'error',
            sending_description:JSON.stringify(result, null, '  '),
        };
        //logmanage.insertnotifylog(data);
        new sendMailsaveemail(params.db,Notificatiopmailsenddata);
        cb(result);
    } else {
        Notificatiopmailsenddata.isSent=1;
        new sendMailsaveemail(params.db,Notificatiopmailsenddata);
        
        const data={
            message:emailData,
            senderId:mailsenddata.to,
            sending_status:'successful',
            sending_description:JSON.stringify(result, null, '  '),
        };
        //logmanage.insertnotifylog(data);
        cb(result);
    }
    /*mailgun.messages().send(mailsenddata, function(error, body) {
        if (error) {
            cb(error);
            const data={
                message:emailData,
                senderId:mailsenddata.to,
                sending_status:'error',
                sending_description:JSON.stringify(error, null, '  '),
            };
            //logmanage.insertnotifylog(data);
            new sendMailsaveemail(params.db,Notificatiopmailsenddata)
        } else {
            cb(body);
            Notificatiopmailsenddata.isSent=1;
            new sendMailsaveemail(params.db,Notificatiopmailsenddata);
            
            const data={
                message:emailData,
                senderId:mailsenddata.to,
                sending_status:'successful',
                sending_description:JSON.stringify(body, null, '  '),
            };
            //logmanage.insertnotifylog(data);
           
        }
    });*/
}


class sendMailsaveemail{
    constructor(db,Notify_Datas){
        // sendMailsaveemail = (db,Notify_Datas) => {

            var sql = 'call SP_ADMIN_ADD_Notification(?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
            db.query(
               sql,
               [
                 Notify_Datas.title,
                 Notify_Datas.subtitle,
                 Notify_Datas.message,
                 Notify_Datas.additionalData,
                 Notify_Datas.userId,
                 Notify_Datas.areaId,
                 Notify_Datas.notModeId,
                 Notify_Datas.instanceId,
                 Notify_Datas.event,
                 Notify_Datas.isRead,
                 Notify_Datas.isSent,
                 Notify_Datas.senddate,
                 Notify_Datas.tenantId,
                 Notify_Datas.eventId,
               ],
               
               function(err, success) {
                 if (err) {
                 } else {
                 }
               });
            }
    // }

}
class sendMailsaveemailadmin_Signup{
    constructor(db,Notify_Datas){
        // sendMailsaveemail = (db,Notify_Datas) => {

            var sql = 'call SP_ADMIN_ADD_Notification_admin_Signup(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
            db.query(
               sql,
               [
                 Notify_Datas.title,
                 Notify_Datas.subtitle,
                 Notify_Datas.message,
                 Notify_Datas.additionalData,
                 Notify_Datas.userId,
                 Notify_Datas.areaId,
                 Notify_Datas.notModeId,
                 Notify_Datas.instanceId,
                 Notify_Datas.event,
                 Notify_Datas.isRead,
                 Notify_Datas.isSent,
                 Notify_Datas.senddate,
                 Notify_Datas.tenantId,
                 Notify_Datas.eventId,
                 Notify_Datas.uniqueId,
               ],
               
               function(err, success) {
                 if (err) {
                 } else {
                 }
               });
            }
    // }

}

/**
 * 
 * @description send mail using aws
 */
exports.sendMailAws = async (from, to, subject, messages) =>{

    /*
    var mailGenerator = new Mailgen({
        theme: 'default',
        product: {
            name: 'The EDGE Team',    
            link: 'https://edgelearning.co.in',
            logo: config.mailGun.asset.image,
            copyright: "© 2020 EDGE. All rights reserved."
            }
        })
        var email = {
            body: {
                name: username ,
                intro: messages,
                signature: 'Cheers'
                // greeting: '<span style="font-family:Arial;font-size:14.5px;">Hi</span>'
            }
        };
    
        if(typeof messages === 'object'){
            email.body['action'] = messages.action;
            email.body['outro'] = messages.outro;
            email.body.intro = messages.msg;
        }
        var html = mailGenerator.generate(email)
        */
    if (from) {
        from = "From: " + from + "\n";
    } else {
        from = "From: 'Edge Learning' <" + registeremail + ">\n"
    }
    // var toemail = "";
    // if (Array.isArray(to)) {
    //     toemail
    // }
    var ses_mail = from;
    ses_mail = ses_mail + "To: " + to + "\n";
    ses_mail = ses_mail + "Subject: "+ subject +"\n";
    ses_mail = ses_mail + "MIME-Version: 1.0\n";
    ses_mail = ses_mail + "Content-Type: multipart/mixed; boundary=\"NextPart\"\n\n";
    ses_mail = ses_mail + "--NextPart\n";
    ses_mail = ses_mail + "Content-Type: text/html; charset=us-ascii\n\n";
    ses_mail = ses_mail + messages, // html,
    // ses_mail = ses_mail + "--NextPart\n";
    // ses_mail = ses_mail + "Content-Type: text/plain;\n";
    // ses_mail = ses_mail + "Content-Disposition: attachment; filename=\"attachment.txt\"\n\n";
    // ses_mail = ses_mail + "AWS Tutorial Series - Really cool file attachment!" + "\n\n";
    ses_mail = ses_mail + "--NextPart--";
    
    var params = {
        RawMessage: { Data: new Buffer(ses_mail) },
        Destinations: Array.isArray(to) ? to : [ to ],
        Source: "'Edge Learning<"+registeremail+">'"
    };
    return new Promise(resolve => {
        ses.sendRawEmail(params, function(err, data) {
            if(err) {
                resolve({err: true, result: err});
            } 
            else {
                resolve({err: false, result: data});
            }           
        });
    });
}

exports.sendSFTPMail = async (to, subject, messages, username, cb) =>{
    try {
        var mailGenerator = new Mailgen({
            theme: 'default',
            product: {
                name: '<span style="font-family:Arial;font-size:14.5px;">Team L&D.</span>',    
                link: 'https://learn.bajajfinserv.in',
                logo: config.mailGun.asset.image,
                copyright: "© 2020 BFL Illume. All rights reserved."
            }
        });
        var email = {
            body: {
                name: '<span style="font-family:Arial;font-size:14.5px;">'+ username +'</span>',
                intro: messages,
                signature: '<span style="font-family:Arial;font-size:14.5px;">Cheers</span>',
                greeting: '<span style="font-family:Arial;font-size:14.5px;">Hi</span>'
            }
        };
        if(typeof messages === 'object'){
            email.body['action'] = messages.action;
            email.body['outro'] = messages.outro;
            email.body.intro = messages.msg;
        }
        const message = {
            from: 'ILLUME<no-reply@bajajfinserv.in>', // Sender address
            to: to,         // List of recipients
            subject: subject, // Subject line
            html: mailGenerator.generate(email) // Plain text body
        };
        transport.sendMail(message, function(err, info) {
            if (err) {
                console.log(err);
                cb(err);
            } else {
                console.log(info);
                cb(info);
            }
        });   
    } catch (error) {
        cb(error);
    }
}