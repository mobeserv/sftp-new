'use strict';

var schedule = require('node-schedule');
var Client = require('ssh2-sftp-client');
var config = require('./config');
var globalpath = __dirname + '/sftpfiles/';
var csv = require('csvtojson');
var TwinBcrypt = require('twin-bcrypt');
//var salt = TwinBcrypt.genSalt(10);
//var enrol = require('../Admin/ENROLemployee');
const mail = require('./mail');
var attempt = 3;
var totalattempt = 0;

exports.addrolechangecron = function(server, db) {
    //console.log('function excute');

    if (config.sftpconnection.sftpcron) {
        var rule = new schedule.RecurrenceRule();
        rule.second = 0;
        rule.minute = 5;
        rule.hour = 0;
        var j = schedule.scheduleJob(rule, function() {
            totalattempt = 0;
            exports.rolechangedataAdd(server, db);
        });
    }

}

exports.rolechangedataAdd_manual = function(req, res) {
    var resdata = req.query;
    var db = req.db;
    if (!config.sftpconnection.sftpcron) {
        res.json({
            type: false,
            data: 'You resquest not process.'
        });
    } else {
        let _typem = 'MANUAL';
        let sftp = new Client();
        try {
            sftp.connect(config.sftpconnection).then(() => {
                processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "SFTP Connection done", 'successfully', _typem, 0);
                console.log('connection successfull');
                return sftp.list('/EMP_MOVEMENT_ILLUME');
            }).then((data) => {

                try {
                    var sql = 'call SP_GET_RoleChange_Tenant_List()';
                    db.query(sql, [], function(err, list) {
                        if (err) {
                            processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "Get tenant list", 'Error' + err.message, _typem, 0);
                        } else {
                            processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "Get tenant list", 'sucessfull', _typem, 0);
                            if (list[0].length > 0) {
                                res.json({
                                    type: true,
                                    data: 'You resquest is in progress.'
                                });

                                sftpmultiple(db, sftp, list[0], data, 0, resdata.date, _typem, function(resp) {
                                    processsftpfile(db, sftp, resp, data, 0, undefined, _typem, function(resp) {
                                        processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "completed", 'sucessfully', _typem, 0);
                                        console.log('Done ALL successfull');
                                        sentprcesslog(db, _typem);
                                    });
                                });
                            } else {
                                processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "NO data Found", 'sucessfull', _typem, 0);
                            }
                        }
                    })
                } catch (e) {
                    processlog(db, 'SFTP ROLE CHANGE DATA INSERT', 'Error in Get tenant list in try block', 'Error' + e.message, _typem, 0);
                }

            }).catch((err) => {
                processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "List not read", err.message, _typem, 0);
                //console.log(err, 'catch error');
            });
        } catch (e) {
            //console.log(e);
            processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "SFTP Server not connected", e.message, _typem, 0);
        }
    }
}

exports.rolechangedataAdd = function(server, db) {
    processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "attempt", totalattempt + 1, 'AUTO', 0);
    let _typea = 'AUTO';
    let sftp = new Client();

    try {
        sftp.connect(config.sftpconnection).then(() => {
            processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "SFTP Connection done", 'successfully', _typea, 0);
            console.log('connection successfull');
            return sftp.list(config.sftpconnection.roleChangefileLocation);
        }).then((data) => {

            try {
                var sql = 'call SP_GET_RoleChange_Tenant_List()';
                db.query(sql, [], function(err, list) {
                    if (err) {
                        processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "Get tenant list", 'Error' + err.message, _typea, 0);
                    } else {
                        processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "Get tenant list", 'sucessfull', _typea, 0);
                        if (list[0].length > 0) {

                            sftpmultiple(db, sftp, list[0], data, 0, undefined, _typea, function(resp) {
                                processsftpfile(db, sftp, resp, data, 0, undefined, _typea, function(resp) {
                                    processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "completed", 'sucessfully', _typea, 0);
                                    console.log('Done ALL successfull');
                                    sentprcesslog(db, _typea);
                                });
                            });
                        } else {
                            processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "NO data Found", 'sucessfull', _typea, 0);
                        }
                    }
                })
            } catch (e) {
                processlog(db, 'SFTP ROLE CHANGE DATA INSERT', 'Error in Get tenant list in try block', 'Error' + e.message, _typea, 0);
            }

        }).catch((err) => {
            processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "List not read", err.message, _typea, 0);
            if (totalattempt < attempt) {
                totalattempt++;
                exports.rolechangedataAdd(server, db);
            }
            //console.log(err, 'catch error');
        });
    } catch (e) {
        //console.log(e);
        processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "SFTP Server not connected", e.message, _typea, 0);
        if (totalattempt < attempt) {
            totalattempt++;
            exports.rolechangedataAdd(server, db);
        }
    }
}
exports.sftpUserAdd_temp = function(server, db) {
    let genfname = 'sftpsheet.csv';
    readfileanddata_temp(globalpath + genfname, db, 1);
};

function readfileanddata_temp(path, db, tenantId) {

    console.log(path);
    try {
        csv()
            .fromFile(path)
            .then((jsonObj) => {
                processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "read file data", 'successfull', 'MANUAL', tenantId);
                SaveMultiUser(db, jsonObj, 1, tenantId, 0, [], [], [], [], function(userList, unsuccessfuluser, updateuserList, resions) {

                    var donedata = {
                        Total_req_found: jsonObj.length,
                        Total_Created_Users: userList.length,
                        Total_Updated_Users: updateuserList.length,
                        Not_created_Users_New: unsuccessfuluser.length,
                        resions_list: resions
                    }
                    console.log("Done all calls ", JSON.stringify(donedata))
                    processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "Done all calls ", JSON.stringify(donedata), 'MANUAL', tenantId);
                })


            });
    } catch (e) {
        processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "read file data", e, 'MANUAL', tenantId);
    }
}

function processsftpfile(db, sftp, tlist_array, filelistdata, u, date, type, callback) {
    if (u == tlist_array.length || tlist_array.length === 0)
        return callback('Done');
    readfileanddata(tlist_array[u].filepath, db, tlist_array, filelistdata, u, sftp, date, type, callback);
}


function sftpmultiple(db, sftp, tlist_array, filelistdata, u, date, type, callback) {
    if (u == tlist_array.length || tlist_array.length === 0)
        return callback(tlist_array);
    //console.log(tlist_array[u]);
    let todaydate = GetFormattedDate(date);
    let genfname = tlist_array[u].fileName + todaydate;
    processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "List read successfully", 'successfull', type, tlist_array[u].tenantId);
    var found = filelistdata.find(function(element) {
        let filename = element.name;
        let tempfname = filename.substring(tlist_array[u].fsubStringstart, tlist_array[u].fsubStringEnd);
        // console.log('tempfname', tempfname);
        if (tempfname == genfname) {
            return element;
        }

    });
    //console.log(found.name);
    if (found != undefined) {
        processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "File found", 'File found for ' + todaydate, type, tlist_array[u].tenantId);
        sftp.fastGet('/EMP_MOVEMENT_ILLUME/' + found.name, globalpath + found.name, []).then((data) => {
            //console.log(data, 'the data info');
            processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "File downloaded successfully", 'successfull for ' + todaydate, type, tlist_array[u].tenantId);
            // readfileanddata(globalpath + found.name, db, tlist_array, filelistdata, u, sftp, date, type,callback);
            tlist_array[u].filepath = globalpath + found.name;
            sftpmultiple(db, sftp, tlist_array, filelistdata, u + 1, date, type, callback);
        }, (err) => {
            //console.log(err, 'catch error');
            tlist_array[u].filepath = '';
            processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "File download err", err.message, type, tlist_array[u].tenantId);
            sftpmultiple(db, sftp, tlist_array, filelistdata, u + 1, date, type, callback);
        });
    } else {
        console.log('file not found');
        processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "File found", 'File not found for ' + todaydate, type, tlist_array[u].tenantId);
        sftpmultiple(db, sftp, tlist_array, filelistdata, u + 1, date, type, callback);
    }

}

function readfileanddata(path, db, tlist_array, filelistdata, u, sftp, date, type, callback) {
    console.log(path);
    try {
        csv()
            .fromFile(path)
            .then((jsonObj) => {
                processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "read file data", 'successfull', type, tlist_array[u].tenantId);
                SaveMultiUser(db, jsonObj, 1, tlist_array[u].tenantId, 0, [], [], [], [], type, function(userList, unsuccessfuluser, updateuserList, resions) {

                    var donedata = {
                            Total_req_found: jsonObj.length,
                            Total_Created_Users: userList.length,
                            Total_Updated_Users: updateuserList.length,
                            Not_created_Users_New: unsuccessfuluser.length,
                            reason_list: resions
                        }
                        //processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "done all calls ", 'successfull', 'AUTO');
                    processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "Done all calls ", JSON.stringify(donedata), type, tlist_array[u].tenantId);
                    // send_response(req, res, undefined, donedata);
                    processsftpfile(db, sftp, tlist_array, filelistdata, u + 1, date, type, callback);
                });
            }).catch((error)=>{
                console.log(error);
                processsftpfile(db, sftp, tlist_array, filelistdata, u + 1, date, type, callback);
            });
    } catch (e) {
        //console.log(e);
        processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "read file data", e.message, type, tlist_array[u].tenantId);
        processsftpfile(db, sftp, tlist_array, filelistdata, u + 1, date, type, callback);
    }
}

function SaveMultiUser(newdb, ExcelData, userId, userTId, i, userList, unsuccessfuluser, updateuserList, resions, type, callback) {
    // console.log('ExcelData length ' + ExcelData.length);
    // console.log('s-',i);
    if (i == ExcelData.length || ExcelData.length === 0)
        return callback(userList, unsuccessfuluser, updateuserList, resions);
    uploadMultipleEmployeesData(ExcelData, userId, userTId, newdb, i, userList, unsuccessfuluser, updateuserList, resions, type, callback);
}

function uploadMultipleEmployeesData(ExcelData, userId, userTId, newdb, i, userList, unsuccessfuluser, updateuserList, resions, type, callback) {

    var resourceSQL = "call SP_Insert_Emp_role_change_data(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,@SQLResult, @OStatus, @isUserCreated)";

    newdb.query(resourceSQL, [
        ExcelData[i]['Employee ID'],
        ExcelData[i]['Employee Name'],
        ExcelData[i]['Movement Type'],
        ExcelData[i]['Movement Initiation Date'],
        ExcelData[i]['Movement Effective Date'],
        ExcelData[i]['Event Status'],
        ExcelData[i]['Previous Band'],
        ExcelData[i]['New Band'],
        ExcelData[i]['Previous Designation'],
        ExcelData[i]['New Designation'],
        ExcelData[i]['Previous Department'],
        ExcelData[i]['New Department'],
        ExcelData[i]['Previous Sub Department'],
        ExcelData[i]['New Sub Department'],
        ExcelData[i]['Previous Sub Sub Department'],
        ExcelData[i]['New Sub Sub Department'],
        ExcelData[i]['Previous Location'],
        ExcelData[i]['New Location'],
        ExcelData[i]['Previous Manager Employee Code'],
        ExcelData[i]['New Manager Employee Code'],
        ExcelData[i]['Previous HRP Employee Code'],
        ExcelData[i]['New HRP Employee Code'],
        ExcelData[i]['First Band Movement'],
    ], function(err, res) {
        if (err) {
            unsuccessfuluser.push(ExcelData[i]);
            resions.push({ emp_code: ExcelData[i].employee_code, err: err });
            user_processlog(newdb, 'SFTP ROLE CHANGE DATA INSERT', "user added", err.message, type, userTId, ExcelData[i].employee_code);
        } else {
            if (res[0].length > 0) {
                ExcelData[i].result = res[0][0].SQLResult;
                ExcelData[i].flag = res[0][0].OStatus;
                ExcelData[i].isUserCreated = res[0][0].isUserCreated;
                userList.push(ExcelData[i]);
                user_processlog(newdb, 'SFTP ROLE CHANGE DATA INSERT', "user added", res[0][0].SQLResult, type, userTId, ExcelData[i].employee_code);
            }
        }
        SaveMultiUser(newdb, ExcelData, userId, userTId, i + 1, userList, unsuccessfuluser, updateuserList, resions, type, callback);
    });
}


function GetFormattedDate(date) {
    if (date) {
        var todayTime = new Date(date);
        var month = todayTime.getMonth() + 1;
        var day = todayTime.getDate();
        var year = todayTime.getFullYear();
    } else {
        var todayTime = new Date();
        todayTime.setDate(todayTime.getDate() - 1);
        var month = todayTime.getMonth() + 1;
        var day = todayTime.getDate();
        var year = todayTime.getFullYear();
    }


    if (day < 10) {
        day = "0" + day
    }
    if (month < 10) {
        month = "0" + month
    }
    return year + "" + month + "" + day;
}

function processlog(db, type, activity, desc, trig, tId) {
    var resourceSQL = "call SP_Automated_Process_log(?,?,?,?,?)";
    db.query(resourceSQL, [type, activity, desc, trig, tId], function(err, res) {
        if (err) {
            // console.log('err ', err);
        } else {
            //  console.log('res ', res);

        }
    });
}

function user_processlog(db, type, activity, desc, trig, tId, ecn) {
    var resourceSQL = "call SP_Automated_Process_log_sftpuser(?,?,?,?,?,?)";
    db.query(resourceSQL, [type, activity, desc, trig, tId, ecn], function(err, res) {
        if (err) {
            // console.log('err ', err);
        } else {
            //  console.log('res ', res);

        }
    });
}


function changeDate(value) {
    if (value == null || value == '') {
        return value
    } else {
        const splitarray = value.includes('/') ? value.split('/') : value.split('-');
        const date = splitarray.reverse().join('-');
        return date;
    }
};

function sentprcesslog(db, type) {

    var resourceSQL = "call SP_Send_process_log();";
    db.query(resourceSQL, [], function(err, res) {
        if (err) {
            processlog(db, 'SFTP ROLE CHANGE DATA INSERT MAIL TO ADMIN', "getdata", err, type, 0);
        } else {
            if (res[0].length > 0) {
                var to = [];
                var subject = 'SFTP role change data insert log';
                var messages = `Sftp role change data insert log as follows ` + res[1][0].alldescription;
                var username = 'Admin';
                to.push('umesh@mobeserv.com');
                to.push('swapnali@mobeserv.com');
                to.push('support@mobeserv.com');
                /* to.push('swapnali@mobeserv.com');
                to.push('support@mobeserv.com');
                to.push('umesh@mobeserv.com');*/
                to.push('bhavesh@mobeserv.com');
                to.push('prashant.ingale@bajajfinserv.in');
                to.push('rohit.dolekar@bajajfinserv.in');//newly added data
                to.push('yogita.shelar@bajajfinserv.in');//newly added data
                
                mail.sendMail(to, subject, messages, username, (result) => {
                    processlog(db, 'SFTP USER CREATION MAIL TO ADMIN', "mail sent to admin", 'sucessfully', type, 0);
                });
            }
        }
    });
};

exports.getTenantListRolechangeSftp = (server, db) => {
    processlog(db, 'SFTP USER CREATION', "attempt", totalattempt + 1, 'AUTO', 0);
    //let startTimeStamp = new Date();
    let _typea = 'AUTO';
    try {
        // var db = req.db;
        var sql = 'call SP_GET_RoleChange_Tenant_List()';
        db.query(sql, [], function (err, list) {
            if (err) {
                processlog(db, 'getTenantListRolechangeSftp', "Get tenant list", 'Error' + err.message, 'AUTO', 0);

            } else {
                processlog(db, 'getTenantListRolechangeSftp', "Get tenant list", 'sucessfull', 'AUTO', 0);
                if (list[0].length > 0) {
 

                    let count = 0;
                    let endLength = list[0].length - 1;
                    console.log('Role change list', list[0])
                    const pre = () =>{
                        if(endLength < count){
                            processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "completed", 'sucessfully', _typea, 0);
                            console.log('Done ALL successfull');
                            // setTimeout(() =>{
                                sentprcesslog(db, _typea);
                            // }, 10000);
                            
                            
                        }else {
                            
                            list[0][count]["sftpConData"] = JSON.parse(list[0][count]["sftpConData"]);
                            exports.roleChangeDataBfvl(list[0][count], db, function (resp) {
                                console.log(list[0][count]["tenantId"]+' '+'is connected');
                                count ++;
                                pre();
                            });  
                           
                        }
                    }
                    pre();
                } else {
                    processlog(db, 'getTenantListRolechangeSftp', "NO data Found", 'sucessfull', 'AUTO', 0);
                    sentprcesslog(db, _typea);
                }
            }
        })
    } catch (e) {
        processlog(db, 'getTenantListRolechangeSftp', 'Error in Get tenant list in try block', 'Error' + e.message, 'AUTO', 0);
        sentprcesslog(db, _typea);
    }
}


exports.roleChangeDataBfvl = function(tenantData, db, cb) {
    processlog(db, 'SFTP ROLE CHANGE DATA INSERT'+' '+tenantData["tenantId"], "attempt", totalattempt + 1, 'AUTO', 0);
    let _typea = 'AUTO';
    let sftp = new Client();
    console.log('tenantData', tenantData);
    try {
        sftp.connect(tenantData["sftpConData"]).then(() => {
            processlog(db, 'SFTP ROLE CHANGE DATA INSERT'+' '+tenantData['tenantId'], "SFTP Connection done", 'successfully', _typea, 0);
            console.log('connection successfull');
            return sftp.list(tenantData["sftpConData"]["roleChangefileLocation"]);
        }).then((data) => {


     
                      let tenantList = [];
                      tenantList.push(tenantData);
                      

                            sftpmultiplebfvl(db, sftp, tenantList, data, 0, undefined, _typea, function(resp) {
                                processsftpfile(db, sftp, resp, data, 0, undefined, _typea, function(resp) {
                                    processlog(db, 'SFTP ROLE CHANGE DATA INSERT'+' '+tenantData['tenantId'], "completed", 'sucessfully', _typea, 0);
                                    // console.log('Done ALL successfull');
                                    // sentprcesslog(db, _typea);
                                    cb(true);
                                });
                            });
                     
             
                            
        }).catch((err) => {
            processlog(db, 'SFTP ROLE CHANGE DATA INSERT'+' '+tenantData['tenantId'], "List not read", err.message, _typea, 0);
            if (totalattempt < attempt) {
                totalattempt++;
                exports.roleChangeDataBfvl(tenantData, db, cb);
            }
            cb(false);
            //console.log(err, 'catch error');
        });
    } catch (e) {
        //console.log(e);
        processlog(db, 'SFTP ROLE CHANGE DATA INSERT'+' '+tenantData['tenantId'], "SFTP Server not connected", e.message, _typea, 0);
        if (totalattempt < attempt) {
            totalattempt++;
            exports.roleChangeDataBfvl(tenantData, db, cb);
        }
        cb(false);
    }
}


function sftpmultiplebfvl(db, sftp, tlist_array, filelistdata, u, date, type, callback) {
    if (u == tlist_array.length || tlist_array.length === 0)
        return callback(tlist_array);
    //console.log(tlist_array[u]);
    let todaydate = GetFormattedDate(date);
    let genfname = tlist_array[u].fileName + todaydate;
    processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "List read successfully", 'successfull', type, tlist_array[u].tenantId);
    var found = filelistdata.find(function(element) {
        let filename = element.name;
        let tempfname = filename.substring(tlist_array[u].fsubStringstart, tlist_array[u].fsubStringEnd);
        // console.log('tempfname', tempfname);globalpath
        if (tempfname == genfname) {
            return element;
        }

    });
    //console.log(found.name);
    if (found != undefined) {
        processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "File found", 'File found for ' + todaydate, type, tlist_array[u].tenantId);
       // sftp.fastGet('/EMP_MOVEMENT_ILLUME/' + found.name,  + found.name, []).then((data) => {
        console.log('filepath :',tlist_array[u]["sftpConData"]["roleChangefileLocation"]+'/'+ found.name)
        sftp.fastGet(tlist_array[u]["sftpConData"]["roleChangefileLocation"]+'/'+ found.name, globalpath + found.name, []).then((data) => {
            // tlist_array[u]["sftpConData"]["fileLocation"]+'/'
            //console.log(data, 'the data info');
            processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "File downloaded successfully", 'successfull for ' + todaydate, type, tlist_array[u].tenantId);
            // readfileanddata(globalpath + found.name, db, tlist_array, filelistdata, u, sftp, date, type,callback);
            tlist_array[u].filepath = globalpath + found.name;
            sftpmultiplebfvl(db, sftp, tlist_array, filelistdata, u + 1, date, type, callback);
        }, (err) => {
            //console.log(err, 'catch error');
            tlist_array[u].filepath = '';
            processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "File download err", err.message, type, tlist_array[u].tenantId);
            sftpmultiplebfvl(db, sftp, tlist_array, filelistdata, u + 1, date, type, callback);
        });
    } else {
        console.log('file not found');
        processlog(db, 'SFTP ROLE CHANGE DATA INSERT', "File found", 'File not found for ' + todaydate, type, tlist_array[u].tenantId);
        sftpmultiplebfvl(db, sftp, tlist_array, filelistdata, u + 1, date, type, callback);
    }

}