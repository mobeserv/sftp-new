var Client = require('ssh2-sftp-client');
var config = require('./config');
const mail = require("./mail");
var globalpath = __dirname + '/sftpfiles/';
var dummyfiles = config.sftpconnection.pmstestingfiles;
var csv = require('csvtojson');
const xlsxTojson = require("xlsx-to-json-lc");

var maxAttemptsArray = [1, 2, 3];
var maxAttempts = maxAttemptsArray.length;
var totalAttempts = 0;

var pl = require('./processLog');
var SFTP = require("./SFTPemployee");
const QP = require('./QP');
const exceljs = require('./excel-js/exceljsHelper');
const ErrorLog = require("./manage");

const AWS = require("aws-sdk"); // from AWS SDK
const FILE_PERMISSION = config.s3.acl.public
const BUCKET = config.s3.bucket;
const KEY = config.s3.accesskey;
const SECRET = config.s3.secretkey;
const s3 = new AWS.S3({ signatureVersion: 'v4', accessKeyId: KEY, secretAccessKey: SECRET });

const fs = require('fs');
const dirPath = require('path').resolve('./');

exports.pmsSftpAPI = (req, res) => {
    try {
        var files = req.files;
        Object.keys(files).forEach(async key => {
            // console.log(key, files[key]);
            var { err, data } = await uploadfiles(files[key]);
            if (err) {
                console.log(data);
                res.json({
                    data: data
                });
            }
            else {
                console.log(data);
            }
        });
        res.json({
            data: 'Pms Sftp Enrolment started'
        });
        exports.pmsSftp(req.db, 'manual');
    } catch (error) {
        console.log('error : ', error.message)
    }
}

exports.pmsSftp = async (db, mode) => {
    var { err, data } = await exports.getTenantList(db);
    if (err) {
        pl.processlog(db, 'pms Sftp Enrolment', "getTenantList", data, mode, 0);
    }
    else {
        if (data && Array.isArray(data) && data.length > 0) {
            var { err, data } = await exports.pmsSftpEnrol(db, data, mode);
            if (err) {
                pl.processlog(db, 'pms Sftp Enrolment', "pmsSftpEnrol", data, mode, 0);
            }
            else {
                // console.log(data);
                pl.processlog(db, 'pms Sftp Enrolment', "pmsSftpEnrol", data, mode, 0);
            }
        }
        else {
            pl.processlog(db, 'pms Sftp Enrolment', "getTenantList", 'No data found in getTenantList', mode, 0);
        }
    }
}

exports.getTenantList = async (db) => {
    return new Promise((res, rej) => {
        var sql = 'call SP_GET_SFTP_PMS_Tenant_List()';
        db.query(sql, [], function (err, result) {
            if (err) {
                res({ err: true, data: err.message })
            } else if (result && Array.isArray(result) && result[0].length > 0) {
                res({ err: false, data: result[0] })
            }
            else {
                res({ err: false, data: '' })
            }
        })
    })
}

exports.pmsSftpEnrol = async (db, tenantData, mode) => {
    pl.processlog(db, 'pms Sftp Enrolment', 'pmsSftpEnrol', 'Started', mode, 0);

    return new Promise(async (res, rej) => {
        var SFTPConnectionResult = {};
        var SFTPLastAttemptErr = true;

        var tenantArray = [];
        if (tenantData && tenantData && tenantData.length > 0) {
            tenantArray = tenantData;
            await asyncForEach(tenantArray, async (item) => {
                item["sftpConData"] = JSON.parse(item["sftpConData"]);
                let sftpConfig = item['sftpConData'];
                let sftpFileLocation = item["sftpConData"]["fileLocation"];
                let tId = item['tenantId'];
                let tenantData = item;
                await asyncForEach(maxAttemptsArray, async (item) => {
                    if (totalAttempts < maxAttempts && SFTPLastAttemptErr) {
                        var { err, data } = await exports.sftpConnect(db, sftpConfig, sftpFileLocation, tId, mode);

                        SFTPLastAttemptErr = err;
                        SFTPConnectionResult = data;

                    }
                })
                totalAttempts = 0;
                if (SFTPLastAttemptErr) {
                    pl.processlog(db, 'pms Sftp Enrolment', "sftpConnect", SFTPConnectionResult, mode, tId);
                    res({ err: true, data: SFTPConnectionResult })
                }
                else {
                    var { err, data } = await exports.downloadSftpFiles(db, SFTPConnectionResult, tenantData, mode, sftpFileLocation);
                    if (err) {
                        pl.processlog(db, 'pms Sftp Enrolment', "downloadSftpFiles", data, mode, tId);
                        res({ err: true, data: data })
                    }
                    else {
                        var { err, data } = await exports.getFileSubData(db, data, tenantData, mode);
                        if (err) {
                            pl.processlog(db, 'pms Sftp Enrolment', "getFileSubData", data, mode, tId);
                            res({ err: true, data: data })
                        }
                        else {
                            // console.log(data);
                            var excelData = data;
                            // console.log(excelData);
                            var subData = tenantData['sftpConData']['subData'];
                            // console.log(subData);
                            var subDataArray = tenantData['sftpConData']['subDataArray'];
                            // console.log(subDataArray);
                            await asyncForEach(subDataArray, async (item) => {
                                // Object.keys(subData).forEach(async (item) => {
                                // console.log(item);
                                tId = item;
                                // console.log(tId);
                                var tName = subData[tId];
                                // console.log(tName);
                                var tExcelData = excelData[tName];
                                // console.log(tExcelData);
                                var { err, data } = await exports.getDataString(db, tExcelData, tId, mode);
                                if (err) {
                                    pl.processlog(db, 'pms Sftp Enrolment', "getDataString", data, mode, tId);
                                    res({ err: true, data: data })
                                }
                                else {
                                    var { err, data } = await exports.enrolUser(db, data, tId, mode);
                                    if (err) {
                                        pl.processlog(db, 'pms Sftp Enrolment', "enrolUser", data, mode, tId);
                                        res({ err: true, data: data })
                                    }
                                    else {
                                        var { err, data } = await exports.generateReportAndSendMail(db, data, tId, mode);
                                        if (err) {
                                            pl.processlog(db, 'pms Sftp Enrolment', "generateReportAndSendMail", data, mode, tId);
                                            res({ err: true, data: data })
                                        }
                                        else {
                                            pl.processlog(db, 'pms Sftp Enrolment', "Completed", 'Successfully', mode, tId);
                                            exports.sendEnrolmentNotification(db, tId, mode);
                                        }
                                    }
                                }
                            })
                            res({ err: true, data: 'Completed' })
                        }
                    }
                }
            })
        }
    })
}

exports.sftpConnect = async (db, sftpConfig, fileLocation, tId, mode) => {
    return new Promise((res, rej) => {
        totalAttempts++;
        pl.processlog(db, 'pms Sftp Enrolment', "pms Sftp Connection attempt", `attempt : ${totalAttempts}`, mode, tId);
        let sftp = new Client();
        sftp.connect(sftpConfig)
            .then(() => {
                pl.processlog(db, 'pms Sftp Enrolment', "pms Sftp Connection done", 'successfully', mode, tId);
                console.log('pms Sftp connection successful');
                return sftp.list(config.sftpconnection.pmstesting ? '/' : fileLocation);
            }).then(result => {
                // console.log(result, 'the data info');
                result['client'] = sftp;
                res({ err: false, data: result })
            }).catch((err) => {
                res({ err: true, data: err.message })
            });

    })
}

exports.downloadSftpFiles = async (db, clientInfo, tenantData, mode, fileLocation) => {
    return new Promise((res, rej) => {

        let sftp = clientInfo['client'];
        let todaydate = SFTP.GetFormattedDate(undefined);
        // let genfname = tenantData['fileName'] + todaydate;
        // let sftpConData = tenantData['tenantId'];
        let genfname = tenantData['fileName'];
        pl.processlog(db, `pms Sftp Enrolment`, "List read successfully", 'successfull', mode, tenantData['tenantId']);

        var found = clientInfo.find((element) => {
            let filename = element.name;
            // let tempfname = filename.substring(tenantData['fsubStringstart'], tenantData['fsubStringEnd']);
            let tempfname = filename.substring(0, filename.indexOf(' '));
            if (tempfname == genfname) {
                return element;
            }

        });
        // console.log(found.name);
        if (found != undefined) {
            pl.processlog(db, 'pms Sftp Enrolment', "File found", found.name, mode, tenantData['tenantId']);
            let filename = '';
            let filepath = '';
            if (config.sftpconnection.pmstesting && dummyfiles[tenantData['tenantId']]) {
                filename = dummyfiles[tenantData['tenantId']]
                filepath = globalpath + filename; //found.name;
                res({ err: false, data: filepath })
            }
            else {
                fileLocation = String(fileLocation + found.name);
                console.log('File location -', fileLocation);
                sftp.fastGet(fileLocation, globalpath + found.name, []).then((data) => {
                    pl.processlog(db, 'pms Sftp Enrolment', "File downloaded successfully", found.name, mode, tenantData['tenantId']);

                    filename = found.name;

                    filepath = globalpath + filename; //found.name;

                    res({ err: false, data: filepath })
                }, (err) => {
                    pl.processlog(db, 'pms Sftp Enrolment', "File download err", err.message, mode, tenantData['tenantId']);
                    res({ err: true, data: err.message })
                });
            }
        } else {
            console.log('file not found');
            pl.processlog(db, 'pms Sftp Enrolment', "File not found", 'File not found for ' + todaydate, mode, tenantData['tenantId']);
            if (config.sftpconnection.pmstesting && dummyfiles[tenantData['tenantId']]) {
                filename = dummyfiles[tenantData['tenantId']]
                filepath = globalpath + filename; //found.name;
                res({ err: false, data: filepath })
            }
            else {
                pl.processlog(db, 'pms Sftp Enrolment', "File not found", 'File not found for ' + todaydate, mode, tenantData['tenantId']);
                res({ err: true, data: "File not found" })
            }
        }
    })
}

exports.getFileSubData = async (db, filepath, tenantData, mode) => {
    return new Promise((res, rej) => {
        console.log(filepath);
        xlsxTojson({
            input: filepath, 
            output: null, //since we don't need output.json
            lowerCaseHeaders:false //converts excel header rows into lowercase as json keys
          }, (err, result) => {
            if(err) {
                pl.processlog(db, 'pms Sftp Enrolment', "getFileSubData", err.message, mode, tenantData['tenantId']);
            }else {
                let jsonObj = result;
                pl.processlog(db, 'pms Sftp Enrolment', 'getFileSubData', 'started', mode, tenantData['tenantId']);
                let subData = tenantData['sftpConData']['subData'];
                let resultData = {};
                // resultData['subData'] = subData;
                for (const key in jsonObj) {
                    for (const tenant in subData) {
                        //console.log(tenant)
                        //console.log(key, jsonObj[key]);
                        let rowData = jsonObj[key]['Company Name'];
                        if (rowData === subData[tenant]) {
                            if (!resultData[subData[tenant]]) {
                                resultData[subData[tenant]] = [];
                                // resultData[subData[tenant]]['tenantId'] = tenant;
                                resultData[subData[tenant]].push(jsonObj[key])
                            } else {
                                resultData[subData[tenant]].push(jsonObj[key])
                            }
                        }
                    }
                }
                // console.log(resultData);
                // console.log(typeof(resultData));
                pl.processlog(db, 'pms Sftp Enrolment', 'getFileSubData', 'completed', mode, tenantData['tenantId']);
                res({ err: false, data: resultData })
            }
          });

        // console.log(filepath);
        // csv()
        //     .fromFile(filepath)
        //     .then(async (jsonObj) => {
        //         pl.processlog(db, 'pms Sftp Enrolment', 'getFileSubData', 'started', mode, tenantData['tenantId']);
        //         let subData = tenantData['sftpConData']['subData'];
        //         let resultData = {};
        //         // resultData['subData'] = subData;
        //         for (const key in jsonObj) {
        //             for (const tenant in subData) {
        //                 //console.log(tenant)
        //                 //console.log(key, jsonObj[key]);
        //                 let rowData = jsonObj[key]['Company Name'];
        //                 if (rowData === subData[tenant]) {
        //                     if (!resultData[subData[tenant]]) {
        //                         resultData[subData[tenant]] = [];
        //                         // resultData[subData[tenant]]['tenantId'] = tenant;
        //                         resultData[subData[tenant]].push(jsonObj[key])
        //                     } else {
        //                         resultData[subData[tenant]].push(jsonObj[key])
        //                     }
        //                 }
        //             }
        //         }
        //         // console.log(resultData);
        //         // console.log(typeof(resultData));
        //         pl.processlog(db, 'pms Sftp Enrolment', 'getFileSubData', 'completed', mode, tenantData['tenantId']);
        //         res({ err: false, data: resultData })
        //     }).catch((err) => {
        //         pl.processlog(db, 'pms Sftp Enrolment', "getFileSubData", err.message, mode, tenantData['tenantId']);
        //         res({ err: true, data: err.message })
        //     });
    })
}

exports.getDataString = async (db, jsonObj, tenantId, mode) => {
    return new Promise(async (res, rej) => {
        pl.processlog(db, 'pms Sftp Enrolment', 'getDataString', 'started', mode, tenantId);
        let dataString = '';
        let primarySep = '%pipe%';
        let secondarySep = '%dollar%';
        var { err, data } = await exports.getFileColumns(db, tenantId, mode);
        if (err) {
            res({ err: true, data: data })
        }
        else {
            let columnList = data;
            for (const i in jsonObj) {
                //console.log(key, jsonObj[key]);
                let item = jsonObj[i];
                //let columnList = ['Employee code','Content Type','Content Code','Conversation template name','Enrollment type','Enrollment date'];
                for (const j in columnList) {
                    // console.log(item[columnList[j]]);
                    dataString += item[columnList[j]] + primarySep;
                }
                dataString += secondarySep;
            }
            // console.log(dataString);
            pl.processlog(db, 'pms Sftp Enrolment', 'getDataString', 'completed', mode, tenantId);
            res({ err: false, data: dataString })
        }

    })
}

exports.enrolUser = async (db, dataString, tenantId, mode) => {
    return new Promise(async (res, rej) => {
        pl.processlog(db, 'pms Sftp Enrolment', "enrolUser", 'started', mode, tenantId);
        const qP = new QP(db);
        const SQL = 'call SP_PMS_SFTP_enrolment(?,?);';

        await qP.queryPromise(SQL, [dataString, tenantId])
            .then((result) => {
                if (result && Array.isArray(result) && result.length > 0) {
                    pl.processlog(db, 'pms Sftp Enrolment', "enrolUser", 'completed', mode, tenantId);
                    res({ err: false, data: result })
                }
            })
            .catch((err) => {
                res({ err: true, data: err.message })
            })
    })
}

exports.getFileColumns = async (db, tenantId, mode) => {
    return new Promise(async (res, rej) => {
        pl.processlog(db, 'pms Sftp Enrolment', "getFileColumns", 'started', mode, tenantId);
        const qP = new QP(db);
        const SQL = 'call SP_PMS_SFTP_get_column_list(?);';

        await qP.queryPromise(SQL, [tenantId])
            .then((result) => {
                if (result && Array.isArray(result) && result[0].length > 0) {
                    let columns = JSON.parse(result[0][0]['columns']);
                    pl.processlog(db, 'pms Sftp Enrolment', "getFileColumns", 'completed', mode, tenantId);
                    res({ err: false, data: columns })
                }
            })
            .catch((err) => {
                //pl.processlog(db, 'pms Sftp Enrolment', "getFileColumns", err.message, mode, tenantId);
                res({ err: true, data: err.message })
            })
    })
}

exports.generateReportAndSendMail = async (db, ExcelData, tId, mode) => {
    return new Promise(async (res, rej) => {
        var { err, data } = await exports.get_pms_enrollment_report(db, ExcelData, tId, mode);
        if (err) {
            pl.processlog(db, 'pms Sftp Enrolment', "get_pms_enrollment_report", data, mode, tId);
            res({ err: true, data: data })
        }
        else {
            var { err, data } = await exports.uploadtos3(data, db, tId);
            if (err) {
                pl.processlog(db, 'pms Sftp Enrolment', "uploadtos3", data, mode, tId);
                res({ err: true, data: data })
            }
            else {
                var { err, data } = await saveLink(db, tId, data['link'], data['file']['path'], data['file']['name']);
                if (err) {
                    pl.processlog(db, 'pms Sftp Enrolment', "saveLink", data, mode, tId);
                    res({ err: true, data: data })
                }
                else {
                    pl.processlog(db, 'pms Sftp Enrolment', "saveLink", data, mode, tId);
                    exports.sendEmailPmsSftp(db, tId, mode);
                    res({ err: false, data: '' });
                }
            }
        }
    })
}

exports.sendEmailPmsSftp = async (db, tId, mode) => {
    try {
        var resourceSQL = "call SP_PMS_SFTP_send_mail(?);";
        db.query(resourceSQL, [tId], async function (err, res) {
            if (err) {
                pl.processlog(db, 'pms Sftp Enrolment MAIL TO ADMIN', "error in sendEmailPmsSftp", err.message, mode, tId);
            } else {
                if (res[0].length > 0) {
                    let a = await mail.sendmailcc(res[0][0].to, res[0][0].subject, res[0][0].message, res[0][0].username, res[0][0].cc);
                    pl.processlog(db, 'pms Sftp Enrolment MAIL TO ADMIN', JSON.stringify(res[0][0].to) + ": mail sent to admin :", 'sucessfully', mode, tId);
                }
            }
        });
    }
    catch (err) {
        pl.processlog(db, 'pms Sftp Enrolment MAIL TO ADMIN', "error in sendEmailPmsSftp", err.message, mode, tenantId);
    }
};

exports.get_pms_enrollment_report = async (db, excelData, tenantId, mode) => {
    return new Promise(async (resolve, reject) => {
        pl.processlog(db, 'pms Sftp Enrolment', "get_pms_enrollment_report", 'started', mode, tenantId);
        const newdb = db;
        try {
            if (excelData && excelData.length > 0) {

                const timestamp = Date.now();
                const dateObject = new Date(timestamp);
                const date = dateObject.getDate();
                const month = dateObject.getMonth() + 1;
                const year = dateObject.getFullYear();
                var datetime = `${year}-${month}-${date}`;
                var fileName = 'pms_' + datetime + '_' + tenantId
                const data = {
                    "name": fileName,
                    "headerArray": parseJsonFun(excelData[0], 1),
                    "rows": excelData[1]
                };

                const object = data;
                const workbook = exceljs.jsonToExcel(object);
                workbook.removeWorksheet('MasterData');

                // let filepath = dirPath + "/pmsReports/" + fileName + ".xlsx";
                let filepath = config.mntdir + "/pmsReports/" + fileName + ".xlsx";

                var filefors3 = {
                    path: filepath,
                    name: fileName + ".xlsx"
                }

                await workbook.xlsx
                    .writeFile(filepath)
                    .then(async () => {
                        pl.processlog(db, 'pms Sftp Enrolment', "get_pms_enrollment_report", 'completed', mode, tenantId);
                        // await uploadtos3(filefors3, db, tenantId);
                        // resolve(1);
                        resolve({ err: false, data: filefors3 })
                    }
                    );

            } else {
                pl.processlog(db, 'pms Sftp Enrolment', "get_pms_enrollment_report", 'No data available', mode, tenantId);
                // ErrorLog.inserterrorlog('get_pms_enrollment_report', 'No data available', 'API', null);
                resolve({ err: true, data: 'No data available' })
            }
            // })
        }
        catch (err) {
            console.log(err.message);
            pl.processlog(db, 'pms Sftp Enrolment', "get_pms_enrollment_report", err.message, mode, tenantId);
            ErrorLog.inserterrorlog('get_pms_enrollment_report', err.message, 'API', null);
            resolve({ err: true, data: err.message })
        }
    })
}

exports.uploadtos3 = async (file) => {
    return new Promise((resolve, reject) => {
        const Key = file.name;
        s3.upload({
            Key,
            Bucket: BUCKET,
            Body: fs.readFileSync(file.path),
            ACL: FILE_PERMISSION
        },
            async (err, data) => {
                if (err) {
                    //ErrorLog.inserterrorlog('Error in Upload to s3', err.message, 'uploadtos3', null);
                    resolve({ err: true, data: err.message });
                }
                else {
                    let link = data.Location;
                    if (link != null && link != '') {
                        let resultData = {};
                        resultData['link'] = link;
                        resultData['file'] = file;
                        resolve({ err: false, data: resultData });
                    }
                    else {
                        resolve({ err: true, data: 'Link not found' });
                    }
                }
            });
    })
}

async function saveLink(db, tenantId, link, deletepath, fileName) {
    return new Promise(async (resolve, reject) => {
        const SQL = 'call SP_pms_sftp_link_save(?,?,?);';
        var params = [fileName, link, tenantId];
        db.query(SQL, params, async function (err, result) {
            if (err) {
                ErrorLog.inserterrorlog('Error in PMS SFTP Report Links', err.message, 'saveLink', null);
                resolve({ err: true, data: err.message })
            }
            // console.log(result, '', tenantId);
            if (result && Array.isArray(result) && result[0].length != 0 && result[0][0]) {

                fs.unlink(deletepath, (err) => {
                    if (err) {
                        ErrorLog.inserterrorlog('Error in PMS SFTP Report Links', err.message, 'saveLink', null);
                    }
                    else {
                        console.log("File Deleted successfully....");
                    }
                    resolve({ err: false, data: 'Link Saved and File Deleted successfully' })
                });
            } else {
                resolve({ err: true, data: 'Not Found' })
            }
        })
    })
}

function uploadfiles(file) {
    return new Promise((resolve, reject) => {
        try {
            if (!file) { return resolve(true, 'File not found'); }

            const newpath = __dirname + '/sftpfiles/' + file.name;
            file.mv(newpath)
                .then(() => {
                    resolve({ err: false, data: newpath });
                })
                .catch(err => {
                    resolve({ err: true, data: err.message });
                });
        } catch (error) {
            resolve({ err: true, data: error.message });
        }
    })
}

exports.sendEnrolmentNotification = async (db, tId, mode) => {
    var resourceSQL = "call SP_send_SFTP_PMS_enrolment_mail(?);";
    db.query(resourceSQL, [tId], function (err, res) {
        if (err) {
            pl.processlog(db, 'sendEnrolmentNotification', "error", err.message, mode, tId);
        } else {
            pl.processlog(db, 'sendEnrolmentNotification', "Enrolment mail triggered", 'sucessfully', mode, tId);
        }
    });
}

function parseJsonFun(list, type) {

    if (type == 1) {
        list.forEach(element => {
            if (element["value"] != "" && typeof element["value"] == 'string') {
                element["value"] = JSON.parse(element["value"]);
            }
        });
    } else if (type == 2) {
        list.forEach(element => {
            if (element["list"] != "" && typeof element["list"] == 'string') {
                element["list"] = JSON.parse(element["list"]);
            }
            if (element["localFilterParams"] != "" && typeof element["localFilterParams"] == 'string') {
                element["localFilterParams"] = JSON.parse(element["localFilterParams"]);
            }
            if (element["filterMetadata"] != "" && typeof element["filterMetadata"] == 'string') {
                element["filterMetadata"] = JSON.parse(element["filterMetadata"]);
            }
        });
    }

    return list;
}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}
