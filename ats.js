var config = require('./config');
var Client = require('ssh2-sftp-client');
var attempt = 3;
var totalattempt = 0;
var sftpemployee = require('./SFTPemployee');
var pl = require('./processLog');
const mail = require("./mail");
const json2csv = require("json2csv");
const fs = require('fs');

exports.atsUserAdd = function (server, db) {

    pl.processlog(db, 'ATS USER CREATION', "attempt", totalattempt + 1, 'AUTO', 0);
    //startTimeStamp = new Date();
    //var isException=0;
    let ats = new Client();
    //try {
        pl.processlog(db, 'ATS USER CREATION', "initiated", "Procces initiated", 'AUTO', 0);
        ats.connect(config.ats.connection).then(() => {
            pl.processlog(db, 'ATS USER CREATION', "ATS Connection done", 'successfully', 'AUTO', 0);
            console.log('connection successfull');
            return ats.list(config.ats.fileLocation);
        }).then((data) => {
            //console.log(data, 'the data info');
            // for(let i =0 ;i<tlist_array.length;i++)
            // {   

            // }

            try {
                // var db = req.db;
                var sql = 'call SP_GET_ATS_Tenant_List()';
                db.query(sql, [], function (err, list) {
                    if (err) {
                        pl.processlog(db, 'ATS USER CREATION', "Get tenant list", 'Error' + err.message, 'AUTO', 0);
                        // isException=1;
                        // endTimeStamp=new Date();
                        // ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
                        // logEnum.process.ats, logEnum.process.ats,startTimeStamp,endTimeStamp,
                        // isException,logEnum.exceptionType[500].value,logEnum.exceptionType[500].name,null,null);
                    } else {
                        pl.processlog(db, 'ATS USER CREATION', "Get tenant list", 'sucessfull', 'AUTO', 0);
                        if (list[0].length > 0) {
                            sftpemployee.sftpmultiple(db, ats, list[0], data, 0, undefined, 'AUTO', 'ATS', function (resp) {
                                sftpemployee.processsftpfile(db, ats, resp, data, 0, undefined, 'AUTO', 'ATS', function (resp) {
                                    pl.processlog(db, 'ATS USER CREATION', "completed", 'sucessfully', 'AUTO', 0);
                                    //
                                    // endTimeStamp=new Date();
                                    // ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
                                    // logEnum.process.ats, logEnum.process.ats,startTimeStamp,endTimeStamp,
                                    // isException,logEnum.exceptionType[200].value,logEnum.exceptionType[200].name,null,null);
                                    //enrol.enrolementcron(server,db);
                                    //markusers_as_inactive(db, server, 'AUTO');
                                    console.log('Done ALL ATS successfull');
                                    exports.sendEmailsftpats(db, 2,'ATS');
                                    sftpemployee.markusers_as_inactive(db, server, 'AUTO');
                                    //setTimeout(function () { sentprcesslog(db); }, 3000);
                                });
                            });
                        } else {
                            pl.processlog(db, 'ATS USER CREATION', "NO data Found", 'sucessfull', 'AUTO', 0);
                            sftpemployee.markusers_as_inactive(db, server, 'AUTO');
                            //404
                            // endTimeStamp=new Date();
                            // ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
                            // logEnum.process.ats, logEnum.process.ats,startTimeStamp,endTimeStamp,
                            // isException,logEnum.exceptionType[404].value,logEnum.exceptionType[404].name,null,null);
                        }
                    }
                })
            } catch (e) {
                pl.processlog(db, 'ATS USER CREATION', 'Error in Get tenant list in try block', 'Error' + e.message, 'AUTO', 0);
                sftpemployee.markusers_as_inactive(db, server, 'AUTO');
                // isException=1;
                // endTimeStamp=new Date();
                // ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
                // logEnum.process.ats, logEnum.process.ats,startTimeStamp,endTimeStamp,
                // isException,logEnum.exceptionType[500].value,logEnum.exceptionType[500].name,null,null);
            }
            ats.on('close', function (result) {
                pl.processlog(db, 'ATS USER CREATION', "ATS Connection Closed", "Close event", 'AUTO', 0);
            });
            ats.on('end', function (result) {
                pl.processlog(db, 'ATS USER CREATION', "ATS Connection ended", "End event", 'AUTO', 0);
            });
            ats.on('finish', function (result) {
                pl.processlog(db, 'ATS USER CREATION', "ATS Connection Finished", "Finish event", 'AUTO', 0);
            });
            ats.on('error', function (err) {
                pl.processlog(db, 'ATS USER CREATION', "ATS Connection Error", "Error event", 'AUTO', 0);
                // isException=1;
                // endTimeStamp=new Date();
                // ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
                // logEnum.process.ats, logEnum.process.ats,startTimeStamp,endTimeStamp,
                // isException,logEnum.exceptionType[500].value,logEnum.exceptionType[500].name,null,null);
            });
        }).catch((err) => {
            
            //console.log(err);
            if (err) {
                pl.processlog(db, 'ATS USER CREATION', "List not read", err.message, 'AUTO', 0);
            } else {
                pl.processlog(db, 'ATS USER CREATION', "List not read", 'Connection Timeout', 'AUTO', 0);
            }         
            if (totalattempt < attempt) {
                totalattempt++;
                exports.atsUserAdd(server, db);
            }
            else{
                pl.processlog(db, 'ATS USER CREATION', "ATS Server not connected", 'Completed all attempts', 'AUTO', 0);
                sftpemployee.markusers_as_inactive(db, server, 'AUTO');
                // isException=1;
                // endTimeStamp=new Date();
                // ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
                // logEnum.process.ats, logEnum.process.ats,startTimeStamp,endTimeStamp,
                // isException,logEnum.exceptionType[500].value,logEnum.exceptionType[500].name,null,null);
            }
            //console.log(err, 'catch error');

        });
    } 
    // catch (e) {
    //     //console.log(e);
    //     pl.processlog(db, 'ATS USER CREATION', "ATS Server not connected", e.message, 'AUTO', 0);
    //     if (totalattempt < attempt) {
    //         totalattempt++;
    //         exports.atsUserAdd(server, db);
    //     }   
    //     else
    //     {
    //         pl.processlog(db, 'ATS USER CREATION', "ATS Server not connected", 'Completed all attempts', 'AUTO', 0);
    //         // isException=1;
    //         // endTimeStamp=new Date();
    //         // ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
    //         // logEnum.process.ats, logEnum.process.ats,startTimeStamp,endTimeStamp,
    //         // isException,logEnum.exceptionType[500].value,logEnum.exceptionType[500].name,null,null);
    //     }   
    // }

//}

exports.sendEmailsftpats = async (db, flag, servertype) => {
    //let db = req.db;
    var resourceSQL = "call SP_send_SFTP_ATS_mail(?);";
    db.query(resourceSQL, [flag], async function (err, res) {
        if (err) {
            pl.processlog(db, `${servertype} USER CREATION MAIL TO ADMIN`, "sendEmailsftpats", err.message, 'AUTO', 0);
        } else {
            if (res[0].length > 0) {
                let a = await mail.sendmailcc(res[0][0].to, res[0][0].subject, res[0][0].message, res[0][0].username, res[0][0].cc);
                pl.processlog(db, `${servertype} USER CREATION MAIL TO ADMIN`, JSON.stringify(res[0][0].to) + ": mail sent to admin :", 'sucessfully', 'AUTO', 0);
                exports.sendparticipantsnotification(db, flag);//mail to newlycreated day - 1 users
            }
        }
    });
};

exports.sendparticipantsnotification = async (db, flag) => {
    var resourceSQL = "call SP_send_SFTP_ATS_participants_mail(?);";
    db.query(resourceSQL, [flag], function (err, res) {
        if (err) {
            pl.processlog(db, 'sendparticipantsnotification', "error", err.message, 'AUTO', 0);
        } else {
                pl.processlog(db, 'sendparticipantsnotification', "Mail sent to participants", 'sucessfully', 'AUTO', 0);
            }
        });
}

exports.uploadReportOnServer = async (db) => { 
    pl.processlog(db, 'ATS uploadReportOnServer', "initiated", "Procces initiated", 'AUTO', 0);
    var resourceSQL = "call SP_get_attendance_report_upload_on_ats(?);";
    db.query(resourceSQL, [1], async function (err, res) {
        if (err) {
            pl.processlog(db, 'uploadReportOnServer', "error", err.message, 'AUTO', null);
        } else {
            let ats = new Client();
            var csv = res[0].length > 0 ? json2csv.parse(res[0]) : json2csv.parse(res[1]);
            let filename = await generateFile(csv, res[2][0].localPath);
            let remotepath = config.ats.uploadFileLocation + filename;
            let localpath = config.mntdir + res[2][0].localPath + filename;
            //let localpath = "C:\\Arsalan\\Server" + res[2][0].localPath + filename;

            ats.connect(config.ats.connection).then(() => {
                ats.put(localpath, remotepath, false);
            }).then(() => {
                //console.log(data, 'the data info');
                fs.unlink(localpath, (err) => {
                    if (err) {
                        pl.processlog(db, 'uploadReportOnServer', "unlink", err.message, 'AUTO', 0);
                    }
                    else{
                        console.log("File Deleted successfully....");
                    }
                });
                pl.processlog(db, 'ATS uploadReportOnServer', "completed", "Successfully", 'AUTO', 0);
            }).catch((err) => {
                pl.processlog(db, 'uploadReportOnServer', "error", err.message, 'AUTO', 0);
            });
        }
    });
}

const generateFile = async (data, localpath) => {
    return new Promise((res,rej) => {
        const content = data;
        var fileName = generateFileName('.csv');
        const path = config.mntdir + localpath + fileName;
        //const path = "C:\\Arsalan\\Server" + localpath + fileName;
        fs.writeFile(path, content, err => {
        if (err) {
            console.log(err.message);
        }
        else{
            console.log('File written successfully!');
        }
    });
        res (fileName);
    })

}

const generateFileName = (extension) => {
    let D = new Date();
    //let filename = 'Candidate_Attendance_' + `${D.getFullYear()}${D.getMonth() + 1}${D.getDate()}${D.getHours()}${D.getMinutes()}${D.getSeconds()}` + extension;
    let filename = 'Candidate_Attendance_' + `${D.getFullYear()}${("0" + (D.getMonth() + 1)).slice(-2)}${("0" + (D.getDate())).slice(-2)}${("0" + (D.getHours())).slice(-2)}${("0" + (D.getMinutes())).slice(-2)}${("0" + (D.getSeconds())).slice(-2)}` + extension;
    return filename;
  }
