/**
 * Logs (Learning Record Store)
 * @author Bhavesh Tandel
 */

const fs = require('fs');
const Helper = require('./helper');
var logtype = {
    course : 'course',
    module : 'module',
    activity : 'activity',
    }
const logEnum = require('./logEnum');
// const logtype = require('./logEnum')
class Manage{
	
    constructor(app){
        this.helper = Helper;
        this.enum = {
            kpiconstant: logEnum.kpiconstant,
            process: logEnum.process,
            exceptionType: logEnum.ExceptionType
        }
    }

    
    async savedlog(list){

        if(!list || list.length == 0) return null;

        this.savedLoginfile("Insert Log Started","List Count is "+list.length);
    
        this.multipleinserterlog(list, 0, function(result){
            return {data:"sucess"};
        });
    }
    
    async multipleinserterlog (list, i, cb){
    
        if (i == list.length || list.length === 0 ){
            this.savedLoginfile("Insert Log Ended","List Count is "+list.length);
            return cb();
        }
        
        
        var item = list[i];
        var record;
        console.log('logtype.activity',logtype.activity);
        switch (item.logType?item.logType:1)
        {
            case logtype.activity :
                record = await this.helper.insertCourseActivityLogs(item);
            break;
            default :
                record = await this.helper.insertLogs(item);
        } ;
        // this.savedLoginfile("Inserted Data ", i +"\n"+ JSON.stringify(record));
        this.multipleinserterlog(list,i+1,cb);
    }
    async inserterrorlog (eventname, msg, source, userid){
    
        try {
            const record = await this.helper.insertErrorLogs(eventname, msg, source, userid);
        } catch(e) {
            savedErrorLog('inserterrorlog', e);
        }
    }
    
    async insertnotifylog (params){
    
        try {
            const record = await this.helper.insertNotificationogs(params);
        } catch(e) {
            savedErrorLog('inserterrorlog', e);
        }
    }

    async insertapilog (channel, product, process, subProcess,startTimeStamp,endTimeStamp,exception,functionStatusCode,exceptionType,userid,tenantid){
    
        try {
            const record = await this.helper.insertApiLogs(channel, product, process, subProcess,startTimeStamp,endTimeStamp,exception,functionStatusCode,exceptionType,userid,tenantid);
        } catch(e) {
            savedErrorLog('insertapilog', e.message);
        }
    }

    savedLoginfile(eventname,event){
        var logfile = __dirname + "/log/log.txt";
        fs.appendFileSync(logfile, "------"+ eventname +"------"+"\n"+event+"\n"+"Date : "+new Date().toDateString()+" "+new Date().toTimeString()+"\n");
    }

    savedErrorLog(eventname,event){
        var logfile = __dirname + "/log/error.txt";
        fs.appendFileSync(logfile, "------"+ eventname +"------"+"\n"+event+"\n"+"Date : "+new Date().toDateString()+" "+new Date().toTimeString()+"\n");
    }
    
}

module.exports = new Manage();