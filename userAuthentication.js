let jwt = require('jsonwebtoken');
var ErrorLog = require('./manage');
var config = require('./config');

exports.checkToken = function (req, res, next) {
    let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase
    let customHeader = req.headers['custom-header'];
    try {
        if (token) {
            if (token.startsWith('Bearer ')) {
                // Remove Bearer from string
                token = token.slice(7, token.length);
            }
            jwt.verify(token, config.secret, (err, decoded) => {
                if (err) {
                    res.statusCode = 401;
                    return res.json({
                        flag: 0,
                        success: false,
                        message: 'Token is expired'
                    });
                } else {
                    req.decoded = decoded;
                    if (customHeader) {
                        var header = exports.convertCustomHeaders(customHeader);
                        req.headers['custom-header'] = header;
                    }
                    next();
                }
            });
        } else {
            return res.status(401).json({
                flag: 0,
                success: false,
                message: 'Auth token is not supplied'
            });
        }
    }
    catch (error) {
        ErrorLog.inserterrorlog('checkToken', error.message, 'API', null);
        res.status(500).json({
            type: false,
            data: error.message,
            message: 'Something went wrong. please try again later.'
        });
    }

};

exports.convertCustomHeaders = function (str) {
    try {
        if (str) {
            let decryptData = AES.decrypt(str, config.secret);
            let decryptData1 = decryptData.toString(crypto.enc.Utf8);
            // req.headers['custom-header'] = decryptData1;
            return decryptData1;
        }
    } catch (error) {
        ErrorLog.inserterrorlog('convertCustomHeaders', error.message, 'API', null);
        return str;
    }
}