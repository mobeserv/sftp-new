var config = require('./config');
var https = require('https');
var OneSignal = require('onesignal-node');
// create a new Client for a dev app    
var devClient = new OneSignal.Client({
    userAuthKey: 'N2ZmMTQ3MTItZDRhNi00NWFkLThjODMtMWQ1ZGI5NDA4MWYw',
    // note that "app" must have "appAuthKey" and "appId" keys    
    app: { appAuthKey: 'MzlkYWQyMzgtMzcwZC00MDQwLWJkMDUtYmU0YmVmYjJhY2Uy', appId: 'fb8ba139-e430-4b0e-b702-ef5ecbbbbd5f' }
});

// create a new Client for a single app    
var Client = new OneSignal.Client({
    userAuthKey: 'N2ZmMTQ3MTItZDRhNi00NWFkLThjODMtMWQ1ZGI5NDA4MWYw',
    // note that "app" must have "appAuthKey" and "appId" keys    
    app: { appAuthKey: config.onesignal.apikey, appId: config.onesignal.app_id }
});
const dotenv = require('dotenv');
dotenv.config();
const onesignal_app_id = process.env.onesignal_app_id;
const onesignal_rest_api_key = process.env.onesignal_rest_api_key;
const onesignal_channel_id = process.env.onesignal_channel_id;

exports.sendNotification = (data, cb) => {
    var headers = {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": "Basic " + config.onesignal.apikey //"N2E1NzJmZGUtYTZjNi00MTYzLTgzZDMtMDczMjdiNDg5MmRk" Mobeserv // "MDQ1MDhmYTUtNWJmMC00YjYyLWJjNjgtZjhlZmJkZjA5YzVm" EW
    };

    var options = {
        host: "onesignal.com",
        port: 443,
        path: "/api/v1/notifications",
        method: "POST",
        headers: headers
    };
    var req = https.request(options, function(res) {
        res.on('data', function(data) {
            cb(JSON.stringify(data));
        });
    });

    req.on('error', function(e) {
        cb(data);
    });

    req.write(JSON.stringify(data));
    req.end();
}

exports.ActiveUserNotification = (data, cb) => {
    var additionalData = {};
    if (typeof data.data === 'string') {
        additionalData = JSON.parse(data.data);
    } else if (typeof data.data === 'object') {
        additionalData = data.data;
    }
    var NotificationObj = {
        contents: {
            "en": data.message
        },
        headings: {
            "en": data.title
        },
        data: additionalData,
        // included_segments : ["Active Users"],
        include_player_ids: data.playersIds == undefined ? [] : data.playersIds, // It should be array
        small_icon: data.small_icon, // it should be url
        large_icon: data.large_icon, // it should be url
        android_led_color: "488aff",
        android_accent_color: "488aff",
        buttons: data.buttons === undefined ? [] : data.buttons, // [{"id": "id1" // unique, "text": "button1", "icon": "ic_menu_share"}]
        "app_id": data.onesignal_app_id ? data.onesignal_app_id : onesignal_app_id,
        // "android_channel_id": onesignal_channel_id,
        "web_url": data.web_url === undefined ? '' : data.web_url,
        "app_url": data.app_url === undefined ? '' : data.app_url,
        "web_buttons": data.web_buttons === undefined ? [] : data.web_buttons
    }

    var onesignalClient = null;

    if (data.onesignal_app_id) {
        onesignalClient = devClient;
    } else {
        NotificationObj["android_channel_id"] = onesignal_channel_id;
        onesignalClient = Client;
    }

    var notification = new OneSignal.Notification(NotificationObj);

    onesignalClient.sendNotification(notification, (err, httpResponse, data) => {
        if (err) {
            console.log('Something went wrong...');
            cb({ statusCode: 500 });
        } else {
            console.log(data, httpResponse.statusCode);
            cb({ statusCode: httpResponse.statusCode });
        }
    })
}


exports.notification = {
    app_id: config.onesignal.app_id, //554c1929-611b-453e-8376-e76c1eeb1d7b EW //"db7bd806-fd67-4495-915d-b76c19c9e9de", // Mobeserv
    // title: "",
    headings: {
        "en": ""
    },
    contents: {
        "en": "",
    },
    date: new Date(),
    included_segments: ["Active Users"], // example : 'ALL', 'Course'
    small_icon: "", // it should be url
    large_icon: "", // it should be url
    android_led_color: "FFFF0000",
    android_accent_color: "FFFF0000",
}

exports.sendNotificationAPI = function(req, res) {
    /* Sample Json Format to pass in notification
		{
		  "playersIds":"eK-XBYX2x6c:APA91bGfbCWX3SAkmahzOPzpJEW1FQIcbnWn6FhRkKHwtKMWpgXKsMH62FvKOx9NjiLzzDL1Ou-TWcYSCO9JPkrFPeJHC8w43IAute0kma_3FD0ItzWub56etfKBWDkScKLF9EQZfidg",
		  "title":"Welcome To BFL EDGE",
          "message":"Welcome To BFL EDGE",
          "small_icon": "",
          "large_icon": "",
		  "data": {}
		}
    */
    const body = req.body;
    const data = {
        title: body.title,
        message: body.message,
        small_icon: body.small_icon,
        large_icon: body.large_icon,
        data: body.data,
        playersIds: body.playersIds,
        buttons: body.buttons
    }
    exports.ActiveUserNotification(data, (result) => {
        res.json({
            type: true,
            data: 'Done'
        });
    });
}

exports.listenNotificationWebHooks = function(req, res) {
    var params = req.body;
    // let json = parser.toJson(res);
    // json = json.replace(/\\"/g, '"');
    // let resp = JSON.parse(json);
    console.log('Notification Webhook listen request..', res);
    console.log('Notification Webhook listen response..', req);
    res.send('OK');
    // try {
    //     res.status(200).json({
    //         status: true,
    //         msg: '',
    //         data: res.event,
    //     });
    // } catch (err) {
    //     res.status(500).json({ status: false, msg: err, data: [] });
    // }
};