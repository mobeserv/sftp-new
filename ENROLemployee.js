"use strict";

// var schedule = require('node-schedule');
// var Client = require('ssh2-sftp-client');
// var globalpath = __dirname + '/sftpfiles/';
// var csv = require('csvtojson');
// var TwinBcrypt = require('twin-bcrypt');
// var salt = TwinBcrypt.genSalt(10);
var notification = require("./notificationInsert");
var config = require("./config");
var EL = require("./manage");
var report = require("./report");
var pms = require("./pmsNew");

exports.enrolementcron = function (server, db) {
  try {
    // var db = req.db;
    var sql = "call SP_GET_Tenant_List()";
    db.query(sql, [], function (err, list) {
      if (err) {
        processlog(
          db,
          "Error in Get tenant list ",
          "Get tenant list",
          "Error" + err.message,
          "Auto",
          0
        );
      } else {
        processlog(
          db,
          "Enrol employee start ",
          "Get tenant list",
          "sucessfull",
          "Auto",
          0
        );
        if (list[0].length > 0) {
          processlog(
            db,
            "Enrol employee start after if condn data[0]length > 0",
            "Get tenant list 1",
            "sucessfull 1",
            "Auto",
            0
          );
          enrolment_fro_single_tenant(
            db,
            list[0],
            0,
            [],
            [],
            [],
            "Auto",
            function (sucessfull, unsuccessfull, resions) {
              //new log start
            var str = JSON.stringify(list[0]);
            processlog(
              db,
              "Entered function for donedata",
              "Entered",
              str,
              "Auto",
              0
            );
            //new log end
              //console.log("Done all calls!",userList.length);
              //console.log("Done all calls!",unsuccessfuluser);
              //console.log("Done all calls!",resions);
              var donedata = {
                Total_tenant_found: list[0].length,
                sucessfull_enrolmentfor_tenant: sucessfull.length,
                unsucessfull_enrolmentfor_tenant: unsuccessfull.length,
                resions_list: resions,
              };
              processlog(
                db,
                "Enrol employee",
                "done all calls ",
                JSON.stringify(donedata),
                "Auto",
                0
              );
              //send_response(req, res, undefined, donedata);
              /**
               * Commented By Bhavesh Tandel
               * Added a cron in the SFTPEmployee.js file and run every day at 2 AM 
               */
              // notification.notificationcron(server, db);
              report.insertCronandSFTPLinks(db,2); // 2 for cronReport
              pms.pmsSftp(db, 'auto');
            }
          );
        } else {
          processlog(
            db,
            "Tenant list result",
            "NO data Found",
            "sucessfull",
            "Auto",
            0
          );
        }
      }
    });
  } catch (e) {
    processlog(
      db,
      "Error in Get tenant list in try block",
      "Get tenant list",
      "Error" + e.message,
      "Auto",
      0
    );
  }
};

exports.enrolUser_manual = function (req, res) {
  try {
    var db = req.db;
    var sql = "call SP_GET_Tenant_List()";
    db.query(sql, [], function (err, list) {
      if (err) {
        processlog(
          db,
          "Error in Get tenant list ",
          "Get tenant list",
          "Error" + err.message,
          "manual",
          0
        );
      } else {
        processlog(
          db,
          "Enrol employee start ",
          "Get tenant list",
          "sucessfull",
          "manual",
          0
        );
        if (list[0].length > 0) {
          enrolment_fro_single_tenant(
            db,
            list[0],
            0,
            [],
            [],
            [],
            "Manual",
            function (sucessfull, unsuccessfull, resions) {
              //console.log("Done all calls!",userList.length);
              //console.log("Done all calls!",unsuccessfuluser);
              //console.log("Done all calls!",resions);
              var donedata = {
                Total_tenant_found: list[0].length,
                sucessfull_enrolmentfor_tenant: sucessfull.length,
                unsucessfull_enrolmentfor_tenant: unsuccessfull.length,
                resions_list: resions,
              };
              processlog(
                db,
                "Enrol employee",
                "done all calls ",
                JSON.stringify(donedata),
                "Manual",
                0
              );
              send_response(req, res, undefined, donedata);
            }
          );
        } else {
          processlog(
            db,
            "Tenant list result",
            "NO data Found",
            "sucessfull",
            "manual",
            0
          );
        }
      }
    });
  } catch (e) {
    processlog(
      db,
      "Error in Get tenant list in try block",
      "Get tenant list",
      "Error" + e.message,
      "manual",
      0
    );
  }
};

function enrolment_fro_single_tenant(
  newdb,
  tenatlist,
  i,
  sucessfull,
  unsuccessfull,
  resions,
  type,
  callback
) {
  processlog(
    newdb,
    "enrolment for single tenant 2 ",
    "enrolment for single tenant 2",
    "sucessfull"+"|"+JSON.stringify(sucessfull)+"|"+JSON.stringify(unsuccessfull)+"|"+JSON.stringify(resions)+"|"+JSON.stringify(tenatlist),
    "Auto",
    0
  );
  if (i == tenatlist.length || tenatlist.length === 0)
    return callback(sucessfull, unsuccessfull, resions);
  var db = newdb;
  processlog(
    db,
    "Enrol employee start for tenant" + tenatlist[i].tenantName,
    "function call",
    "sucessfull",
    type,
    tenatlist[i].tenantId
  );
  try {
    //console.log(tenatlist[i].tenantId, "--", tenatlist[i].tenantName);
    var db = newdb;
    var sql = "call SP_ADMIN_COURSE_ENROL(?,?,?);";
    db.query(
      sql,
      [tenatlist[i].tenantId, config.serverType, null],
      function (err, list) {
        if (err) {
          processlog(
            db,
            "Error at Enrol employee start for tenant" +
              tenatlist[i].tenantName +
              " in try block",
            "Enrol employee",
            "Error" + err.message,
            type,
            tenatlist[i].tenantId
          );
          unsuccessfull.push(tenatlist[i]);
          resions.push({ tenantId: tenatlist[i].tenantId, err: err });
        } else {
          sucessfull.push(tenatlist[i]);
          resions.push({ tenantId: tenatlist[i].tenantId, err: err });
        }
        enrolment_fro_single_tenant(
          newdb,
          tenatlist,
          i + 1,
          sucessfull,
          unsuccessfull,
          resions,
          type,
          callback
        );
      }
    );
  } catch (e) {
    processlog(
      db,
      "Error at Enrol employee start for tenant" +
        tenatlist[i].tenantName +
        " in try block",
      "Enrol employee",
      "Error" + e.message,
      type,
      tenatlist[i].tenantId
    );
  }
}
//console.log(GetFormattedDate());

function GetFormattedDate(date) {
  if (date) {
    var todayTime = new Date(date);
    var month = todayTime.getMonth() + 1;
    var day = todayTime.getDate();
    var year = todayTime.getFullYear();
  } else {
    var todayTime = new Date();
    var month = todayTime.getMonth() + 1;
    var day = todayTime.getDate() - 1;
    var year = todayTime.getFullYear();
  }

  if (day < 10) {
    day = "0" + day;
  }
  if (month < 10) {
    month = "0" + month;
  }
  return year + "" + month + "" + day;
}

function processlog(db, type, activity, desc, trig, tId) {
  var resourceSQL = "call SP_Automated_Process_log(?,?,?,?,?)";
  db.query(resourceSQL, [type, activity, desc, trig, tId], function (err, res) {
    if (err) {
      // console.log('err ', err);
    } else {
      //  console.log('res ', res);
    }
  });
}

function send_response(req, res, err, data) {
  if (err) {
    res.json({
      type: false,
      data: "Error occured: " + err,
    });
  } else {
    res.json({
      type: true,
      Data: data,
    });
  }
}

exports.manual_enrol_test = function (req, res) {
  //   var resdata = req.query.arg1;
  var db = req.db;
  var query = require("url").parse(req.url, true).query;
  //   console.log("resdata", resdata);
  console.log("query", query);
  /*tenatlist[i].tenantId,config.serverType
    SP_ADMIN_COURSE_ENROL_temp_testing(tenatlist[i].tenantId,config.serverType)*/
  try {
    if (!query.cId && !query.wId) {
      res.json({
        type: false,
        Data: "Please fill the all field",
      });
    } else {
      res.json({
        type: true,
        Data: "Rule Base Enrolment Process Start.",
      });
      var sql = "call SP_ADMIN_COURSE_ENROL_temp_testing(?,?,?,?)";
      db.query(
        sql,
        [query.tId, config.serverType, query.cId, query.wId],
        function (err, list) {
          if (err) {
            EL.inserterrorlog(
              "manual_enrol_test",
              error,
              "manual_enrol_test",
              null
            );
          }
        }
      );
    }
  } catch (e) {
    EL.inserterrorlog("manual_enrol_test", e, "manual_enrol_test", null);
  }
};
