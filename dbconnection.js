const QP = require('./QP');
var pl = require('./processLog');

exports.keepConnectionAlive = (db) =>{
    const qP = new QP(db);
    var SQL = 'call SP_SFTP_Connection_KeepAlive();';
        qP.queryPromise(SQL, [])
            .then(data => {
                // const date = new Date();
                console.log("Connection Keep Alive Cron Running at " + data[0][0]['date']);
                //pl.processlog(db, "SFTP keepConnectionAlive", `Connection is Active`, "successfully", 'AUTO', 0);
            })
            .catch(error => {
                //console.log("Error in Connection Keep Alive Cron " + error.message);
                pl.processlog(db, "SFTP keepConnectionAlive", "error", error.message, 'AUTO', 0);
            });
}
