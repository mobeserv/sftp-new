'use strict';

var schedule = require('node-schedule');
var Client = require('ssh2-sftp-client');
var config = require('./config');
var globalpath = __dirname + '/sftpfiles/';
var dummyfiles = config.sftpconnection.sftptestingfiles;
var csv = require('csvtojson');
var TwinBcrypt = require('twin-bcrypt');
var salt = TwinBcrypt.genSalt(10);
var enrol = require('./ENROLemployee');
const mail = require('./mail');
const rolechange = require('./SFTPemprolechange');
var notification = require("./notificationInsert");
const ats = require("./ats")
var attempt = 3;
var totalattempt = 0;
//var slmfilepath = global.__basedir + '/uploaddump/DEV_tenent1_10users.csv';
var ErrorLog = require("./manage");
var logEnum =ErrorLog.enum;
var report = require("./report");
var dbc = require("./dbconnection");
//let genfname ='SFTPFile121331.csv';
//var slmfilepath = globalpath + genfname ;
var startTimeStamp ;
var isException=0;
var endTimeStamp;
// var tlist_array=[
//     {
//         tId:1,
//         fileillias:'Illume_OPM_',
//         range:[0,19],
//     },
//     {
//         tId:2,
//         fileillias:'BHFL_Illume_OPM_',
//         range:[0,24],
//     }
// ]
exports.addusercron = function (server, db) {
    //console.log('function excute');

    //if (config.sftpconnection.sftpcron) {

        //rolechange.getTenantListRolechangeSftp(server, db);
        var rule = new schedule.RecurrenceRule();
        rule.second = 0;
        rule.minute = 15;
        rule.hour = 0;
        console.log('User Add Cron Init');
        var j = schedule.scheduleJob(rule, function () {
            console.log('User Cron Started');
            totalattempt = 0;
            exports.getTenantListSftp(server, db);
        });

        var rule2 = new schedule.RecurrenceRule();    
        rule2.second = 0;
        rule2.minute = 5;
        rule2.hour = 0;
        console.log('User Role Change Cron Init');
        var k = schedule.scheduleJob(rule2, function () {
            console.log('User Role Change Cron Started');
            rolechange.getTenantListRolechangeSftp(server, db);
        });

        var rule1 = new schedule.RecurrenceRule();
        rule1.second = 0;
        rule1.minute = 0;
        rule1.hour = 2;
        console.log('Notification Cron Init');
        var x = schedule.scheduleJob(rule1, function () {
            console.log('Notification Cron Started');
            notification.notificationcron(server, db);
        });

        var uploadReportCron = schedule.scheduleJob('30 17 * * *', function () {
            ats.uploadReportOnServer(db);
        });

        var keepConnectionAliveCron = schedule.scheduleJob('0 */2 * * *', function () { // Every 2 hr
            dbc.keepConnectionAlive(db);
        });
    }
    // exports.sftpUserAdd(server, db); 
//}
exports.sftpUserAdd_manual = function (req, res) {
    var resdata = req.query;
    var db = req.db;

    let sftp = new Client();
    try {
        sftp.connect(config.sftpconnection).then(() => {
            processlog(db, 'SFTP USER CREATION', "SFTP CONNECTION", 'successfull', 'Manual', 0);
            console.log('connection successfull');
            return sftp.list('/');
        }).then((data) => {
            try {
                // var db = req.db;
                var sql = 'call SP_GET_SFTP_Tenant_List()';
                db.query(sql, [], function (err, list) {
                    if (err) {
                        processlog(db, 'SFTP USER CREATION', "Get tenant list", 'Error' + err.message, 'Manual', 0);
                    } else {
                        processlog(db, 'SFTP USER CREATION', "Get tenant list", 'sucessfull', 'Manual', 0);
                        if (list[0].length > 0) {
                            exports.sftpmultiple(db, sftp, list[0], data, 0, resdata.date, 'Manual', function (resp) {
                                enrol.enrolementcron(server, db);
                                console.log('Done ALL SFTP successfull');
                            });
                        } else {
                            processlog(db, 'SFTP USER CREATION', "NO data Found", 'sucessfull', 'Manual', 0);
                        }
                    }
                })
            } catch (e) {
                processlog(db, 'SFTP USER CREATION', 'Error in Get tenant list in try block', 'Error' + e.message, 'Manual', 0);

            }

            // let todaydate = GetFormattedDate(resdata.date);
            // let genfname = "Illume_OPM_" + todaydate;
            // processlog(db, 'SFTP USER CREATION', "list read suessfull", 'successfull', 'Manual',0);
            // var found = data.find(function(element) {
            //     let filename = element.name;
            //     let tempfname = filename.substring(0, 19);
            //     // Illume_OPM_20190603231502.csv
            //     // console.log('condition', tempfname == genfname , ' tempname' ,tempfname ,' genfname' ,genfname);
            //     if (tempfname == genfname) {
            //         return element;
            //     }

            // });
            // //console.log(found.name);
            // if (found != undefined) {
            //     processlog(db, 'SFTP USER CREATION', "file found", 'file found for ' + todaydate, 'Manual');
            //     sftp.fastGet(found.name, globalpath + found.name, []).then((data) => {
            //         //console.log(data, 'the data info');
            //         processlog(db, 'SFTP USER CREATION', "download file", 'successfull', 'Manual');

            //         // readfileanddata(globalpath+found.name,db);

            //         csv()
            //             .fromFile(globalpath + found.name)
            //             .then((jsonObj) => {
            //                 processlog(db, 'SFTP USER CREATION', "read file data", 'successfull', 'Manual');
            //                 SaveMultiUser(db, jsonObj, 1, 1, 0, [], [], [], [], function(userList, unsuccessfuluser, updateuserList, resions) {

            //                     var donedata = {
            //                         Total_req_found: jsonObj.length,
            //                         successfull_added_user: userList.length,
            //                         unsuccessfull_user: unsuccessfuluser.length,
            //                         resions_list: resions
            //                     }
            //                     processlog(db, 'SFTP USER CREATION', "done all calls ", JSON.stringify(donedata), 'Manual');
            //                     send_response(req, res, undefined, donedata);
            //                 });
            //             });

            //     }, (err) => {
            //         console.log(err, 'catch error');
            //         processlog(db, 'SFTP USER CREATION', "download file", err, 'Manual');
            //         send_response(req, res, err, donedata);
            //     });
            // } else {
            //     console.log('file not found');
            //     processlog(db, 'SFTP USER CREATION', "file found", 'file not found for ' + todaydate, 'Manual');
            // }

        }).catch((err) => {
            processlog(db, 'SFTP USER CREATION', "list not read", err.message, 'Manual', 0);
            //console.log(err, 'catch error');
            send_response(req, res, err, []);

        });
    } catch (e) {
        //console.log(e);
        processlog(db, 'SFTP USER CREATION', "SFTP CONNECTION", e.message, 'Manual', 0);
        send_response(req, res, err, []);
    }

}

exports.sftpUserAdd = function (server, db) {

    processlog(db, 'SFTP USER CREATION', "attempt", totalattempt + 1, 'AUTO', 0);
    startTimeStamp = new Date();
    var isException=0;
    let sftp = new Client();
    try {
        processlog(db, 'SFTP USER CREATION', "initiated", "Procces initiated", 'AUTO', 0);
        sftp.connect(config.sftpconnection).then(() => {
            processlog(db, 'SFTP USER CREATION', "SFTP Connection done", 'successfully', 'AUTO', 0);
            console.log('connection successfull');
            return sftp.list(config.sftpconnection.fileLocation);
        }).then((data) => {
            //console.log(data, 'the data info');
            // for(let i =0 ;i<tlist_array.length;i++)
            // {   

            // }

            try {
                // var db = req.db;
                var sql = 'call SP_GET_SFTP_Tenant_List()';
                db.query(sql, [], function (err, list) {
                    if (err) {
                      //  processlog(db, 'SFTP USER CREATION', "Get tenant list", 'Error' + err.message, 'AUTO', 0);
                        isException=1;
                        endTimeStamp=new Date();
                        ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
                        logEnum.process.sftp, logEnum.process.sftp,startTimeStamp,endTimeStamp,
                        isException,logEnum.exceptionType[500].value,logEnum.exceptionType[500].name,null,null);
                    } else {
                        processlog(db, 'SFTP USER CREATION', "Get tenant list", 'sucessfull', 'AUTO', 0);
                        if (list[0].length > 0) {

                            exports.sftpmultiple(db, sftp, list[0], data, 0, undefined, 'AUTO', 'SFTP', function (resp) {
                                exports.processsftpfile(db, sftp, resp, data, 0, undefined, 'AUTO', 'SFTP', function (resp) {
                                    processlog(db, 'SFTP USER CREATION', "completed", 'sucessfully', 'AUTO', 0);
                                    //
                                    endTimeStamp=new Date();
                                    ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
                                    logEnum.process.sftp, logEnum.process.sftp,startTimeStamp,endTimeStamp,
                                    isException,logEnum.exceptionType[200].value,logEnum.exceptionType[200].name,null,null);
                                    //enrol.enrolementcron(server,db);
                                    console.log('Done ALL SFTP successfull');
                                    ats.sendEmailsftpats(db, 1,'SFTP');
                                    report.insertCronandSFTPLinks(db,1); //1 sftpstatus
                                    ats.atsUserAdd(server, db);
                                    //setTimeout(function () { sentprcesslog(db); }, 3000);
                                });
                            });
                        } else {
                            processlog(db, 'SFTP USER CREATION', "NO data Found", 'sucessfull', 'AUTO', 0);
                            //404
                            endTimeStamp=new Date();
                            ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
                            logEnum.process.sftp, logEnum.process.sftp,startTimeStamp,endTimeStamp,
                            isException,logEnum.exceptionType[404].value,logEnum.exceptionType[404].name,null,null);
                            ats.atsUserAdd(server, db);
                        }
                    }
                })
            } catch (e) {
                processlog(db, 'SFTP USER CREATION', 'Error in Get tenant list in try block', 'Error' + e.message, 'AUTO', 0);
                isException=1;
                endTimeStamp=new Date();
                ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
                logEnum.process.sftp, logEnum.process.sftp,startTimeStamp,endTimeStamp,
                isException,logEnum.exceptionType[500].value,logEnum.exceptionType[500].name,null,null);
                ats.atsUserAdd(server, db);
            }
            sftp.on('close', function (result) {
                processlog(db, 'SFTP USER CREATION', "SFTP Connection Closed", "Close event", 'AUTO', 0);
            });
            sftp.on('end', function (result) {
                processlog(db, 'SFTP USER CREATION', "SFTP Connection ended", "End event", 'AUTO', 0);
            });
            sftp.on('finish', function (result) {
                processlog(db, 'SFTP USER CREATION', "SFTP Connection Finished", "Finish event", 'AUTO', 0);
            });
            sftp.on('error', function (err) {
                processlog(db, 'SFTP USER CREATION', "SFTP Connection Error", "Error event", 'AUTO', 0);
                isException=1;
                endTimeStamp=new Date();
                ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
                logEnum.process.sftp, logEnum.process.sftp,startTimeStamp,endTimeStamp,
                isException,logEnum.exceptionType[500].value,logEnum.exceptionType[500].name,null,null);
            });
        }).catch((err) => {
            
            //console.log(err);
            if (err) {
                processlog(db, 'SFTP USER CREATION', "List not read", err.message, 'AUTO', 0);
            } else {
                processlog(db, 'SFTP USER CREATION', "List not read", 'Connection Timeout', 'AUTO', 0);
            }         
            if (totalattempt < attempt) {
                totalattempt++;
                exports.sftpUserAdd(server, db);
            }
            else{
            isException=1;
            endTimeStamp=new Date();
            ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
            logEnum.process.sftp, logEnum.process.sftp,startTimeStamp,endTimeStamp,
            isException,logEnum.exceptionType[500].value,logEnum.exceptionType[500].name,null,null);
            ats.atsUserAdd(server, db);
            }
            //console.log(err, 'catch error');

        });
    } catch (e) {
        //console.log(e);
        processlog(db, 'SFTP USER CREATION', "SFTP Server not connected", e.message, 'AUTO', 0);
        if (totalattempt < attempt) {
            totalattempt++;
            exports.sftpUserAdd(server, db);
        }   
        else
        {
            isException=1;
            endTimeStamp=new Date();
            ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
            logEnum.process.sftp, logEnum.process.sftp,startTimeStamp,endTimeStamp,
            isException,logEnum.exceptionType[500].value,logEnum.exceptionType[500].name,null,null);
            ats.atsUserAdd(server, db);
        }   
    }

}

exports.processsftpfile = function (db, sftp, tlist_array, filelistdata, u, date, type, servertype, callback) {
    if (u == tlist_array.length || tlist_array.length === 0 || !tlist_array[u].filepath)
        return callback('Done');
    //console.log(tlist_array[u]);
    readfileanddata(tlist_array[u].filepath, db, tlist_array, filelistdata, u, sftp, date, type, servertype, callback);
     // readfileanddata(slmfilepath, db, tlist_array, filelistdata, u, sftp, date, type,callback);
}


exports.sftpmultiple = function (db, sftp, tlist_array, filelistdata, u, date, type, servertype, callback) {
    if (u == tlist_array.length || tlist_array.length === 0)
        return callback(tlist_array);
    //console.log(tlist_array[u]);
    let todaydate = exports.GetFormattedDate(date);
    let genfname = tlist_array[u].fileName + todaydate;
    //console.log(genfname);
    processlog(db, `${servertype} USER CREATION`, "List read successfully", 'successfull', type, tlist_array[u].tenantId);
    var found = filelistdata.find(function (element) {
        let filename = element.name;
        let tempfname = filename.substring(tlist_array[u].fsubStringstart, tlist_array[u].fsubStringEnd);
        // Illume_OPM_20190603231502.csv
        // console.log('condition', tempfname == genfname , ' tempname' ,tempfname ,' genfname' ,genfname);
        //console.log('tempfname', tempfname,' genfname',genfname)
        if (tempfname == genfname) {
            return element;
        }

    });
    //console.log(found.name);
    if (found != undefined) {
        processlog(db, `${servertype} USER CREATION`, "File found", 'File found for ' + todaydate, type, tlist_array[u].tenantId);
        sftp.fastGet(servertype == 'SFTP' ? config.sftpconnection.fileLocation+found.name : config.ats.fileLocation+found.name, globalpath + found.name, []).then((data) => {
            // console.log(data, 'the data info');
            // console.log(found.name);
            //processlog(db, `${servertype} USER CREATION`, "File downloaded successfully", 'successfull for ' + todaydate, type, tlist_array[u].tenantId);
            processlog(db, `${servertype} USER CREATION`, "File downloaded successfully", found.name, type, tlist_array[u].tenantId); //Changed above line here to store filename received from server
            // readfileanddata(globalpath + found.name, db, tlist_array, filelistdata, u, sftp, date, type,callback);
            var filename = found.name;
            if(config.sftpconnection.sftptesting && dummyfiles[tlist_array[u].tenantId] && servertype == 'SFTP'){
                filename = dummyfiles[tlist_array[u].tenantId]
            }
            tlist_array[u].filepath = globalpath + filename; //found.name;
            exports.sftpmultiple(db, sftp, tlist_array, filelistdata, u + 1, date, type, servertype, callback);
        }, (err) => {
            //console.log(err, 'catch error');
            tlist_array[u].filepath = '';
            processlog(db, `${servertype} USER CREATION`, "File download err", err.message, type, tlist_array[u].tenantId);
            exports.sftpmultiple(db, sftp, tlist_array, filelistdata, u + 1, date, type, servertype, callback);
        });
    } else {
        console.log('file not found');
        processlog(db, `${servertype} USER CREATION`, "File found", 'File not found for ' + todaydate, type, tlist_array[u].tenantId);
        exports.sftpmultiple(db, sftp, tlist_array, filelistdata, u + 1, date, type, servertype, callback);
    }

}

function readfileanddata(path, db, tlist_array, filelistdata, u, sftp, date, type, servertype, callback) {
    console.log(path);
    
    try {
        csv()
            .fromFile(path)
            .then(async (jsonObj) => {
                processlog(db, `${servertype} USER CREATION`, "read file data", 'successfull', 'AUTO', tlist_array[u].tenantId);
                let data = await exports.findlovvalues(db, jsonObj, tlist_array[u].tenantId, type, servertype);
                console.log(data);
                SaveMultiUser(db, jsonObj, 1, tlist_array[u].tenantId, 0, [], [], [], [], servertype, function (userList, unsuccessfuluser, updateuserList, resions) {

                    var donedata = {
                        Total_req_found: jsonObj.length,
                        Total_Created_Users: userList.length,
                        Total_Updated_Users: updateuserList.length,
                        Not_created_Users_New: unsuccessfuluser.length,
                        reason_list: resions
                    }
                    //processlog(db, 'SFTP USER CREATION', "done all calls ", 'successfull', 'AUTO');
                    processlog(db, `${servertype} USER CREATION`, "Done all calls ", JSON.stringify(donedata), 'AUTO', tlist_array[u].tenantId);
                    // send_response(req, res, undefined, donedata);
                    exports.processsftpfile(db, sftp, tlist_array, filelistdata, u + 1, date, type, servertype, callback);
                });
            }).catch((error)=>{
                console.log(error);
                endTimeStamp=new Date();
                isException=1;
                ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
                logEnum.process.sftp, logEnum.process.sftp,startTimeStamp,endTimeStamp,
                isException,logEnum.exceptionType[500].value,logEnum.exceptionType[500].name,null);
                exports.processsftpfile(db, sftp, tlist_array, filelistdata, u + 1, date, type, servertype, callback);
            });
    } catch (e) {
        //console.log(e);

        processlog(db, `${servertype} USER CREATION`, "read file data", e.message, 'AUTO', tlist_array[u].tenantId);
        isException=1;
        endTimeStamp=new Date();
        ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
        logEnum.process.sftp, logEnum.process.sftp,startTimeStamp,endTimeStamp,
        isException,logEnum.exceptionType[500].value,logEnum.exceptionType[500].name,null);
        exports.processsftpfile(db, sftp, tlist_array, filelistdata, u + 1, date, type, servertype, callback);
       
    }
}

function SaveMultiUser(newdb, ExcelData, userId, userTId, i, userList, unsuccessfuluser, updateuserList, resions, servertype, callback) {
    // console.log('ExcelData length ' + ExcelData.length);
    // console.log('s-',i);
    if (i == ExcelData.length || ExcelData.length === 0)
        return callback(userList, unsuccessfuluser, updateuserList, resions);

    if (ExcelData[i].password) {
        TwinBcrypt.hash(ExcelData[i].password, salt,
            function (p) { },
            function (hash) {
                ExcelData[i]['encryptedPwd'] = hash;
                if (ExcelData[i].encryptedPwd) {
                    ExcelData[i]['tenantId'] = userTId;
                    uploadMultipleEmployeesData(ExcelData, userId, userTId, newdb, i, userList, unsuccessfuluser, updateuserList, resions, servertype, callback);
                }
            });
    } else {
        var randomPassword = generatePassword();
        ExcelData[i].password = randomPassword;
        TwinBcrypt.hash(randomPassword, salt,
            function (p) { },
            function (hash) {
                ExcelData[i]['encryptedPwd'] = hash;
                if (ExcelData[i].encryptedPwd) {
                    ExcelData[i]['tenantId'] = userTId;
                    uploadMultipleEmployeesData(ExcelData, userId, userTId, newdb, i, userList, unsuccessfuluser, updateuserList, resions, servertype, callback);
                }
            });
    }
}

function uploadMultipleEmployeesData(ExcelData, userId, userTId, newdb, i, userList, unsuccessfuluser, updateuserList, resions, servertype, callback) {
    //console.log('f-',i);
    //console.log('uploadMultipleEmployeesData');
    var resourceSQL = "call SP_ADD_VALID_employee_usingsftp_without_LOV(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,@SQLResult, @OStatus, @isUserCreated)";
    newdb.query(resourceSQL, [
        1,
        ExcelData[i].employee_code,
        ExcelData[i].encryptedPwd,
        ExcelData[i].first_name,
        ExcelData[i].last_name,
        ExcelData[i].email,
        ExcelData[i].mobileno,
        ExcelData[i].city || '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        ExcelData[i].band,
        '',
        '',
        '',
        ExcelData[i].business_group,
        '',
        '',
        '',
        '',
        ExcelData[i].department,
        changeDate(ExcelData[i].dob),
        changeDate(ExcelData[i].doj),
        changeDate(ExcelData[i].dol),
        '',
        changeDate(ExcelData[i].dor),
        '',
        '',
        '',
        '',
        ExcelData[i].employee_category,
        ExcelData[i].employee_classification,
        '',
        '',
        '',
        ExcelData[i].middle_name || '',
        '',
        ExcelData[i].gender || '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        ExcelData[i].reportingmanager_code,
        ExcelData[i].reportingmanager_name || '',
        ExcelData[i].role_designation,
        ExcelData[i].sub_department,
        ExcelData[i].Skip_Level_Manager_Code || '',
        ExcelData[i].Skip_Level_Manager_Name || '',
        '',
        ExcelData[i].sub_sub_department,
        ExcelData[i].State,
        '',
        '',
        ExcelData[i].zone_region,
        '',
        '',
        'y',
        '',
        ExcelData[i].area || '',
        ExcelData[i].country || '',
        ExcelData[i].Recruiter_ECN || '',
        ExcelData[i].Recruiter_Name || '',
        userTId
    ], function (err, res) {
        if (err) {
            unsuccessfuluser.push(ExcelData[i]);
            resions.push({ emp_code: ExcelData[i].employee_code, err: err });
            user_processlog(newdb, 'SFTP USER CREATION', "user create or update", err.message, 'AUTO', userTId, ExcelData[i].employee_code, 3, servertype);
        } else {
            if (res[0].length > 0) {
                ExcelData[i].result = res[0][0].SQLResult;
                ExcelData[i].flag = res[0][0].OStatus;
                ExcelData[i].isUserCreated = res[0][0].isUserCreated;
                if (ExcelData[i].isUserCreated) {
                    userList.push(ExcelData[i]);
                    console.log(i, ExcelData[i].employee_code, res[0][0].SQLResult);
                    user_processlog(newdb, 'SFTP USER CREATION', "user create or update", res[0][0].SQLResult, 'AUTO', userTId, ExcelData[i].employee_code, 1, servertype);
                } else {

                    //resions.push(res[0][0].SQLResult);
                    if (ExcelData[i].flag) {
                        updateuserList.push(ExcelData[i]);
                        console.log(i, ExcelData[i].employee_code, res[0][0].SQLResult)
                        user_processlog(newdb, 'SFTP USER CREATION', "user create or update", res[0][0].SQLResult, 'AUTO', userTId, ExcelData[i].employee_code, 2, servertype);
                    } else {
                        console.log(i, ExcelData[i].employee_code, res[0][0].SQLResult)
                        unsuccessfuluser.push(ExcelData[i]);
                        resions.push({ emp_code: ExcelData[i].employee_code, err: res[0][0].SQLResult });
                        user_processlog(newdb, 'SFTP USER CREATION', "user create or update", res[0][0].SQLResult, 'AUTO', userTId, ExcelData[i].employee_code, 3, servertype);
                    }

                }
            }
        }
        SaveMultiUser(newdb, ExcelData, userId, userTId, i + 1, userList, unsuccessfuluser, updateuserList, resions, servertype, callback);
    });
}

// faild to register with bfl user incative in below function

 exports.markusers_as_inactive = function(db, server, type) {
    processlog(db, 'USER STATUS UPDATE AS INACTIVE', "Process start", 'sucessfull', type, 0);
    try {
        var sql = 'call SP_GET_SFTP_Tenant_List_user_inactive();';
        db.query(sql, [], function (err, list) {
            if (err) {
                processlog(db, 'USER STATUS UPDATE AS INACTIVE', "Get tenant list", 'Error' + err.message, type, 0);
                enrol.enrolementcron(server, db);
            } else {
                processlog(db, 'USER STATUS UPDATE AS INACTIVE', "Get tenant list", 'sucessfull', type, 0);
                if (list[0].length > 0) {
                    tenentviseuserupdate(db, list[0], 0, type, function (resp) {
                        enrol.enrolementcron(server, db);
                        console.log('Done ALL successfull');
                        processlog(db, 'USER STATUS UPDATE AS INACTIVE', "Process Done", 'sucessfull', type, 0);
                    });
                } else {
                    processlog(db, 'USER STATUS UPDATE AS INACTIVE', "NO data Found", 'sucessfull', type, 0);
                    enrol.enrolementcron(server, db);
                }
            }
        })
    } catch (e) {
        processlog(db, 'USER STATUS UPDATE AS INACTIVE', 'Error in Get tenant list in try block', 'Error' + e.message, type, 0);
        enrol.enrolementcron(server, db);
    }
}

function tenentviseuserupdate(db, tlist_array, u, type, callback) {
    if (u == tlist_array.length || tlist_array.length === 0)
        return callback('Done');
    try {
        processlog(db, 'USER STATUS UPDATE AS INACTIVE', "Inactive User For Tenent" + tlist_array[u].tenantId, 'sucessfull', type, tlist_array[u].tenantId);
        var sql = 'call SP_Inactive_User_status_for_stage_day1(?)';
        db.query(sql, [tlist_array[u].tenantId], function (err, list) {
            if (err) {
                processlog(db, 'USER STATUS UPDATE AS INACTIVE', "Inactive User For Tenent" + tlist_array[u].tenantId, 'Error' + err.message, type, tlist_array[u].tenantId);
                tenentviseuserupdate(db, tlist_array, u + 1, type, callback);
            } else {
                processlog(db, 'USER STATUS UPDATE AS INACTIVE', "Inactive User For Tenent" + tlist_array[u].tenantId, 'sucessfull', type, tlist_array[u].tenantId);
                tenentviseuserupdate(db, tlist_array, u + 1, type, callback);
            }
        })
    } catch (e) {
        processlog(db, 'USER STATUS UPDATE AS INACTIVE', 'Error in INACTIVE user tenent vise in try block', 'Error' + e.message, type, tlist_array[u].tenantId);
        tenentviseuserupdate(db, tlist_array, u + 1, type, callback);
    }
}


function generatePassword() {
    // var length = 8,
    //     charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    let retVal = "Edgelms@123";
    // for (var i = 0, n = charset.length; i < length; ++i) {
    //     retVal += charset.charAt(Math.floor(Math.random() * n));
    // }
    return retVal;
}
 
exports.GetFormattedDate = function (date) {
    if (date) {
        var todayTime = new Date(date);
        var month = todayTime.getMonth() + 1;
        var day = todayTime.getDate();
        var year = todayTime.getFullYear();
    } else {
        var todayTime = new Date();
        todayTime.setDate(todayTime.getDate() - 1);
        var month = todayTime.getMonth() + 1;
        var day = todayTime.getDate();
        var year = todayTime.getFullYear();
    }


    if (day < 10) {
        day = "0" + day
    }
    if (month < 10) {
        month = "0" + month
    }
    return year + "" + month + "" + day;
}

function processlog(db, type, activity, desc, trig, tId) {
    var resourceSQL = "call SP_Automated_Process_log(?,?,?,?,?)";
    db.query(resourceSQL, [type, activity, desc, trig, tId], function (err, res) {
        if (err) {
            // console.log('err ', err);
        } else {
            //  console.log('res ', res);

        }
    });
}

function user_processlog(db, type, activity, desc, trig, tId, ecn, flag, servertype) {
    var resourceSQL = "call SP_Automated_Process_log_sftpuser(?,?,?,?,?,?,?,?)";
    db.query(resourceSQL, [type, activity, desc, trig, tId, ecn, flag, servertype], function (err, res) {
        if (err) {
            // console.log('err ', err);
        } else {
            //  console.log('res ', res);

        }
    });
}

function send_response(req, res, err, data) {
    if (err) {
        res.json({
            type: false,
            data: "Error occured: " + err
        });
    } else {
        res.json({
            type: true,
            Data: data
        });
    }
}

function changeDate(value) {
    if (value == null || value == '') {
        return value
    } else {
        const splitarray = value.includes('/') ? value.split('/') : value.split('-');
        const date = splitarray.reverse().join('-');
        return date;
    }
};

// function sentprcesslog(db) {

//     var resourceSQL = "call SP_Send_process_log();";
//     db.query(resourceSQL, [], function (err, res) {
//         if (err) {
//             processlog(db, 'SFTP USER CREATION MAIL TO ADMIN', "getdata", err, 'AUTO', 0);
//         } else {
//             if (res[0].length > 0) {
//                 var to = [];
//                 var subject = 'SFTP user creation log';
//                 var messages = `Sftp user creation log as follows 
//                   ` + res[0][0].alldescription;
//                 var username = 'Admin';
//                 to.push('swapnali@mobeserv.com');
//                 to.push('support@mobeserv.com');
//                 to.push('bhavesh@mobeserv.com');
//                 to.push('ashwini.nikam@bajajfinserv.in');
//                 to.push('prashant.ingale@bajajfinserv.in');
//                 to.push('rohit.dolekar@bajajfinserv.in'); //newly added data
//                 to.push('yogita.shelar@bajajfinserv.in'); //newly added data
//                 to.push('adarsh.jain@bajajfinserv.in'); //newly added data changed from  'chanchal@mobeserv.in'
//                 to.push('sahil.gandhi@bajajfinserv.in');//newly added data changed from 'dhanashree@mobeserv.com' on 01-11-2022 as service request
//                 to.push('pradnya.pote@bizsupporta.com');//Added on 2022-01-05(Service request),also changed sahil.gandhi@bizsupporta.com to sahil.gandhi@bajajfinserv.in
//                 //to.push('sharad.shingate@bizsupporta.com');//newly added data (commented on 01-11-2022 as service request)
//                 to.push('mete.rajesh@bajajfinserv.in');//newly added data
//                 to.push('aditya@mobeserv.com');//newly added data
//                 to.push('team@mobeserv.com');//newly added data
//                 // if (config.sftp.email && Array.isArray(config.sftp.email) && config.sftp.email.length > 0) {
//                 //     to = config.sftp.email;
//                 // } else {
//                 //     // For production only
//                 //     to.push('swapnali@mobeserv.com');
//                 //     to.push('paresh@mobeserv.com');
//                 //     to.push('team@mobeserv.com');
//                 //     to.push('bhavesh@mobeserv.com');
//                 //     to.push('ashwini.nikam@bajajfinserv.in');
//                 //     to.push('prashant.ingale@bajajfinserv.in');
//                 //     to.push('rohit.dolekar@bajajfinserv.in'); //newly added data
//                 //     to.push('yogita.shelar@bajajfinserv.in'); //newly added data
//                 // }
//                 mail.sendMail(to, subject, messages, username, (result) => {
//                     processlog(db, 'SFTP USER CREATION MAIL TO ADMIN', JSON.stringify(to) + ": mail sent to admin :" + JSON.stringify(result), 'sucessfully', 'AUTO', 0);
//                 });
//             }
//         }
//     });
// };

//changeDate('28/10/2019');




// exports.sftpUserAdd_temp = function(server, db) {
//     // let todaydate = GetFormattedDate('2019-06-14');
//     let genfname = 'BFL_All_Users_270919.csv';//'Illume_OPM_20190614231502.csv'; //"Illume_OPM_" + todaydate+'.csv';
//     readfileanddata_temp(globalpath + genfname, db);
// };

// function readfileanddata_temp(path, db) {
//     console.log(path);
//     try {
//         csv()
//             .fromFile(path)
//             .then((jsonObj) => {
//                 processlog(db, 'SFTP USER CREATION', "read file data", 'successfull', 'AUTO');
//                 SaveMultiUser(db, jsonObj, 1, 1, 0, [], [], [], [], function(userList, unsuccessfuluser, resions) {

//                     var donedata = {
//                         Total_req_found: jsonObj.length,
//                         successfull_added_user: userList.length,
//                         unsuccessfull_user: unsuccessfuluser.length,
//                         resions_list: resions
//                     }
//                      processlog(db, 'SFTP USER CREATION', "done all calls ", JSON.stringify(donedata), 'AUTO');
//                     //send_response(req, res, undefined, donedata);
//                     console.log(donedata);
//                 });
//             });
//     } catch (e) {
//         console.log(e);
//         processlog(db, 'SFTP USER CREATION', "read file data", e, 'AUTO');
//     }
// }

//For Test
/*
exports.addusercrontest = function (server, db) {
    //console.log('function excute');

    //if (config.sftpconnection.sftpcron) {

        //rolechange.addrolechangecron(server, db);
        var rule = new schedule.RecurrenceRule();
        rule.second = 0;
        rule.minute = 3;
        rule.hour = 1;
        console.log('Cron Init');
        var j = schedule.scheduleJob(rule, function () {
            console.log('User Cron Started');
            totalattempt = 0;
            exports.sftpUserAddtest1(server, db);
        });
        var rule1 = new schedule.RecurrenceRule();
        rule1.second = 0;
        rule1.minute = 0;
        rule1.hour = 23;
        console.log('Cron Init');
        var x = schedule.scheduleJob(rule1, function () {
            console.log('Notification Cron Started');
            //notification.notificationcron(server, db);
        });
    }

exports.sftpUserAddtest = function (server, db) {
    processlog(db, 'SFTP USER CREATION', "attempt", totalattempt + 1, 'AUTO', 0);
    let sftp = new Client();
    try {
        processlog(db, 'SFTP USER CREATION', "initiated", "Procces initiated", 'AUTO', 0);
        /* --t sftp.connect(config.sftpconnection).then(() => {
            processlog(db, 'SFTP USER CREATION', "SFTP Connection done", 'successfully', 'AUTO', 0);
            console.log('connection successfull');
            return sftp.list('/');
        }).then((data) => { 
            //console.log(data, 'the data info');
            // for(let i =0 ;i<tlist_array.length;i++)
            // {   

            // }

            try {
                // var db = req.db;
                var sql = 'call SP_GET_SFTP_Tenant_List()';
                db.query(sql, [], function (err, list) {
                    if (err) {
                        processlog(db, 'SFTP USER CREATION', "Get tenant list", 'Error' + err.message, 'AUTO', 0);
                    } else {
                        processlog(db, 'SFTP USER CREATION', "Get tenant list", 'sucessfull', 'AUTO', 0);
                        if (list[0].length > 0) {

                           //t exports.sftpmultiple(db, sftp, list[0], data, 0, undefined, 'AUTO', function (resp) {
                            //t processsftpfiletest(db, sftp, data, 0, undefined, 'AUTO', function (resp) {
                                let genfname ='DevUserList.csv'; //"Illume_OPM_" + todaydate+'.csv';
                                readfileanddatatest(globalpath + genfname, db, 1);
                                    processlog(db, 'SFTP USER CREATION', "completed", 'sucessfully', 'AUTO', 0);
                                    //enrol.enrolementcron(server,db);
                                    //markusers_as_inactive(db, server, 'AUTO');
                                    console.log('Done ALL successfull');
                                    setTimeout(function () { sentprcesslog(db); }, 3000);
                               //t });
                           //t });
                        } else {
                            processlog(db, 'SFTP USER CREATION', "NO data Found", 'sucessfull', 'AUTO', 0);
                        }
                    }
                })
            } catch (e) {
                processlog(db, 'SFTP USER CREATION', 'Error in Get tenant list in try block', 'Error' + e.message, 'AUTO', 0);
            }
            /*sftp.on('close', function (result) {
                processlog(db, 'SFTP USER CREATION', "SFTP Connection Closed", "Close event", 'AUTO', 0);
            });
            sftp.on('end', function (result) {
                processlog(db, 'SFTP USER CREATION', "SFTP Connection ended", "End event", 'AUTO', 0);
            });
            sftp.on('finish', function (result) {
                processlog(db, 'SFTP USER CREATION', "SFTP Connection Finished", "Finish event", 'AUTO', 0);
            });
            sftp.on('error', function (err) {
                processlog(db, 'SFTP USER CREATION', "SFTP Connection Error", "Error event", 'AUTO', 0);
            });
        }).catch((err) => {
            //console.log(err);
            if (err) {
                processlog(db, 'SFTP USER CREATION', "List not read", err.message, 'AUTO', 0);
            } else {
                processlog(db, 'SFTP USER CREATION', "List not read", 'Connection Timeout', 'AUTO', 0);
            }
            if (totalattempt < attempt) {
                totalattempt++;
                exports.sftpUserAdd(server, db);
            }
            //console.log(err, 'catch error');

        });
    } catch (e) {
        //console.log(e);
        processlog(db, 'SFTP USER CREATION', "SFTP Server not connected", e.message, 'AUTO', 0);
        if (totalattempt < attempt) {
            totalattempt++;
            exports.sftpUserAdd(server, db);
        }
    }

}


function readfileanddatatest(path, db, tId) {
    console.log(path);
    try {
        csv()
            .fromFile(path)
            .then((jsonObj) => {
                processlog(db, 'SFTP USER CREATION', "read file data", 'successfull', 'AUTO', 1);
                SaveMultiUsertest(db, jsonObj, 1, tId, 0, [], [], [], [], function (userList, unsuccessfuluser, updateuserList, resions) {

                    var donedata = {
                        Total_req_found: jsonObj.length,
                        Total_Created_Users: userList.length,
                        Total_Updated_Users: updateuserList.length,
                        Not_created_Users_New: unsuccessfuluser.length,
                        reason_list: resions
                    }
                    //processlog(db, 'SFTP USER CREATION', "done all calls ", 'successfull', 'AUTO');
                    processlog(db, 'SFTP USER CREATION', "Done all calls ", JSON.stringify(donedata), 'AUTO', 1);
                    // send_response(req, res, undefined, donedata);
                    //exports.processsftpfile(db, sftp, tlist_array, filelistdata, u + 1, date, type, callback);
                });
            });
    } catch (e) {
        //console.log(e);
        processlog(db, 'SFTP USER CREATION', "read file data", e.message, 'AUTO', 1);
        //exports.processsftpfile(db, sftp, tlist_array, filelistdata, u + 1, date, type, callback);
    }
}

exports.sftpUserAddtest1 = function (server, db) {
    let genfname ='DevList.csv'; //"Illume_OPM_" + todaydate+'.csv';
    readfileanddatatest(globalpath + genfname, db, 1);

}


function SaveMultiUsertest(newdb, ExcelData, userId, userTId, i, userList, unsuccessfuluser, updateuserList, resions, callback) {
    console.log('ExcelData length ' + ExcelData.length);
    // console.log('s-',i);
    if (i == ExcelData.length || ExcelData.length === 0)
        return callback(userList, unsuccessfuluser, updateuserList, resions);

    if (ExcelData[i].password) {
        TwinBcrypt.hash(ExcelData[i].password, salt,
            function (p) { },
            function (hash) {
                ExcelData[i]['encryptedPwd'] = hash;
                if (ExcelData[i].encryptedPwd) {
                    ExcelData[i]['tenantId'] = userTId;
                    uploadMultipleEmployeesData(ExcelData, userId, userTId, newdb, i, userList, unsuccessfuluser, updateuserList, resions, callback);
                }
            });
    } else {
        var randomPassword = generatePassword();
        ExcelData[i].password = randomPassword;
        TwinBcrypt.hash(randomPassword, salt,
            function (p) { },
            function (hash) {
                ExcelData[i]['encryptedPwd'] = hash;
                if (ExcelData[i].encryptedPwd) {
                    ExcelData[i]['tenantId'] = userTId;
                    uploadMultipleEmployeesData(ExcelData, userId, userTId, newdb, i, userList, unsuccessfuluser, updateuserList, resions, callback);
                }
            });
    }
}*/

exports.sftpUserAddInner = (tenantData, db, cb) => {
    // return new Promise((resolve, reject)=>{
        processlog(db, 'SFTP USER CREATION'+' '+tenantData["tenantId"], "attempt", totalattempt + 1, 'AUTO', 0);
        startTimeStamp = new Date();
        var isException = 0;
        let sftp = new Client();
        try {
            processlog(db, 'SFTP USER CREATION'+' '+tenantData["tenantId"], "initiated", "Procces initiated", 'AUTO', 0);
            sftp.connect(tenantData["sftpConData"]).then(() => {
                processlog(db, 'SFTP USER CREATION'+' '+tenantData["tenantId"], "SFTP Connection done", 'successfully', 'AUTO', 0);
                console.log('connection successfull');
                return sftp.list(tenantData["sftpConData"]["fileLocation"]);
            }).then((data) => {

                                let tenantList = [];
                                tenantList.push(tenantData);

                                exports.sftpmultiplebfvl(db, sftp, tenantList, data, 0, undefined, 'AUTO', 'SFTP', function (resp) {
                                    exports.processsftpfile(db, sftp, resp, data, 0, undefined, 'AUTO', 'SFTP', function (resp) {
                                        processlog(db, 'SFTP USER CREATION'+' '+tenantData["tenantId"], "completed", 'sucessfully', 'AUTO', 0);
                                        //
                                        cb(true);
                                        endTimeStamp=new Date();
                                        ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
                                        logEnum.process.sftp, logEnum.process.sftp,startTimeStamp,endTimeStamp,
                                        isException,logEnum.exceptionType[200].value,logEnum.exceptionType[200].name,null,null);                                 
                                    
                                    });
                                });
                            
                                // resolve(true);
                                // resolve({err: false, data: true});
                                
        
                sftp.on('close', function (result) {
                    processlog(db, 'SFTP USER CREATION'+' '+tenantData["tenantId"], "SFTP Connection Closed", "Close event", 'AUTO', 0);
                });
                sftp.on('end', function (result) {
                    processlog(db, 'SFTP USER CREATION'+' '+tenantData["tenantId"], "SFTP Connection ended", "End event", 'AUTO', 0);
                });
                sftp.on('finish', function (result) {
                    processlog(db, 'SFTP USER CREATION'+' '+tenantData["tenantId"], "SFTP Connection Finished", "Finish event", 'AUTO', 0);
                });
                sftp.on('error', function (err) {
                    processlog(db, 'SFTP USER CREATION'+' '+tenantData["tenantId"], "SFTP Connection Error", "Error event", 'AUTO', 0);
                    isException=1;
                    endTimeStamp=new Date();
                    ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
                    logEnum.process.sftp, logEnum.process.sftp,startTimeStamp,endTimeStamp,
                    isException,logEnum.exceptionType[500].value,logEnum.exceptionType[500].name,null,null);
                });

                
            }).catch((err) => {
                if (err) {
                    processlog(db, 'SFTP USER CREATION'+' '+tenantData["tenantId"], "List not read", err.message, 'AUTO', 0);
                } else {
                    processlog(db, 'SFTP USER CREATION'+' '+tenantData["tenantId"], "List not read", 'Connection Timeout', 'AUTO', 0);
                }  

                if (totalattempt < attempt) {
                    totalattempt++;
                    exports.sftpUserAddInner(tenantData, db, function (resp) {
                        console.log(tenantData["tenantId"]+' '+'is connected')
                    });
                } else{
                    isException = 1;
                    endTimeStamp=new Date();
                    ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
                    logEnum.process.sftp, logEnum.process.sftp,startTimeStamp,endTimeStamp,
                    isException,logEnum.exceptionType[500].value,logEnum.exceptionType[500].name,null,null);
                    // ats.atsUserAdd(server, db);
                }
                //console.log(err, 'catch error');
                cb(false);
            });
        } catch (e) {
            //console.log(e);
            processlog(db, 'SFTP USER CREATION'+' '+tenantData["tenantId"], "SFTP Server not connected", e.message, 'AUTO', 0);
            if (totalattempt < attempt) {
                totalattempt++;
                exports.sftpUserAddInner(tenantData, db, function (resp) {
                    console.log(tenantData["tenantId"]+' '+'is connected')
                });
            }   
            else
            {
                isException=1;
                endTimeStamp=new Date();
                ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
                logEnum.process.sftp, logEnum.process.sftp,startTimeStamp,endTimeStamp,
                isException,logEnum.exceptionType[500].value,logEnum.exceptionType[500].name,null,null);
                // ats.atsUserAdd(server, db);
            }   
            cb(false);
        }
    // });
    

}

exports.getTenantListSftp = (server, db) => {
    processlog(db, 'SFTP USER CREATION', "attempt", totalattempt + 1, 'AUTO', 0);
    startTimeStamp = new Date();
    var isException = 0;
    try {
        // var db = req.db;
        var sql = 'call SP_GET_SFTP_Tenant_List()';
        db.query(sql, [], function (err, list) {
            if (err) {
              //  processlog(db, 'SFTP USER CREATION', "Get tenant list", 'Error' + err.message, 'AUTO', 0);
                isException = 1;
                endTimeStamp = new Date();
                ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
                logEnum.process.sftp, logEnum.process.sftp,startTimeStamp,endTimeStamp,
                isException,logEnum.exceptionType[500].value,logEnum.exceptionType[500].name,null,null);
            } else {
                processlog(db, 'SFTP USER CREATION', "Get tenant list", 'sucessfull', 'AUTO', 0);
                if (list[0].length > 0) {
                    let promiseArr = [];
                    //  list[0].forEach((element) => {
                    //     element["sftpConData"] = JSON.parse(element["sftpConData"]);
                    //     exports.sftpUserAddInner(element, db, function (resp) {
                    //         console.log(element["tenantId"]+' '+'is connected')
                    //     });
                        

                    //         // if(data){
                    //         //     console.log(element["tenantId"]+' '+'is connected');
                    //         // }
                           
                        
                      
                    // });
                    // console.log('Done ALL SFTP successfull');
                    // ats.sendEmailsftpats(db, 1,'SFTP');
                    // report.insertCronandSFTPLinks(db,1); //1 sftpstatus
                    // ats.atsUserAdd(server, db);

                    let count = 0;
                    let endLength = list[0].length - 1;

                    const pre = () =>{
                        console.log('length:', endLength, 'count:',count);
                        if(endLength < count){
                            console.log('Done ALL SFTP successfull');

                            // setTimeout(() =>{
                                ats.sendEmailsftpats(db, 1,'SFTP');
                                report.insertCronandSFTPLinks(db,1); //1 sftpstatus
                                ats.atsUserAdd(server, db);
                            // }, 10000);
                           
                            
                        }else {
                            
                            list[0][count]["sftpConData"] = JSON.parse(list[0][count]["sftpConData"]);
                            exports.sftpUserAddInner(list[0][count], db, function (resp) {
                                console.log(list[0][count]["tenantId"]+' '+'is connected');
                                count ++;
                                pre();
                            });  
                           
                        }
                    }
                    pre();
                } else {
                    processlog(db, 'SFTP USER CREATION', "NO data Found", 'sucessfull', 'AUTO', 0);
                    //404
                    endTimeStamp=new Date();
                    ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
                    logEnum.process.sftp, logEnum.process.sftp,startTimeStamp,endTimeStamp,
                    isException,logEnum.exceptionType[404].value,logEnum.exceptionType[404].name,null,null);
                    ats.sendEmailsftpats(db, 1,'SFTP');
                    report.insertCronandSFTPLinks(db,1); //1 sftpstatus
                    ats.atsUserAdd(server, db);
                }
            }
        })
    } catch (e) {
        processlog(db, 'SFTP USER CREATION', 'Error in Get tenant list in try block', 'Error' + e.message, 'AUTO', 0);
        isException = 1;
        endTimeStamp = new Date();
        ErrorLog.insertapilog(logEnum.kpiconstant.channel, logEnum.kpiconstant.product, 
        logEnum.process.sftp, logEnum.process.sftp,startTimeStamp,endTimeStamp,
        isException,logEnum.exceptionType[500].value,logEnum.exceptionType[500].name,null,null);
        ats.sendEmailsftpats(db, 1,'SFTP');
        report.insertCronandSFTPLinks(db,1); //1 sftpstatus
        ats.atsUserAdd(server, db);
    }
}

exports.sftpmultiplebfvl = function (db, sftp, tlist_array, filelistdata, u, date, type, servertype, callback) {
    if (u == tlist_array.length || tlist_array.length === 0)
        return callback(tlist_array);
    //console.log(tlist_array[u]);
    let todaydate = exports.GetFormattedDate(date);
    let genfname = tlist_array[u].fileName + todaydate;
    //console.log(genfname);
    processlog(db, `${servertype} USER CREATION`, "List read successfully", 'successfull', type, tlist_array[u].tenantId);
    var found = filelistdata.find(function (element) {
        let filename = element.name;
        let tempfname = filename.substring(tlist_array[u].fsubStringstart, tlist_array[u].fsubStringEnd);
        // Illume_OPM_20190603231502.csv
        // console.log('condition', tempfname == genfname , ' tempname' ,tempfname ,' genfname' ,genfname);
        //console.log('tempfname', tempfname,' genfname',genfname)
        if (tempfname == genfname) {
            return element;
        }

    });
    //console.log(found.name);
    if (found != undefined) {
        processlog(db, `${servertype} USER CREATION`, "File found", 'File found for ' + todaydate, type, tlist_array[u].tenantId);
        sftp.fastGet(servertype == 'SFTP' ? tlist_array[u]["sftpConData"]["fileLocation"]+'/'+found.name : config.ats.fileLocation+found.name, globalpath + found.name, []).then((data) => {
            // console.log(data, 'the data info');
            // console.log(found.name);
            //processlog(db, `${servertype} USER CREATION`, "File downloaded successfully", 'successfull for ' + todaydate, type, tlist_array[u].tenantId);
            processlog(db, `${servertype} USER CREATION`, "File downloaded successfully", found.name, type, tlist_array[u].tenantId); //Changed above line here to store filename received from server
            // readfileanddata(globalpath + found.name, db, tlist_array, filelistdata, u, sftp, date, type,callback);
            var filename = found.name;
            if(config.sftpconnection.sftptesting && dummyfiles[tlist_array[u].tenantId] && servertype == 'SFTP'){
                filename = dummyfiles[tlist_array[u].tenantId]
            }
            tlist_array[u].filepath = globalpath + filename; //found.name;
            exports.sftpmultiplebfvl(db, sftp, tlist_array, filelistdata, u + 1, date, type, servertype, callback);
        }, (err) => {
            //console.log(err, 'catch error');
            tlist_array[u].filepath = '';
            processlog(db, `${servertype} USER CREATION`, "File download err", err.message, type, tlist_array[u].tenantId);
            exports.sftpmultiplebfvl(db, sftp, tlist_array, filelistdata, u + 1, date, type, servertype, callback);
        });
    } else {
        console.log('file not found');
        var filename = '';
        if(config.sftpconnection.sftptesting && dummyfiles[tlist_array[u].tenantId] && servertype == 'SFTP'){
            filename = dummyfiles[tlist_array[u].tenantId]
        }
        tlist_array[u].filepath = globalpath + filename;
        processlog(db, `${servertype} USER CREATION`, "File found", 'File not found for ' + todaydate, type, tlist_array[u].tenantId);
        exports.sftpmultiplebfvl(db, sftp, tlist_array, filelistdata, u + 1, date, type, servertype, callback);
    }

}

// Code for storing LOVs from server side

exports.findlovvalues = async (db, ExcelData, tId, type, servertype)=>{
    return new Promise(async (res,rej) => {
        processlog(db, `${servertype} USER CREATION`, `${servertype} Lov Creation`, 'Started', type, tId);
        var {err, data} = await exports.formatinglovvalues(db);
        if(err){
            processlog(db, `${servertype} USER CREATION`, 'error in findlovvalues', err.message, type, tId);
        }else{
            var {err, data} = await exports.formatingEmployee(db, ExcelData, data, tId);
            if(err){
                processlog(db, `${servertype} USER CREATION`, 'error in formatingEmployee', err.message, type, tId);
            }
            else{
                var {err, data} = await exports.insertLovData(db, data, tId, type);
                if(err){
                    processlog(db, `${servertype} USER CREATION`, 'error in insertLovData', err.message, type, tId);
                }
                else{
                    console.log(data);
                    processlog(db, `${servertype} USER CREATION`, 'SFTP Lov Creation', 'Completed', type, tId);
                }
            }
        }
        res({err:false,data:'LOV Creation Process Completed'})    
    })
}

exports.formatinglovvalues = async (db)=>{
    return new Promise(async (res,rej)=>{
        const { err, data } = await getSystemLovs(db, 1, 1);
        var templov = '';
        var result = {};
        if(!err){
            data.forEach((item,index)=>{
                if(item.LOVType !== templov){
                    result[item.LOVType] = {};
                }
                result[item.LOVType][item.LOVName] = item.LOVId;
                templov = item.LOVType;
            })
        }
        res({err:false,data:result})
    })

}
exports.formatingEmployee = async (db, ExcelData, lov, tId)=>{
    return new Promise(async (res,rej)=>{
        //var employeelist = require('./employeesh.json');
        var employeelist = ExcelData;
        var result = {};
        var finalresult = {};
        const { err, data } = await getSystemLovs(db, 2, tId);
        employeelist.forEach((employeelist)=>{
            data.forEach((item)=>{
                var dataItem = item['LOVTypeName'];
                var LovTypeId = item['LOVTypeId']
                if(!result[dataItem]){
                    result[dataItem] = [];
                    finalresult[dataItem] ={
                        LovTypeId: LovTypeId,
                        data: []
                    };
                }
                var val = employeelist[dataItem];
                var tval = String(val).trim(); //Trimmed Value
                var tlval = String(tval).toLowerCase(); ///Trimmed LowerCase Value
                if(tlval && lov[dataItem] && !lov[dataItem][tlval] && !result[dataItem][tlval]){
                    finalresult[dataItem]['data'].push(tval);
                    result[dataItem][tlval] = true;
                }
            })
        })
        //console.log(result);
        //console.log(finalresult);
        res({err:false,data:finalresult})
    })

}

const getSystemLovs = async (db, flag, tId) => {
    return new Promise(function(res,rej) {
        var sql = 'call SP_GET_SFTP_LOV_DATA(?,?)';
        db.query(sql, [flag, tId], function (err, result) {
            //var resultjson = JSON.stringify(result[0]);
        if(err){
                res({err:true,data: err.message})
            } else if(result && Array.isArray(result) && result.length > 0){
                res({err:false,data: result[0]})
            }
            else{
                res({err:false,data: ''})
            }
        })
    })
}

exports.insertLovData = async (db, list, tId, type) => {
    return new Promise(async function(res,rej) {
        // var err = {};
        // var response = [];
        var finalString = '';
        await asyncForEach(Object.keys(list), async (item) =>{
            if(list[item]['data'] && list[item]['data'].length > 0){
                finalString = list[item]['data'].join('%pipe%');
                // console.log(list[item].LovTypeId);
                // console.log(finalString);
                const {err, data } = await exports.insertLovDataSP(db, list[item].LovTypeId, finalString, tId);
                //console.log(err);
                processlog(db, "SFTP Lov Creation for", `LOVTypeId :- ${data['LovTypeId']}`, 'Done', type, tId);
            }
        })
        res({err:false,data: 'Lov Insertion Process Completed'});
    })
}

exports.insertLovDataSP = (db, LovTypeId, finalString, tenantId) =>{
    return new Promise((res, rej) =>{
        var spResult = {};
        spResult={
            LovTypeId: LovTypeId,
            data: 'Success'
        };

        var sql = 'call SP_INSERT_SFTP_LOV_DATA(?,?,?)';
            db.query(sql, [LovTypeId, finalString, tenantId], async function (err, result) {
                if(err){
                    // err[item] = err;
                    res({err:true, data: err.message})
                }else {
                    res({err:false,data: spResult})
                    // response[item] = result;
                }
            })
    })
}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}
