var config = require('./config');
//var msg91 = require("msg91")(config.msg91.API_KEY, config.msg91.SENDER_ID, config.msg91.ROUTE_NO );
const logmanage = require('./manage');

exports.sendSMS = (mobileNo, message, cb) => {
    if(mobileNo && message){
        cb(null);
        /*message = message.replace(/&/g, encodeURIComponent('&'));
        msg91.send(mobileNo, message, function(err, response){
            console.log(err);
            console.log(response);
                    if(err){
          const data={
                message:message,
                senderId:mobileNo,
                sending_status:'error',
                sending_description:err,
            };
            logmanage.insertnotifylog(data);
        }
        else
        {
          const data={
                message:message,
                senderId:mobileNo,
                sending_status:'successful',
                sending_description:response,
            };
            logmanage.insertnotifylog(data);
        }
            getBalance((result)=>{
                cb(result);
            });
        });*/
    }else if(!mobileNo){
        cb("Mobile Number is empty");
    }else if(!message){
        cb("Message is empty");
    }else{
        cb("Mobile Number and Message is empty");
    }
}

exports.getBalance = (cb) =>{
    msg91.getBalance(function(err, msgCount){
        console.log(err);
        console.log(msgCount);
        if(err){
            cb(err);
        }else{
            cb(msgCount);
        }
        
    });
}

exports.sendSMSAPI = (req, res) => {
    var user = req.body;
    const mobileNo = user.mobileNo, message = user.message;
    exports.sendSMS(mobileNo, message, (result) => {
        console.log(result);
        res.json({result});
    })
}