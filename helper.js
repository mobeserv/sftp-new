/**
* Log (Learning Record Store)
* @author Bhavesh Tandel
*/

'user strict';
const DB = require('./db');

class Helper{
	
	constructor(app){
		this.db = DB;

	}

	async insertLogs(params){
		try {
			return await this.db.query(
				"call SP_InsertLogStore(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
				[params.eventname,params.component,params.subcomponent,params.action,params.target,params.areaid,params.area,params.instanceid,params.roleid,params.role,params.tenantid,params.module,params.userid,params.other,params.origin,params.ip,params.crud,params.url]
			);
		} catch (error) {
			console.warn(error);
			return error;
		}		
	}

	async insertCourseActivityLogs(params){
		try {
			return await this.db.query(
				"call SP_ADMIN_INSERT_ACTIVITY_LOG(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
				[
					params.courseId, 
					params.moduleId, 
					params.activityId,
					params.activityName,
					params.userId,
					params.actionType,
					params.activityData,
					params.otherData,
					params.role,
					params.origin,
					params.url,
					params.ip,
					params.crud,
					params.tenantId,
					params.changeString
				]
			);
		} catch (error) {
			console.warn(error);
			return error;
		}		
	}
	async insertErrorLogs(eventname, msg, source, userid){
		try {
			return await this.db.query(
				'call SP_SAVA_ERROR_LOG(?,?,?,?);',
				[eventname, msg, source, userid]
			);
		} catch (error) {
			console.warn(error);
			return error;
		}		
	}
	
	async insertNotificationogs(params){
		try {
			return await this.db.query(
				'call SP_Insert_notification_log(?,?,?,?);',
				[
					params.message,
					params.senderId,
					params.sending_status,
					params.sending_description,

				]
			);
		} catch (error) {
			console.warn(error);
			return error;
		}		
	}

	async insertApiLogs(channel, product, process, subProcess,startTimeStamp,endTimeStamp,exception,functionStatusCode,exceptionType,userid,tenantid){
		try {
			return await this.db.query(
				'call SP_SAVE_API_LOG(?,?,?,?,?,?,?,?,?,?,?);', 
				[channel, product, process, subProcess,startTimeStamp,endTimeStamp,exception,functionStatusCode,exceptionType,userid,tenantid]
			);
		} catch (error) {
			console.warn(error);
			return error;
		}		
	}

	
}
module.exports = new Helper();